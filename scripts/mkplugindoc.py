# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
mkplugindoc

python scripts/mkplugindoc.py ../orchestrator/opentf/plugins/actionprovider/plugin.yaml  >docs/providers/actionprovider.md

If source file name is of form `[x.]+y.z`, `y` is used as the current language.

For example, if source file name is `plugin.fr.yaml`, the current language will
be `fr`.  If it is `plugin.yaml`, the current language will be the default
language, `en`.

Translation files are JSON files in the same directory as the script.  Their
names follow the `translations.x.json` pattern.

Please refer to `translations.en.json` for the texts to translate.
"""

from pathlib import Path

import json
import sys
import yaml

FILE = sys.argv[1]
RESOLVED_FILE = Path(FILE).resolve()
PAGE_TITLE = RESOLVED_FILE.parent.stem

DEFAULT_LANGUAGE = 'en'

if RESOLVED_FILE.name.count('.') >= 2:
    CURRENT_LANGUAGE = RESOLVED_FILE.name.split('.')[-2]
else:
    CURRENT_LANGUAGE = DEFAULT_LANGUAGE

CONTENT = []


########################################################################

APACHE2_LICENSE = '''<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
'''

HOOK_CONFIG_TEMPLATE = 'hook_config_template'
FUNCTIONS_NAVIGATION_TEMPLATE = 'functions_navigation_template'
INPUTS_HEADER = 'inputs_header'
REPORTS_HEADER = 'reports_header'

########################################################################

TRANSLATIONS = {}
try:
    translations = f'{Path(sys.argv[0]).parent}/translations.{CURRENT_LANGUAGE}.json'
    with open(translations, 'r', encoding='utf-8') as f:
        TRANSLATIONS[CURRENT_LANGUAGE] = json.load(f)
except Exception as err:
    print(
        f'Could not read language translations for {CURRENT_LANGUAGE}: {err}.',
        file=sys.stderr,
    )
    sys.exit(2)


########################################################################


def _(message):
    """Translate message.

    If message is not available in current language, use the default
    language version.  If no default language version is available, use
    message as-is.
    """
    if translated := TRANSLATIONS.get(CURRENT_LANGUAGE, {}).get(message):
        return translated
    return TRANSLATIONS.get(DEFAULT_LANGUAGE, {}).get(message, message)


def emit(what=''):
    """Accumulate content."""
    CONTENT.append(what)


def adjust_levels(content, inc):
    """Adjust sections.

    Add `inc` levels to sections so that plugin manifests are 'logical'.
    """
    lines = []
    for line in content.splitlines():
        if line.startswith('#'):
            line = ('#' * inc) + line
        lines.append(line)
    return '\n'.join(lines)


def maybe_emit_items(header, items):
    """Emit items if any.

    If items is empty, the header is ignored.

    # Required parameters

    - header: a string
    - items: an iterable of pairs.
    """
    if not items:
        return
    for line in header.splitlines():
        emit(line)
    for input, definition in items:
        if 'required' in definition:
            opt = f' ({_("required") if definition["required"] else _("optional")})'
        else:
            opt = ''
        emit(f'- `{input}`{opt}')
        emit()
        for line in definition['description'].splitlines():
            emit(f'    {line}')
        emit()


def write_hook_config(name):
    """Write default hook configuration section."""
    print(_(HOOK_CONFIG_TEMPLATE).format(name=name, NAME=name.upper()))


def write_doc(overall, feathers):
    """Write documentation on standard output.

    # Required parameters

    - overall: a dictionary or None
    """
    sys.stdout.reconfigure(encoding="utf-8")
    if overall:
        metadata = overall['metadata']
        if metadata.get('license') == 'apache2':
            print(APACHE2_LICENSE)
        title = metadata.get('title') or overall['events'][0]['categoryPrefix']
        print(f'# {title}')
        print()
        print(metadata['description'])
    else:
        print(f'# {PAGE_TITLE}')

    print()
    print(_('## Functions'))
    if feathers:
        print()
        print('<p>', feathers, '</p>')

    print()
    for line in CONTENT:
        print(line)

    if overall:
        metadata = overall['metadata']
        if 'notes' in metadata:
            print(metadata['notes'])
        if overall['kind'] == 'ProviderPlugin' and metadata.get('hooks') == 'default':
            write_hook_config(metadata['name'])


########################################################################


def main():
    with open(FILE, 'r', encoding='utf-8') as f:
        manifests = list(yaml.safe_load_all(f))

    overall = None
    feather = False
    feathers = ''

    for manifest in manifests:
        if 'cmd' not in manifest:
            overall = manifest
            continue

        name = (
            manifest['events'][0]['categoryPrefix']
            + '/'
            + manifest['events'][0]['category']
        )
        if version := manifest['events'][0].get('categoryVersion'):
            name += f'@{version}'
        else:
            name += '@v1'

        if 'branding' in manifest:
            feather = True
            brand = manifest['branding']
            color = f' stroke="{brand["color"]}"' if "color" in brand else ""
            branding = f'<i data-feather="{brand["icon"]}"{color}></i> '
            feathers += f'<a href="#{"".join(c for c in name if c.isalnum() or c == "-")}" title="{name}">{branding}</a> '
        else:
            branding = ''

        emit(f'### {branding}{name}')
        emit()
        emit(adjust_levels(manifest['metadata']['description'], 2))
        emit()
        maybe_emit_items(
            header=_(INPUTS_HEADER),
            items=manifest['inputs'].items(),
        )
        maybe_emit_items(
            header=_(REPORTS_HEADER),
            items=manifest.get('reports', {}).items(),
        )

        if 'notes' in manifest['metadata']:
            emit(adjust_levels(manifest['metadata']['notes'], 2))
            emit()

    if feather:
        emit('<script src="../../js/feather.min.js"></script>')
        emit('<script>feather.replace()</script>')
        emit()

    write_doc(overall, feathers)


if __name__ == '__main__':
    main()
