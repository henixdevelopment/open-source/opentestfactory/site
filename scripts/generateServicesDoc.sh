#!/bin/bash

# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
receivingDirectory="$SCRIPT_DIR"/../docs/providers

cd `mktemp -d`
echo "Working in `pwd`"
git clone --single-branch --branch main --depth 1 https://gitlab.com/henixdevelopment/open-source/opentestfactory/orchestrator.git
for techno in $( ls orchestrator/opentf/plugins/*/plugin.yaml | xargs -n 1 dirname | xargs -n 1 basename )
do
  sourceFile=orchestrator/opentf/plugins/"$techno"/plugin.yaml
  targetFile="$receivingDirectory"/"$techno".md
  if grep -q categoryPrefix $sourceFile; then 
    python "$SCRIPT_DIR"/mkplugindoc.py $sourceFile > $targetFile 
    echo "Generated $targetFile"
  fi
done
rm -rf orchestrator
