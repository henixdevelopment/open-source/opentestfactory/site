<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Products integrating OpenTestFactory


## Squash
[Squash](https://www.squashtest.com/en?utm_source=Doc_OTF&utm_medium=link){:target="_blank"} is a comprehensive functional testing tool: it supports any lifecycle (V-cycle, Scrum, SAFe), it can be used during the build or run phases, it manages manual and automated testing…

Squash includes its own Orchestrator, which is based on the OpenTestFactory Orchestrator.

This allows a user to launch some automated tests from Squash TM (the Web application part of Squash). Once the tests are finished, their results and reports are published in Squash TM.

This also enables the integration of Squash in a continuous integration environment:  
Once a pipeline is triggered (e.g., by a new pull/merge request or by a scheduled execution),

* a test plan is retrieved from Squash TM;
* then, Squash Orchestrator executes the tests;
* at last, the test results and reports are published in Squash TM.
