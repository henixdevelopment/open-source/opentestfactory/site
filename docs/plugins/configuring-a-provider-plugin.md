<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Common provider settings

!!! Information
    This configuration documentation applies to all provider plugins part of
    OpenTestFactory.  Externally provided provider plugins may or may not
    support this configuration.

    If, as a provider plugin writer you are using the `opentf-toolkit` package, your
    provider plugin supports this configuration.

Provider plugins provide _functions_ that can be used in _workflows_.

They typically expose no user-facing endpoints.

You can find the list of providers part of the reference implementation in the
[Providers](../providers/index.md) section.  If no specific instructions are provided
in your provider's documentation, the provider name to use is the 'short' name
used as the document title (for example, `cucumber` for the Cucumber provider
plugin).

## Environment variables

### Logging

You can set the `{provider_name}_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service.

It defaults to `INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `{provider_name}_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

### Configuration options

You can also set the `watchdog_polling_delay_seconds` configuration option using the
`{provider_name}_WATCHDOG_POLLING_DELAY_SECONDS` environment variable.

If this environment variable is set, it overrides the value in the configuration file.

### Hooks specification

The `{provider_name}_PROVIDER_HOOKS` environment variable, if defined, must refer to an
existing file that contains hook definitions.  Those hooks definitions will replace the ones
possibly present in the plugin configuration file.

!!! tip
    If the content of this referred file changes, the hooks definitions used by the provider
    will change accordingly.  You do not need to restart the provider plugin.

## Configuration file

Those plugins have configuration files (`{provider_name}.yaml` by default) that describe the _host_,
_port_, _ssl_context_, and _trusted_authorities_ they use.  They can also enable _insecure_
logins.

If no configuration file is found for a given provider plugin it will default to the following values:

```yaml hl_lines="13 14"
apiVersion: opentestfactory.org/v1beta1
kind: ProviderConfig
current-context: default
contexts:
- name: default
  context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: reuse
    watchdog_polling_delay_seconds: 30
hooks:
```

The configuration options included in the 'allinone' image are described in "[Common settings](../services/config.md#configuration-file)."
The listening port varies per plugin and the bind address is `127.0.0.1` as these plugins expose no user-facing endpoints.

In addition to the common configuration options, provider plugin configuration files may
have a `hooks` section.

!!! important
    If a `{provider_name}_PROVIDER_HOOKS` environment variable is defined, the hooks defined
    in the configuration file are ignored and the ones defined in the file referred to by the environment
    variable are used instead.

### Watchdog polling delay

_This delay must be an integer. If the entry is missing, the default value will be assumed. It must be defined in the currently-used context._

`watchdog_polling_delay_seconds` defines the interval used to check hooks definition changes.
If not specified, or if set to a lower value, defaults to 30.

This is only used if the `{provider_name}_PROVIDER_HOOKS` environment variable is defined.  It
tells the service how often it must check for changes in the hooks definition.

### Hooks

Hooks define steps that will run before and/or after a function is executed.

For more information about hooks, see "[Hooks for jobs and providers](../impl/reference/hooks.md)."

#### Scope

Hooks defined in a provider plugin configuration file or in a file referred to by the
`{provider_name}_PROVIDER_HOOKS` environment variable only apply to functions offered by the
plugin.

#### Sequence

The `hooks` section contains an ordered list of hooks.  Whenever a function provided
by the plugin is called, those hooks are tested in order.  All hooks that match are
applied.

If two or more hooks match, the `before` blocks are added in order and the `after`
blocks are added in reverse order:

```text
before_steps from workflow-defined hooks
  before_steps from provider-defined hook 1
    before_steps from provider-defined hook 2
      steps from function
    after_steps from provider-defined hook 2
  after_steps from provider-defined hook 1
after_steps from workflow-defined hooks
```

#### Examples

In the following examples, you will define hooks for the Cucumber plugin.

##### Defining hooks using a definition file

!!! note
    This is the recommended way to define hooks.

You can provide a separate file for the hooks definition.  This
file can have any name, as long as it is accessible from your plugin and the
`CUCUMBER_PROVIDER_HOOKS` environment variable is set to its path:

```shell
CUCUMBER_PROVIDER_HOOKS=/app/hooks/cucumber_hooks.yaml
```

The hooks definition is as follows:

```yaml title="/app/hooks/cucumber_hooks.yaml"
hooks:
  - name: my hook
    events:
    - categoryPrefix: cucumber
      category: cucumber
    if: (contains(inputs.reporters, 'junit')) && (runner.os == 'windows')
    before:
    - run: echo hello windows
    - run: del foobar.html
    after:
    - run: echo ::attach::foobar.html
    - run: cleanup
      if: always()
  - name: my other hook
    events:
    - category: _
    after:
    - uses: actions/delete-file
      with:
        path: foo/bar
```

##### Defining hooks in the provider configuration file

!!! warning
    This way to define provider hooks is deprecated.

Alternatively, you can add a `hooks` section in your provider configuration file for the
Cucumber plugin (typically `/app/conf/cucumber.yaml`):

```yaml
apiVersion: opentestfactory.org/v1beta1
kind: ProviderConfig
current-context: ...
contexts:
  ...
hooks:
  - name: my hook
    events:
    - categoryPrefix: cucumber
      category: cucumber
    if: (contains(inputs.reporters, 'junit')) && (runner.os == 'windows')
    before:
    - run: echo hello windows
    - run: del foobar.html
    after:
    - run: echo ::attach::foobar.html
    - run: cleanup
      if: always()
  - name: my other hook
    events:
    - category: _
    after:
    - uses: actions/delete-file
      with:
        path: foo/bar
```

##### Observable effects

Assuming the following workflow:

```yaml
metadata:
  name: hooks demo
jobs:
  my_job:
    runs-on: windows
    steps:
    - run: echo Hi there
    - uses: cucumber/cucumber@v1
      with:
        reporters: [foo, junit]
    - run: echo Bye
  my_second_job:
    runs-on: windows
    steps:
    - uses: cucumber/params@v1
```

The `my_job` job will execute the following steps, in order:

```yaml
- run: echo Hi there
- run: hello windows                    # added by 'my hook'
- run: del foobar.html                  # added by 'my hook'
- run: <what cucumber/cucumber does>
- run: <what actions/delete-file does>  # added by 'my other hook'
- run: echo ::attach::foobar.html       # added by 'my hook'
- run: cleanup                          # added by 'my hook'
```

If Cucumber returns a non-zero status code, the `actions/delete-file` and `echo ::attach::`
steps will be skipped, but the `cleanup` one will run nonetheless, as it includes an
`if: always()` clause.

The `my_second_job` job will execute the following steps, in order:

```yaml
- run: <what cucumber/params does>
- run: <what actions/delete-file does>  # added by 'my other hook'
```

## Subscriptions

The provider plugins typically subscribe to the following events:

| `kind`              | `apiVersion`            |
| ------------------- | ---------------------- |
| `ProviderCommand`   | opentestfactory.org/v1 |

The provider plugins typically expose an `/inbox` endpoint that is used by the event bus to post relevant events.

## Launch command

If you want to manually start a provider plugin, use something like the following command, adjusting
the module name if applicable:

```shell
python -m opentf.plugins.{provider_name}.main [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](../services/config.md#command-line-options)."
