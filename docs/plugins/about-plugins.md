<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# About Plugins

_Plugins are individual blocks that power your orchestrator.  You can create your own plugins
or use and customize plugins shared by the OpenTestFactory community._

## About Plugins

You can create plugins by writing custom code that interacts with your workflow in any way
you would like, including integrating with any available third-party API.  For example, a
function can send SMS alerts when urgent issues are created.

You can write your own plugins to use in your workflow or share the plugins you
build with the OpenTestFactory community.

Plugins can run directly on a machine or in a Docker container.  You can define
a plugin's inputs, outputs, and environment variables.

## Type of Plugins

The most common type of plugin is a plugin that consumes events from the event bus.  The
event bus is a message broker that allows different services to communicate with each other.

In response to this event, the plugin performs a specific action, and may emit events to the
event bus to communicate with other services.

For example, a _provider_ plugin subscribes to `ProviderCommands` events, and then emits a
`ProviderResult` event to communicate the result of the command.

```mermaid
sequenceDiagram
    participant Plugin
    participant eventbus
    participant other services

    Plugin->>eventbus: Subscription

    other services-->>eventbus: ProviderCommand

    eventbus-->>Plugin: ProviderCommand
    activate Plugin
    Plugin-->>eventbus: ProviderResult
    deactivate Plugin
```

Most plugins and services in the OpenTestFactory orchestrator are of this kind: they
consume events from the event bus, and emit events to the event bus in response.

Some plugins simply consumes events from the event bus.  One such plugin is the _Tracker Publisher_
plugin.  It consumes `WorkflowCompleted` events and convey the information it gathered from
this event to external services (GitLab or Jira instances, for example).

Some other plugins simply emit events to the event bus.  One such plugin is the _Receptionist_
service: it emits `Workflow` events, and consumes no events.

Finally, some plugins may simply interact directly with other services through their public
endpoints.  One such plugin is the _Quality Gate_ service: it exposes a public endpoint that
accepts requests, and then interacts with the _Observer_ service.  It neither publishes nor
consumes events from the event bus.

## Cartography

```mermaid
quadrantChart
    title Plugins types
    x-axis Produces no events --> Produces events
    y-axis Consumes no events --> Consumes events
    quadrant-1 Regular
    quadrant-2 Publishers
    quadrant-3 Friends
    quadrant-4 External facades
    Tracker Publisher: [0.3, 0.6]
    Local Publisher: [0.2, 0.7]
    Localstore: [0.1, 0.8]
    Killswitch: [0.65, 0.23]
    Arranger: [0.57, 0.69]
    Observer: [0.87, 0.89]
    Channel Handlers: [0.77, 0.59]
    Receptionist: [0.78, 0.34]
    Quality Gate: [0.25, 0.25]
    S3 Publisher: [0.35, 0.78]
    Insights Collector: [0.6, 0.9]
    Surefire Parser: [0.8, 0.7]
    Providers: [0.9, 0.55]
    Generators: [0.75, 0.85]
```

## Writing Plugins

You can build plugins using any technology you like.  Plugins require a metadata file to
define the inputs, outputs and main entrypoint for your plugin.  The metadata filename must
be `plugin.yaml`.  For more information, see
"[Descriptor Syntax](metadata-syntax-for-opentf-plugins.md)."

## Creating a README File for your Plugin

We recommend creating a README file to help people learn how to use your plugin.  You can include
this information in your `README.md`:

- A detailed description of what the plugin does
- Required input and output arguments
- Optional input and output arguments
- Environment variables the plugin uses
- An example of how to use your plugin in a workflow
