<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Creating Plugins

## [About Plugins](about-plugins.md)

Plugins are individual blocks that power your orchestrator. You can create your own plugins or use and customize plugins shared by the OpenTestFactory community.

## [Creating a Generator Plugin](creating-a-generator-plugin.md)

In this guide you will learn how to build a generator plugin.

## [Creating a Provider Plugin](creating-a-provider-plugin.md)

This guide shows you the minimal steps required to build a provider plugin.

## [Deploying a Plugin](deploying-a-plugin.md)

There are multiple way you can deploy your plugins.

## [Descriptor Syntax](metadata-syntax-for-opentf-plugins.md)

You can create plugins to perform tasks in your workflows. Plugins require a metadata file that uses YAML syntax.

## [Notifications](notifications.md)

A plugin typically consumes events and may produce events on its own. It may also produce _notifications_, which are events that may be consumed by other services that only carry informational elements.
