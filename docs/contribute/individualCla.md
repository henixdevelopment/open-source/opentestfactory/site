<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# OpenTestFactory Individual Contributor License Agreement

Before you contribute to the OpenTestFactory open source project, you must complete all the fields in this form, sign it and return it to Henix. Please make sure you have your GitLab username at the ready. If you don’t have one, you can create one now at GitLab (https://gitlab.com).

Thank you for your interest in contributing to OpenTestFactory software project managed by Henix (“We” or “Us”). We refer to the contributor as "You". 

In order to document the copyright and other rights granted with Contributions (as defined below), We collect a Contributor Agreement ("Agreement") from You which confirms your agreement to the terms below. Please follow the instructions at the end of the document to make it effective. This is a legally binding document, so please read it carefully before agreeing to it.

1. DEFINITIONS

1.1 "Contribution" means any work of authorship that is Submitted to Us in which You own the Copyright. If You do not own the Copyright in the entire work of authorship Submitted, You should follow the procedure in Section 3(d). 

1.2 "Copyright" means all rights protecting works of authorship, including copyright, moral and neighboring rights, as appropriate, for the full term of their existence including any extensions owned or controlled by You [or Your Affiliates].

1.3 "Effective Date" means the earlier of your execution of this Agreement or your first submission of a Contribution to Us.

1.4 "Legal Entity" means a corporation, limited liability company or other entity.

1.5 "Media" means any Contribution which is not software. Media may include video, text or images. 

1.6 "Patent Claims" means any patent claim(s), now owned or acquired in the future, including without limitation, method, process, and apparatus claims which You [or Your Affiliates] (a) owned or controlled by You [or Your Affiliates] or (b) which You [or Your Affiliates] have the right to grant, to the maximum extent possible, whether on the Submission Date of the relevant Contribution or subsequently acquired, under the terms of Section 2.1(b), but only to the extent to which the making, using, having made, selling, offering for sale, importing or otherwise transferring of the Contribution or the Contribution with a Work infringes such Patent Claims. 

1.7 "Submission" means the work of authorship which You have Submitted. A Submission may consist of only a Contribution or may include third party works of authorship in addition to the Contribution. "Submitted" means any form of electronic, verbal, or written communication sent to Us or our representatives, including but not limited to communication of electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, Us for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by You as "Not a Contribution." 

1.8 "Submission Date" means the date on which a Contribution is Submitted to Us.

1.9 "Work" means the work or works of authorship which are software made available by Us to third parties but not including the Contribution. This Agreement may cover Submissions to one or more Works. 

1.10 "You" means any individual on behalf of whom a Contribution has been received by Us. 

2. TRANSFER OPTIONS

2.1 Copyright Assignment/Patent License

(a) At the time the Contribution is Submitted, You assign all right, title, and interest worldwide in all Copyrights covering the Contribution to Us; provided that this transfer is conditioned upon compliance with Section 2.1(f). 

(b) To the extent any of the rights, title and interest in the Copyrights in Section 2.1(a) are not assigned by You to Us, You grant to Us a perpetual, worldwide, exclusive, royalty-free, transferable, irrevocable license under such non assigned Copyrights, with rights to sublicense through multiple tiers of sublicensees, to practice such non-assigned rights, including, but not limited to, the right to reproduce, modify, display, perform and distribute the Contribution provided that this transfer is conditioned upon compliance with Section 2.1(f). 

(c) To the extent any of the rights in and to the Copyrights covering the Contribution can neither be assigned nor licensed by You to Us, You irrevocably waive and agree never to assert such rights against Us, any of our successors in interest, or any of our licensees, either direct or indirect; provided that this transfer is conditioned upon compliance with Section 2.1(f).

(d) Upon such transfer of rights to Us, to the maximum extent permitted by law for the Copyrights to the extent transferred by You under Sections 2.1(a), 2.1(b) and 2.1(c), We immediately grant to You a perpetual, worldwide, non-exclusive, royalty-free, transferable, irrevocable license under such rights covering the Contribution, with rights to sublicense through multiple tiers of sublicensees, to reproduce, modify, display, perform, and distribute the Contribution. The intention of the parties is that this license will be as broad as possible and to provide You with rights as similar as possible to the owner of the rights that You transferred. This license back is limited to the Contribution and does not provide any rights to the Work.

(e) You grant to Us a perpetual, worldwide, non-exclusive, transferable, royalty-free, irrevocable license under the Patent Claims, with the right to sublicense these rights to multiple tiers of sublicensees, to make, have made, use, offer for sale, sell, import and otherwise transfer the Contribution and the Contribution in combination with the Work in which the Contribution is included (and portions of such combination) as it exists on the Submission Date of that Contribution. 

(f) Based on the grant of rights granted in Sections 2.1(a), 2.1(b) and 2.1(c), if We include Your Contribution in a Work, We may license such Contribution under any license, including copyleft, permissive, commercial, or proprietary licenses, but as a condition of the exercise of this right in Section 2.1(f), We agree to license the Contribution under the terms of the license or licenses which We are using for the Work in which the Contribution is included on the Submission Date. 

(g) In addition to the license selected above in Section 2.1(f), We may use any additional license for the Media portion of the Contribution (including any right to adopt any future version of a license if permitted). 

2.2 Moral Rights.

If moral rights apply to the Contribution, to the maximum extent permitted by law, You waive and agree not to assert such moral rights against Us or our successors in interest, or any of our licensees, either direct or indirect. 

2.3 Our Rights.

You acknowledge that We are not obligated to use your Contribution as part of the Work distributed by Us and may make the decision to include any Contribution as it believes is appropriate. 

2.4 Reservation of Rights.

Any rights not expressly assigned under this section are expressly reserved by You. 

3. AGREEMENT

You confirm that:

(a) You have the legal authority to enter into this Agreement.

(b) as provided in the definitions, You reaffirm that You [or Your Affiliates] own the Copyrights and the Patent Claims covering the Submission which are required to grant the rights under Section 2. 

(c) The grant of rights under Section 2 does not violate any grant of rights which You have made to third parties, including your employer. If You are less than eighteen years old, please have your parents or guardian sign the Agreement. 

4. DISCLAIMER

EXCEPT FOR THE EXPRESS WARRANTIES IN ARTICLE 3, THE CONTRIBUTION IS PROVIDED "AS IS". MORE PARTICULARLY, ALL EXPRESS OR IMPLIED WARRANTIES INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE EXPRESSLY DISCLAIMED BY YOU TO US [AND BY US TO YOU]. TO THE EXTENT THAT ANY SUCH WARRANTIES CANNOT BE DISCLAIMED, SUCH WARRANTY IS LIMITED IN DURATION TO THE MINIMUM PERIOD PERMITTED BY LAW. 

5. CONSEQUENTIAL DAMAGE WAIVER

TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL YOU [OR US] BE LIABLE FOR ANY LOSS OF PROFITS, LOSS OF ANTICIPATED SAVINGS, LOSS OF DATA, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL AND EXEMPLARY DAMAGES ARISING OUT OF THIS AGREEMENT REGARDLESS OF THE LEGAL OR EQUITABLE THEORY (CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE) UPON WHICH THE CLAIM IS BASED. 

6. MISCELLANEOUS

6.1 Governing Law.

This Agreement will be governed by and construed in accordance with the laws of France excluding its conflicts of law provisions. Under certain circumstances, the governing law in this section might be superseded by the United Nations Convention on Contracts for the International Sale of Goods ("UN Convention") and the parties intend to avoid the application of the UN Convention to this Agreement and, thus, exclude the application of the UN Convention in its entirety to this Agreement. 

6.2 Miscellaneous Provisions.

This Agreement sets out the entire agreement between You and Us for your Contributions to Us and overrides all other agreements or understandings. Either party may assign the rights and obligations under the Agreement to any third party if the third party agrees, as a condition of the assignment, in writing to abide by the rights and obligations in the Agreement. The failure of either party to require performance by the other party of any provision of this Agreement in one situation does not affect the right of a party to require such performance at any time in the future and a waiver of performance under a provision in one situation is not considered a waiver of the performance of the provision in the future or a waiver of the provision in its entirety. If any provision of this Agreement is found void and unenforceable, such provision will be replaced to the extent possible with a provision that comes closest to the meaning of the original provision and which is enforceable. The terms and conditions set forth in this Agreement applies notwithstanding any failure of essential purpose of this Agreement or any limited remedy to the maximum extent possible under law.

You:

First name:<br>
Last name:<br>
Email:<br>
Phone:<br>
GitLab username:<br>
Address:<br>
Country:<br>

We:

Philippe VAILLERGUES, CEO of HENIX<br>
1, rue François ORY<br>
92120 MONTROUGE<br>
France