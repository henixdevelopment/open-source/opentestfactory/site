<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# How to contribute to OpenTestFactory

## Prerequisite

To be able to submit code, test, or documentation changes to OpenTestfactory, you must have completed one of the two Contributor License Agreements ([individual](individualCla.md) or [entity](entityCla.md), depending on the fact that you are contributing as a private person or as a company employee), signed it, and communicated it to Henix (the simplest is to send a scan of the signed paper to contact@henix.fr).

 

## Proposal

1. Create an issue in GitLab to describe the proposed feature:
    - What is the problem we want to resolve, the usage we want to improve?  
    Detail what users are impacted, for which use cases…
    - How do we want to resolve it?  
    Describe the feature.
    
    The issue should be created in the GitLab project corresponding to the component that must be improved. In case several components must be modified, create the issue in [https://gitlab.com/henixdevelopment/open-source/opentestfactory](https://gitlab.com/henixdevelopment/open-source/opentestfactory){:target="_blank"}.

2. Assign the issue to the OpenTestFactory Product Owner ([@lmazure](https://gitlab.com/lmazure){:target="_blank"}).

3. The OpenTestFactory Product Owner will review the issue with some OpenTestFactory team members.  
    The issue may be closed (with an explanation) if the proposed feature is already available in the current OpenTestFactory implementation or planned in a near future, if it is not clear enough to be usable, or if it is considered as not interesting for the project.  
    If the proposal is interesting, an acceptance comment is added to the issue and this one is assigned to its initiator. This comment will indicate who, in the OpenTestFactory team, will be in charge of approving the future Merge Request. These persons will also be added as participants to the issue.  
    Note that, during this decision process, there may be some discussion (via the issue or, if necessary, with a TCon) between the OpenTestFactory team and the initiator to detail or clarify the proposal.

## Implementation

If the proposal is accepted, you can start implementing the feature.

1. Create a public fork of the GitLab project(s) corresponding to the OpenTestFactory component(s) you want to contribute to.

2. Implement your proposal.  
    Check the documentation and guidelines available on the opentestfactory.org site and in the GitLab projects.  
    The OpenTestFactory team can provide some technical or process support if needed. This support should be asked by commenting on the issue: the OpenTestFactory members participating to the issue will be notified and will try to help (but this support will be constrained by the availability of these persons).

3. Once the code (including the automated tests and the documentation for the new feature) is ready, you have verified that all automated tests and static code analyses are successful, and you have manually validated that your improvement fulfils the initial proposal, a merge request should be created (if several GitLab projects are impacted, one per project).

4. If the Merge Request build fails, the Merge Request needs to be fixed.

5. If the Merge Request build is successful, the OpenTestFactory team members in charge of the code review will perform this one.  
    If refused, a comment is added explaining why the Merge Request is not acceptable and what must be done to correct it. The merge request should be updated, and a new proposal be made.

6. If accepted, the code will be merged by the OpenTestFactory team in the OpenTestFactory code base.  
    Your feature will now be available to all OpenTestFactory users!
