<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using hooks to customize job execution

_You use hooks to override the default behavior of a job or of all jobs in a workflow, or to
customize the behavior of functions._

## Overview

## About functions hooks

You can use `hooks` to define a set of actions to customize the behavior of functions.

Typically, you use function hooks to:

- [Collect additional attachments](../guides/hooks.md#additional-attachments)
- [Add audit logs](../guides/hooks.md#audit-logs)
- [Collect additional logs](../guides/hooks.md#junit-detailed-execution-logs)

<h3>Example of defining hooks for function a</h3>

![Function hooks](../resources/function_hooks.png#only-light)
![Function hooks](../resources/function_hooks_dark.png#only-dark)

When more than one hook is defined for the same event, the orchestrator order them as follows:

1. `before` hooks defined in the workflow (in order of appearance)
2. `before` hooks defined by the function handler (in order of appearance)
3. Steps from the event
4. `after` hooks defined by the function handler (in reverse order)
5. `after` hooks defined in the workflow (in reverse order)

For more information on setting function hooks, see "[Defining function hooks](../guides/hooks.md#defining-provider-hooks-using-a-definition-file)."

## About job hooks

Typically, you use jobs hooks to:

- [Keep the execution workspace on failure](../guides/hooks.md#keep-the-execution-workspace-on-failure)
- [Start and stop a service](../guides/hooks.md#start-and-stop-a-service)

Job hooks override the default behavior at job setup and teardown time.  That allows
you to specify a specific workspace for the job to run on, or to keep this workspace if the
job fails, or if some condition occurs.

<h3>Example of defining job hooks</h3>

![Job hooks](../resources/job_hooks.png#only-light)
![Job hooks](../resources/job_hooks_dark.png#only-dark)

When more than one hook is defined for the same event, the orchestrator order them as follows:

1. `before` hooks defined in the workflow (in order of appearance)
2. `before` hooks defined by the job handler (in order of apperance)
3. Steps from the event
4. `after` hooks defined by the job handler (in reverse order)
5. `after` hooks defined in the workflow (in reverse order)

For more information on setting job hooks, see "[Defining job hooks](../guides/hooks.md#defining-job-hooks-using-a-definition-file)."
