<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using JWT Signature Authorization

JWT signature-based access control defines an access control paradigm whereby
access rights are granted to users depending on the key used to sign the JWT
token.

## Trusted Authorities Attributes 

To enable `JWT` mode for a service, specify the `--authorization-mode=JWT` command-line
option on startup (this is the default).

The orchestrator reads trusted authorities attributes from a file when given the `--trustedkeys-auth-file`
option on the command line.

!!! Info
    If you are using one of the provided Docker images, use the
    `OPENTF_AUTHORIZATION_MODE` and `OPENTF_TRUSTEDKEYS_AUTH_FILE` environment variables instead.
    This setting will apply to every service launched by the image.

Currently, the trusted authorities' attributes cannot be changed without restarting the orchestrator.

The 'trusted authorities' file is a CSV file with a minimum of 2 columns: trusted authority and authority
name, followed by optional group names and namespace names.

Empty lines and lines starting with a `#` character are ignored as comments.

!!! Note

    If you have more than one group or namespace, the column must be double-quoted e.g.

    ```csv
    /etc/opentf/admin_key.pub,admin public keys,"dept_a_admin,dept_b_admin","namespace-a,namespace-b"
    ```

    Groups are not used currently.  If you want to assign namespaces to a trusted authority,
    you must leave the third column empty or fill it with a dummy value.

JWT tokens validated by trusted authorities that have no attributes attached only have access
to the `default` namespace.  That is also the case when no 'trusted authorities' file
is provided.

## Examples

### JWT tokens for administrators

Users with a token whose signature is verified by `/etc/opentf/admin.pub` can do anything to all resources.

```csv
/etc/opentf/admin.pub,Administrators,,"*"
```

### JWT tokens for department A members

Users with a token whose signature is verified by `/etc/opentf/dept_a.pub` can only interact with resources
in the `sandbox` and `department-a` namespaces.

```csv
/etc/opentf/dept_a.pub,Department A,,"department-a,sandbox"
```

### All together

```csv
/etc/opentf/admin.pub,Administrators,,"*"
/etc/opentf/dept_a.pub,Department A,,"department-a,sandbox"
```
