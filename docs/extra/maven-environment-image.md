<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

#  Cucumber JVM/JUnit Environnment

In order to simplify the execution of Cucumber JVM and JUnit tests, OpenTestFactory proposes a Docker image of an execution environment for Maven-driven tests: [opentestfactory/maven-runner](https://hub.docker.com/r/opentestfactory/maven-runner){:target="_blank"}.

## Image content

- [OpenJDK](https://openjdk.org/){:target="_blank"} 17.0.9
- [Maven](https://maven.apache.org/){:target="_blank"} 3.9.5
- [Firefox](https://www.mozilla.org/firefox/){:target="_blank"} 115.4.0esr
- [Chromium](https://www.chromium.org/){:target="_blank"} 122.0.6261.128
- Xvfb
- openssh-server 
- x11vnc

## Using the image

### User profile

The image is conceived to support running tests as `otf`. Running tests as `root` or any other non-`otf` will generate problems.

### Starting the image

```bash
docker run -d \
    --name maven \
    --env PASSWORD_ACCESS=true \
    --env USER_PASSWORD=<secret password> \
    opentestfactory/maven-runner:latest
```

*Note:* If `USER_PASSWORD` is not specified, the default password is `otf`.

### Running a test via `ssh`

(We assume the test is accessible from within the image.)

```bash
ssh otf@<IP of the container> mvn test -Dtest=<test>
```

The `ssh` port is 22 by default. Use the `SSHD_PORT` environment variable to change this value: `docker run -d … --env SSHD_PORT=<your-port> …` to change it. 

### Xvfb

If your tests require a display:

- When using `ssh` to run a test, the display 99 is loaded.
- When using `docker exec` to run a test, use the command `xvfb-run -ac mvn …` (instead of `mvn …`).

### Using Selenium

For tests using [Selenium](https://www.selenium.dev/documentation/webdriver/){:target="_blank"}, WebDriver requires some options:

- Chromium  
    The `--no-sandbox` option is compulsory to use Chromium in headless mode.  
    Code sample: 
    ```java
    ChromeOptions chromeOptions = new ChromeOptions();
    chromeOptions.addArguments("--no-sandbox",
                               "--disable-dev-shm-usage",
                               "--disable-gpu",
                               "--headless");
    chromeOptions.setBinary("/usr/bin/chromium");
    WebDriver driver = new ChromeDriver(chromeOptions);
    ```

- Firefox  
    The `-headless` and `-safe-mode` options are compulsory to run Firefox in headless mode.  
    Code sample: 
    ```java
    FirefoxOptions firefoxOptions = new FirefoxOptions();
    firefoxOptions.addArguments("-headless",
                                "-safe-mode");
    WebDriver driver = new FirefoxDriver(firefoxOptions);
    ```

### Passing arguments

The usual `mvm test -Dvariable=<value>` syntax can be used. Parameters can be retrieved in the test code with `System.getProperty("variable")`.

Example: 

```java
String browser = System.getProperty("browser");
switch (browser) {
    case "chrome":
        driver = new ChromeDriver();
        break;
     case "firefox":
        driver = new FirefoxDriver();  
        break;
    default:
        throw new IllegalArgumentException("Browser \"" + browser + "\" is not supported.");
}
```
allows to select with the `mvn` command line parameter `-Dbrowser=firefox` or `-Dbrowser=chrome` which browser to use. 

### Using a specific Maven configuration

Maven configurations are stored in a `settings.xml` file as described in the [Maven documentation](https://maven.apache.org/settings.html){:target="_blank"}.

In order to use your own `settings.xml`, you need to overwrite the `/usr/share/maven/conf/settings.xml` file.

- in Docker, by using a volume:  
`-v $(pwd)/settings.xml:/usr/share/maven/conf/settings.xml`.

- in Kubernetes, by using a ConfigMap: 
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    ....
    spec:
        ...
          containers:
            image: opentestfactory/maven-runner:X.Y.Z
            name: maven-runner
            volumeMounts:
            - mountPath: /usr/share/maven/conf/settings.xml
              subPath: settings.xml
              name: settings-volume
          volumes:
            - name: settings-volume
              configMap:
                name: settings-mvn
    ```
