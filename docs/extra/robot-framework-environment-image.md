<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Robot Framework Environment

In order to simplify the execution of Robot Framework tests, OpenTestFactory proposes a Docker image of such an execution environment: [opentestfactory/robot-framework-runner](https://hub.docker.com/r/opentestfactory/robot-framework-runner){:target="_blank"}.

## Image content

### Robot Framework libraries

- [Robot Framework](https://robotframework.org/){:target="_blank"} 6.1.1
- [Browser library](https://robotframework-browser.org/){:target="_blank"} 
  ([keywords](https://marketsquare.github.io/robotframework-browser/Browser.html){:target="_blank"}) 18.3.0:
  testing of Web applications
- [Appium library](https://github.com/serhatbolsu/robotframework-appiumlibrary){:target="_blank"}
  ([keywords](http://serhatbolsu.github.io/robotframework-appiumlibrary/AppiumLibrary.html){:target="_blank"}) 2.0.0:
  testing of mobile applications
- [RESTinstance](https://github.com/asyrjasalo/RESTinstance/){:target="_blank"}
  ([keywords](https://asyrjasalo.github.io/RESTinstance/){:target="_blank"}) 1.3.0:
  testing of JSON Web services
- [RequestsLibrary](https://github.com/MarketSquare/robotframework-requests){:target="_blank"}
  ([keywords](https://marketsquare.github.io/robotframework-requests/doc/RequestsLibrary.html){:target="_blank"}) 0.9.7:
   testing of Web services (this is a wrapper of the [Python Requests Library](https://requests.readthedocs.io/en/latest/){:target="_blank"})
- [squash-tf-services](https://pypi.org/project/squash-tf-services/){:target="_blank"}
  ([keywords](https://autom-devops-en.doc.squashtest.com/latest/autom/techno/robotframework.html#parameters-usage){:target="_blank"}) 2.0.0:
  retrieval of parameters defined by a ‘params’ function
- [Allure Robot Framework Listener](https://pypi.org/project/allure-robotframework/){:target="_blank"} 2.13.5:
  required by OpenTestFactory in order to generate Allure reports
- [lxml](https://lxml.de/){:target="_blank"} 5.0.0:
  Python xml parser

### Tools

- Xvfb
- openssh-server 
- x11vnc

## Using the image

### User profile

The image is designed to support running tests as `otf`. Running tests as `root` or any other non-`otf` will result in issues.

### Starting the image

```bash
docker run -d \
  --name robotenv \
  --env PASSWORD_ACCESS=true \
  --env USER_PASSWORD=<secret password> \
  opentestfactory/robot-framework-runner:latest
```

*Note:* If `USER_PASSWORD` is not specified, the default password is `otf`.

### Running a test via `ssh`

(We assume the test is accessible from within the image.)

```bash
ssh otf@<IP of the container> robot test_example.robot
```

The `ssh` port is 22 by default. Use the `SSHD_PORT` environment variable to change this value: `docker run -d … --env SSHD_PORT=<your-port> …` to change it. 

#### Xvfb
When using `ssh` or `docker exec` to run a test, the display 99 is loaded.

### Using Chromium browser

Browser Library is built on top of Playwright. In order to be able to test with Chromium and Playwright inside a container, using a seccomp profile security settings is recommended. The `seccomp_profile.json` can be retrieved [here](https://gitlab.com/henixdevelopment/open-source/opentestfactory/test-environments/robotframework_environment/-/blob/main/seccomp_profile.json){:target="_blank"}. Some explanations can be found in [Playwright documentation](https://github.com/microsoft/playwright/blob/master/docs/src/docker.md#crawling-and-scraping){:target="_blank"}.

*Note:* A seccomp profile is a security feature in Linux that restricts the system calls a process can make, effectively reducing the attack surface and preventing malicious or unintended actions.

#### Using seccomp profile with Docker

Seccomp profile can be added with the `--security-opt seccomp=seccomp_profile.json` flag. ([Docker documentation](https://docs.docker.com/engine/security/seccomp/))

Using `--ipc=host` is recommended, otherwise Chromium may run out of memory.

Docker CLI example:

``` bash
docker run -d \
  --name robotenv \
  --env PASSWORD_ACCESS=true \
  --env USER_PASSWORD=<secret password> \
  --ipc=host \
  --security-opt seccomp=path/to/seccomp_profile.json \
  opentestfactory/robot-framework-runner:latest
```

#### Using seccomp profile with Kubernetes

Seccomp profile can be applied in Kubernetes. More detail of installation on the [seccomp tutorial documentation](https://kubernetes.io/docs/tutorials/security/seccomp/).

On cloud managed instance, adding the file on the node instance is not directly possible. To work around the limitation, the seccomp_profile.json file is loaded inside an image used as an initContainer.

An image with a seccomp_profile.json is available on the [dockerhub opentestfactory/robot-framework-seccomp-profile repository](https://hub.docker.com/r/opentestfactory/robot-framework-seccomp-profile). (not maintained)

Kubernetes manifest example:

``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: example
spec:
  selector:
    ...
  template:
    metadata:
      ...
    spec:
      containers:
      - env:
        ...
        image: opentestfactory/robot-framework-runner
        name: ssh-robotfw
        securityContext:
            seccompProfile:
              type: Localhost
              localhostProfile: seccomp_profile.json # <-- Add the seccomp profile file here
      volumes:
        - name: seccomp-profiles
          hostPath: # <-- Mount the file on the host node
            path: /var/lib/kubelet/seccomp 
      initContainers:
        - name: seccomp-profile-initializer
          image: opentestfactory/robot-framework-seccomp-profile
docker.squashtest.org/forge/robotfw-seccomp-init:1.0.0
          securityContext:
            allowPrivilegeEscalation: false
            # run as root user
            runAsUser: 0
            runAsGroup: 0
          command: ["/bin/sh"]
          args: ["-c","cp seccomp_profile.json /var/lib/kubelet/seccomp/seccomp_profile.json"]
          volumeMounts:
            - mountPath: /var/lib/kubelet/seccomp
              name: seccomp-profiles
```
