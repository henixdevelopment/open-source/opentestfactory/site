<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Extra components

Some auxiliary components are provided by the OpenTestFactory project, but out of its core.  
They are components used internally by the project. While we believe they may be useful to other teams, we cannot commit to improve/maintain/fix them diligently. Nonetheless, we will make our best effort to achieve this.

- [Docker image for executing Cucumber JVM and JUnit tests](maven-environment-image.md)
- [Docker image for executing Robot Framework tests](robot-framework-environment-image.md)
