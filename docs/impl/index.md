---
title: Orchestrator Reference Implementation
---
<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Orchestrator Reference Implementation

Automate, customize, and execute your testing workflows right in your repository
with the OpenTestFactory orchestrator.  You can discover, create, and share plugins
to perform any job you'd like, and combine plugins in a completely
customized workflow.

[Quickstart](../quickstart.md){: .md-button .md-button--primary }
[Reference guides](reference/index.md){: .md-button }

## Getting started

### [About OpenTestFactory orchestrator](../learn-opentf-orchestrator/index.md)

The orchestrator enables you to create custom software development life
cycle (SDLC) workflows directly in your source code repository.

### [About OpenTestFactory orchestrator plugins](../plugins/about-plugins.md)

Generators and providers plugins are individual tasks that you can combine to
create jobs and customize your workflow.  You can create your own generators and
providers plugins or use and customize plugins shared by the OpenTestFactory
community.

## Popular articles

### [Installation](../installation.md)

The OpenTestFactory orchestrator is a set of services running together. They may
or may not run on the same machine, and they may or may not be started at the
same time.

### [Workflow syntax](../specification/workflows.md)

A workflow is a configurable automated process made up of one or more jobs.  You
must create a YAML file to define your workflow configuration.

### [Expressions](../specification/expressions.md)

You can access context information and evaluate expressions in workflows and
plugins.

### [Hooks for jobs and providers](reference/hooks.md)

Hooks define steps that run before and/or after a specific event occurs.
