<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Overview

Reference documentation for creating workflows and quality gates.

# Workflow syntax

The workflow file is written in YAML. In that file, you can use expression syntax
to evaluate contextual information, literals, operators, and functions. Contextual information
includes workflow, resources, and environment variables. When you use `run` in a workflow step
to run shell commands, you can use specific workflow command syntax to set environment
variables, set output parameters for subsequent steps, and set error or debug messages.

- [Workflow syntax for OpenTestFactory Orchestrator](workflows.md)
- [Contexts](contexts.md)
- [Expressions](expressions.md)
- [Workflow commands for OpenTestFactory Orchestrator Plugins](workflow-commands.md)

# Environment variables

The OpenTestFactory orchestrator sets default environment variables for each OpenTestFactory workflow run. You can also set custom environment variables in your workflow file.

- [Environment variables](using-environment-variables.md)

# Quality gate syntax

The OpenTestFactory includes a quality gate service. It applies quality gate definitions
(rule sets) to evaluate workflow execution results. These definitions, written in YAML, use 
workflow expression syntax with `test` context to set rules scopes.

- [Quality gate syntax](qualitygate-syntax.md)
