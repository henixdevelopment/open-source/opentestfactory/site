<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Playwright

This plugin provides functions that handle Playwright tests.  It has been
validated with version 1.41 and should work with any recent version of Playwright.

It can be used directly in a workflow, or indirectly via generators (such as those
providing access to test case managers).

A working Playwright Node.js global installation must be available in the targeted
execution environments.

The functions have a `playwright` category prefix.


## Functions

<p> <a href="#playwrightnpxv1" title="playwright/npx@v1"><i data-feather="play"></i> </a> <a href="#playwrightexecutev1" title="playwright/execute@v1"><i data-feather="play-circle" stroke="red"></i> </a> <a href="#playwrightparamsv1" title="playwright/params@v1"><i data-feather="plus-circle" stroke="red"></i> </a>  </p>

### <i data-feather="play"></i> playwright/npx@v1

Run a Playwright test suite(s) or test case(s).

You can use the `extra-options` input to pass additional parameters
to the `npx playwright test` command.

If the function is _not_ preceded by a repository checkout (`actions/checkout@v2`),
the `working-directory` keyword must be used to indicate the directory where the
Playwright project folder is stored.

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference. Playwright handles it as a regular expression, but it is
    up to the caller to adapt the reference to the execution environment.

- `grep` (optional)

    The regular expression to filter executed tests. This expression is quoted
    by default, but it is up to the caller to adapt it to the execution
    environment. If the expression is quoted by the caller, it will be de-quoted and
    then re-quoted according to the execution environment, with only the outermost
    quotes being removed.

- `browser` (optional)

    Run test in a specific browser. Available options are "chromium", "firefox", "webkit" or
    "all" to run tests in all three browsers at the same time.

- `reporters` (optional)

    The list of desired reporters, if any (the list may be empty).
    The possible reporters are:
    - JUnit (noted `junit`, generating a `pw_junit_report.xml` report).
    - HTML (noted `html`, generating an `index.html` report).
    Any, all, or none of those reporters can be added to the list of desired generated reports.
    
    Note that reporters specified _via_ this option override reporters specified on the project
    configuration level.

- `extra-options` (optional)

    Specify additional parameters to pass to Playwright.
    
    There is a list of possible command line options you can find on the
    <a href="https://playwright.dev/docs/test-cli" target="_blank">Command line</a>
    chapter of the Playwright documentation.
    
    Pay attention not to pass `--reporter` option when using `reporters` input: it creates
    a workflow execution error.

#### Reports

The function generates the following reports:

- `pw_junit_report.xml`

    The Playwright test execution JUnit report.
    
    It has the `application/vnd.opentestfactory.playwright-output+xml`
    content type.

- `index.html`

    The Playwright test execution HTML report.
    
    It has the `application/vnd.opentestfactory.playwright-output+html`
    content type.

#### Examples

This first example runs all tests in the `example.spec.ts` test suite
and attaches both JUnit and HTML reports to the execution.

```yaml
- uses: playwright/npx@v1
  with:
    test: my_pw_project/tests/example.spec.ts
    reporters: ['junit', 'html']
  working-directory: /path/to/my/projects
```

This second example runs only tests having "should open" in their title from
the `sample.spec.ts` test suite in Chromium. A HTML report is attached to the
execution:

```yaml
- uses: playwright/npx@v1
  with:
    test: my_pw_project/tests/sample.spec.ts
    grep: should open
    browser: chromium
    reporters: ['html']
```

This third example lists all tests from the test suites in `my_pw_project/tests_2`
directory without executing them:

```yaml
- uses: playwright/npx@v1
  with:
    test: my_pw_project/tests_2/*.spec.ts
    extra-options: --list
```

### <i data-feather="play-circle" stroke="red"></i> playwright/execute@v1

An `execute` function for use by generators.  Runs a test suite or a
test case in a test suite.

#### Test Reference format

The test reference format used by `playwright/execute@v1` is as follows:

- `{project}[/{directory}]#{specfile}[#{grep}]`

With:

- `{project}` (required): name of the project on the source code repository.
- `{directory}` (optional): path to the `{specfile}` from the root of the project.
- `{specfile}` (required): name of the `.spec.ts` test suite file to execute.
- `{grep}` (optional): regular expression to filter executed test cases against.

!!! warning
    Playwright handles `specfile` and `grep` as regex patterns. It is up to the user
    to provide correct patterns depending on the execution environment. The `grep` is
    quoted by default. If the expression is quoted by the caller, it will be de-quoted
    and then re-quoted according to the execution environment, with only the outermost
    quotes being removed.

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference.

#### Reports

The function generates the following reports:

- `pw_junit_report.xml`

    The Playwright test execution JUnit report.
    
    It has the `application/vnd.opentestfactory.playwright-output+xml`
    content type.

- `index.html`

    The Playwright test execution HTML report.
    
    It has the `application/vnd.opentestfactory.playwright-output+html`
    content type.

#### Configuration file

By default, the Playwright test runner executes tests from outside the `{project}` directory
(at n-1 level), so it does not automatically consider any project-specific configuration
file. If you want to specify the configuration file to use, you can pass it to the
`PLAYWRIGHT_EXTRA_OPTIONS` environment variable in your workflow:

```yaml
metadata:
name: Playwright provider references test
jobs:
  playwright-execute-test:
    runs-on: [playwright, linux]
    variables:
      PLAYWRIGHT_EXTRA_OPTIONS:
        value: --config my_pw_project/playwright.config.ts
    steps:
    - uses: playwright/execute@v1
      with:
        test: my_pw_project/tests#example.spec.ts#has title
```

Alternatively, you can use `working-directory` parameter to indicate where the runner
should execute the tests. If this directory contains a configuration file, it will be
used by the runner:

```yaml
[...]
steps:
- uses: playwright/execute@v1
  with:
    test: my_pw_project/tests#example.spec.ts#has title
  working-directory: my_pw_project
```

!!! warning
    `testDir` parameter in the configuration file limits test execution scope. If it
    differs from the test reference `{directory}` parameter, tests may not be
    executed.

#### Customization

The `PLAYWRIGHT_EXTRA_OPTIONS` environment variable can be used to pass additional
parameters to the `npx playwright test` command.

If defined it will be appended to the end of command line.

The following parameters and Playwright-specific environment variables are used in the
provider-generated `npx playwright test` command:

```bash
export PW_TEST_HTML_REPORT_OPEN=never \
export PLAYWRIGHT_HTML_REPORT=playwright-report \
export PLAYWRIGHT_JUNIT_OUTPUT_NAME=pw_junit_report.xml \
npx playwright test {project}[/{directory}]/{specfile} \
  --grep="{grep}" \
  --reporter=html,junit \
  $PLAYWRIGHT_EXTRA_OPTIONS \
```

You must avoid passing, _via_ the `PLAYWRIGHT_EXTRA_OPTIONS` variable, the command line
parameters that conflict with the parameters already used, or the parameters
that impact the generation or alter the path of the reports expected by the orchestrator
(view ["Reports" section](#reports_1)).

#### Examples

This first example runs `example.spec.ts` test suite at the root of the project:

```yaml
- uses: playwright/execute@v1
  with:
    test: my_pw_project#example.spec.ts
```

This second example runs all test suites in `my_pw_project` project `data2` directory:

```yaml
- uses: playwright/execute@v1
  with:
    test: my_pw_project/data2#*
```

This third example runs all test cases containing "should fail" in their name from
the `sample.spec.ts` test suite in the project `foo` directory:

```yaml
- uses: playwright/execute@v1
  with:
    test: my_pw_project/foo#samples.spec.ts#should fail
```

### <i data-feather="plus-circle" stroke="red"></i> playwright/params@v1

A `params` function for use by generators.

#### Inputs

The function has the following inputs:

- `data` (required)

    The data to use for the automated test.

- `format` (required)

    The format to use for the automated test data.
    
    `format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

#### Example

```yaml
- uses: playwright/params@v1
  with:
    data:
      global:
        key1: value1
        key2: value2
      test:
        key1: value3
        key3: value4
    format: format
```

`format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

`data` can have two keys:

- `global` for defining global parameters.
- `test` for defining test parameters.

<script src="../../js/feather.min.js"></script>
<script>feather.replace()</script>

## Configuration

[Hooks](../guides/hooks.md) can be defined for the provided functions. This can be done in workflow definitions or at the orchestrator level so that they apply to all your workflows.

Configuration at the orchestrator level is done by setting the
`PLAYWRIGHT_PROVIDER_HOOKS` environment variable.

Please refer to "[Common Provider Settings](../plugins/configuring-a-provider-plugin.md)" for more information.


