<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Cucumber JVM

This plugin provides functions that handle Cucumber JVM tests.  It has been
validated with Cucumber-JVM 4.2.6, 5.7.0, 6.11.0, and 7.0.0. Any recent version
should work properly.

Cucumber tests in languages other than Java are not supported.

It can be used directly in a workflow, or indirectly via generators (such as those
providing access to test case managers).

The functions have a `cucumber` or `cucumber5` category prefix.


## Functions

<p> <a href="#cucumber5cucumberv1" title="cucumber5/cucumber@v1"><i data-feather="play"></i> </a> <a href="#cucumbercucumberv1" title="cucumber/cucumber@v1"><i data-feather="play"></i> </a> <a href="#cucumber5executev1" title="cucumber5/execute@v1"><i data-feather="play-circle" stroke="red"></i> </a> <a href="#cucumberexecutev1" title="cucumber/execute@v1"><i data-feather="play-circle" stroke="red"></i> </a> <a href="#cucumber5paramsv1" title="cucumber5/params@v1"><i data-feather="plus-circle" stroke="red"></i> </a> <a href="#cucumberparamsv1" title="cucumber/params@v1"><i data-feather="plus-circle" stroke="red"></i> </a>  </p>

### <i data-feather="play"></i> cucumber5/cucumber@v1

Run a Cucumber JVM 5 or later feature file.

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference to use.

- `tag` (optional)

    Select a specific tagged scenario or dataset.

- `tags` (optional)

    Select a specific set of tagged scenarios or datasets.

- `reporters` (optional)

    The list of desired reporters, if any (the list may be empty).
    
    The possible reporters are:
    
    - JUnit (noted `junit`, generating a `report.xml` report).
    - HTML (noted `html`, generating a `html-report.html` report).
    
    Any, all, or none of those reporters can be added to the list of desired generated
    reports.  Note that without the JUnit reporter, the Cucumber test result will not
    appear in Allure reports.

- `extra-options` (optional)

    Additional parameters to pass to Cucumber.
    
    There is a list of supported properties you can find on the
    <a href="https://cucumber.io/docs/cucumber/api/#options" target="_blank">Options chapter</a>
    of the Cucumber documentation. To properly add an option within the `extra-options`
    tag, use the syntax below:
    
    - `-Dcucumber.[module].[param]=[value]`
    
    Module is optional as in `-Dcucumber.glue=com.example.glue` but can be mandatory
    as in `-Dcucumber.ansi-colors.disabled=true`
    
    Please note that we use several properties within our implementation, thus we do
    not recommend adding them to your extra options:
    
    - `cucumber.filter.tags` (used in the `tag` option)
    - `cucumber.plugin` (used in the `reporters` list)
    - `cucumber.features` (used in the `test` option)

#### Reports

The function generates the following reports:

- `report.xml`

    (only if the `junit` reporter has been specified)  A Surefire report (XML).
    
    The Surefire report has the `application/vnd.opentestfactory.cucumber-surefire+xml`
    content type.

- `html-report.tar`

    (only if the `html` reporter has been specified) A TAR archive containing the
    `html-report.html` report.

#### Test Reference format

The `test` input has the following format:

- `{project}#{featurefile}#{feature}#{scenario}`

With:

- `{project}` (required): the path of the directory containing the `pom.xml` file.
- `{featurefile}` (required): the path to the Cucumber test file, with the `.feature`
  extension.
- `{feature}` (optional): the feature name as specified in the Cucumber test file.
- `{scenario}` (optional): the scenario name as specified in the Cucumber test file.

`{feature}` and `{scenario}` are optional, they may be empty. They will not be used
while driving the test: the whole `.feature` file will be executed.

However, these additional elements are used by publishers to extract and report the
execution status of only the targeted feature or scenario.

!!! note
    `{feature}` could seem useless given the fact that there can be only one feature
    in a `.feature` file, but it is required because of a technical limitation: the name
    of the `.feature` file is absent from the report, only the feature name is present.

!!! information
    Old versions of the orchestrator (up to 3.7.0 included) only supported the following
    `test` syntax:

    ```yaml hl_lines="3"
    - uses: cucumber5/cucumber@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature#MyFeature#Selected scenario name
        tags: "@tag1 and @tag2"
        reporters:
        - junit
        - html
        extra-options: "-Dcucumber.ansi-colors.disabled=true"
    ```

    That old syntax is still accepted, but it has been deprecated because not applicable
    if the `pom.xml` file is not at the root of the Git repository (this is typically
    the case for multi-projects Git repositories).

#### Tag expressions

The `tag` field is optional, allowing you to select one or many specific tagged
scenarios or datasets within the `.feature` file. Please note that the `tag` value must
not include the initial `@` character.

Since Cucumber 5.7.0 (see the details in the
<a href="https://github.com/cucumber/cucumber-jvm/blob/main/CHANGELOG.md#570-2020-05-01" target="_blank">changelog</a>),
tag expressions are allowed, to select scenarios in a more specific way.

To use this feature, you can fill the `tags` field using the following sample formats:

- `"@tag1 @tag2"`: will execute scenarios tagged by `@tag1` **or** `@tag2`.
- `"@tag1 and @tag2"`: will execute scenarios tagged by **both** `@tag1` **and** `@tag2`.
- `"@tag1 and not @tag2"`: will execute scenarios tagged by `@tag1` **but not** `@tag2`.
- `"not (@tag1 or @tag2)"`: will execute scenarios tagged by **neither** `@tag1` **nor**
  `@tag2`.

Double quotes are mandatory.

For more details about Cucumber tag expressions, please read the
<a href="https://cucumber.io/docs/cucumber/api/#tags" target="_blank">tag chapter</a>
in the Cucumber documentation.

!!! warning
    Please note that `tag` and `tags` fields cannot coexist, thus using both of them
    in the same job will return an error.

#### Example

```yaml
- uses: cucumber5/cucumber@v1
  with:
    test: full/path/to/cucumberProject#src/test/java/features/sample.feature#MyFeature#Selected scenario name
    tags: "@tag1 and @tag2"
    reporters:
    - junit
    - html
    extra-options: "-Dcucumber.ansi-colors.disabled=true"
```

### <i data-feather="play"></i> cucumber/cucumber@v1

Process 'cucumber' function for Cucumber JVM versions up to 4 included.

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference to use.

- `tag` (optional)

    Select a specific tagged scenario or dataset.  Please note that the `tag` value must
    not include the initial `@` character.

- `reporters` (optional)

    The list of desired reporters, if any (the list may be empty).
    
    The possible reporters are:
    
    - JUnit (noted `junit`, generating a `report.xml` report).
    - HTML (noted `html`, generating a `html-report.html` report).
    
    Any, all, or none of those reporters can be added to the list of desired generated
    reports.  Note that without the JUnit reporter, the Cucumber test result will not
    appear in Allure reports.

- `extra-options` (optional)

    Additional parameters to pass to the Cucumber's `cucumber.options` property.

#### Reports

The function generates the following reports:

- `report.xml`

    (only if the `junit` reporter has been specified)  A Surefire report (XML).
    
    The Surefire report has the `application/vnd.opentestfactory.cucumber-surefire+xml`
    content type.

- `html-report.tar`

    (only if the `html` reporter has been specified) A TAR archive containing the HTML
    report (`index.html` with its JavaScript and CSS files).

#### Test Reference format

The `test` input has the following format:

- `{project}#{featurefile}#{feature}#{scenario}`

With:

- `{project}` (required): the path of the directory containing the `pom.xml` file.
- `{featurefile}` (required): the path to the Cucumber test file, with the `.feature`
  extension.
- `{feature}` (optional): the feature name as specified in the Cucumber test file.
- `{scenario}` (optional): the scenario name as specified in the Cucumber test file.

`{feature}` and `{scenario}` are optional, they may be empty. They will not be used
while driving the test: the whole `.feature` file will be executed.

However, these additional elements are used by publishers to extract and report the
execution status of only the targeted feature or scenario.

!!! note
    `{feature}` could seem useless given the fact that there can be only one feature
    in a `.feature` file, but it is required because of a technical limitation: the name
    of the `.feature` file is absent from the report, only the feature name is present.

!!! information
    Old versions of the orchestrator (up to 3.7.0 included) only supported the following
    `test` syntax:

    ```yaml hl_lines="3"
    - uses: cucumber/cucumber@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature#MyFeature#Selected scenario name
        tag: tag1
        reporters:
        - junit
        - html
    ```

    That old syntax is still accepted, but it has been deprecated because not applicable
    if the `pom.xml` file is not at the root of the Git repository (this is typically
    the case for multi-projects Git repositories).

#### Example

```yaml
- uses: cucumber/cucumber@v1
  with:
    test: full/path/to/cucumberProject#src/test/java/features/sample.feature##
    tag: tag1
    reporters:
    - junit
    - html
    extra-options: extra
```

### <i data-feather="play-circle" stroke="red"></i> cucumber5/execute@v1

An `execute` function for use by generators for Cucumber JVM 5 or later.  Runs a feature
file.

#### Test Reference format

The test reference format used by `cucumber5/execute@v1` is as follows:

- `{project}#{featurefile}#{feature}#{scenario}`

With:

- `{project}` (required): the path of the directory containing the `pom.xml` file.
- `{featurefile}` (required): the path to the Cucumber test file, with the `.feature`
  extension.
- `{feature}` (optional): the feature name as specified in the Cucumber test file.
- `{scenario}` (optional): the scenario name as specified in the Cucumber test file.

`{feature}` and `{scenario}` are optional, they may be empty. They will not be used
while driving the test: the whole `.feature` file will be executed.

However, these additional elements are used by publishers to extract and report the
execution status of only the targeted feature or scenario.

!!! note
    `{feature}` could seem as useless given the fact that there can be only one feature
    in a `.feature` file, but it is required because of a technical limitation: the name
    of the `.feature` file is absent from the report, only the feature name is present.

!!! information
    Old versions of the orchestrator (up to 3.7.0 included) only supported the following
    `test` syntax:

    ```yaml hl_lines="3"
    - uses: cucumber5/execute@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature#MyFeature#Selected scenario name
    ```

    That old syntax is still accepted, but it has been deprecated because not applicable
    if the `pom.xml` file is not at the root of the Git repository (this is typically
    the case for multi-projects Git repositories).

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference.

#### Reports

The function generates the following reports:

- `report.xml`

    A Surefire report (XML).
    
    The Surefire report has the `application/vnd.opentestfactory.cucumber-surefire+xml`
    content type.

- `html-report.tar`

    A TAR archive. Contains the usual report:
    
    - `html-report.html`

#### Customization

The `CUCUMBER_EXTRA_OPTIONS` environment variable can be used to pass additional
parameters to the `mvn test` command. To properly add an option, use the following syntax:

    -Dcucumber.[module].[param]=[value]

The following parameters are used in the provider-generated `mvn test` command:

```bash
mvn test \
  -f "{POM_path}" -Dmaven.test.failure.ignore=true \
  -Dcucumber.features="{feature_path}" \
  -Dcucumber.plugin="html:{html_report_path},junit:{junit_report_path}" \
  -Dcucumber.filter.tags="{Squash_TM_tags}" $CUCUMBER_EXTRA_OPTIONS
```

You must avoid passing, _via_ the `CUCUMBER_EXTRA_OPTIONS` variable, the command line
parameters that conflict with the parameters already used, or the parameters
that impact the generation or alter the path of the reports expected by the orchestrator
(view ["Reports" section](#reports_2)).

#### Example

```yaml
- uses: cucumber5/execute@v1
  with:
    test: full/path/to/cucumberProject#src/test/java/features/sample.feature#feature_name#scenario
```

### <i data-feather="play-circle" stroke="red"></i> cucumber/execute@v1

An `execute` function for use by generators for Cucumber JVM versions up to 4 included.
Runs a feature file.

#### Test Reference format

The test reference format used by `cucumber/execute@v1` is as follows:

- `{project}#{featurefile}#{feature}#{scenario}`

With:

- `{project}` (required): the path of the directory containing the `pom.xml` file.
- `{featurefile}` (required): the path to the Cucumber test file, with the `.feature`
  extension.
- `{feature}` (optional): the feature name as specified in the Cucumber test file.
- `{scenario}` (optional): the scenario name as specified in the Cucumber test file.

`{feature}` and `{scenario}` are optional, they may be empty. They will not be used
while driving the test: the whole `.feature` file will be executed.

However, these additional elements are used by publishers to extract and report the
execution status of only the targeted feature or scenario.

!!! note
    `{feature}` could seem useless given the fact that there can be only one feature
    in a `.feature` file, but it is required because of a technical limitation: the name
    of the `.feature` file is absent from the report, only the feature name is present.

!!! information
    Old versions of the orchestrator (up to 3.7.0 included) only supported the following
    `test` syntax:

    ```yaml hl_lines="3"
    - uses: cucumber/execute@v1
      with:
        test: cucumberProject/src/test/java/features/sample.feature#MyFeature#Selected scenario name
    ```

    That old syntax is still accepted, but it has been deprecated because not applicable
    if the `pom.xml` file is not at the root of the Git repository (this is typically
    the case for multi-projects Git repositories).

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference to use.

#### Reports

The function generates the following reports:

- `report.xml`

    A Surefire report (XML).
    
    The Surefire report has the `application/vnd.opentestfactory.cucumber-surefire+xml`
    content type.

- `html-report.tar`

    A TAR archive containing the HTML report (`index.html` with its JavaScript and CSS
    files).

#### Customization

The `CUCUMBER_EXTRA_OPTIONS` environment variable can be used to pass additional
parameters to the Cucumber's `cucumber.options` property.

Some parameters are already defined in the provider-generated `mvn test` command:

```bash
mvn test \
  -f "{POM_path}" \
  -Dmaven.test.failure.ignore=true \
  -Dcucumber.options="{feature_path} \
  --plugin html:{html_report_path} \
  --plugin junit:{junit_report_path} \
  --tags {Squash_TM_tags} $CUCUMBER_EXTRA_OPTIONS"
```

You must avoid passing, _via_ the `CUCUMBER_EXTRA_OPTIONS` variable, the command line
parameters that conflict with the parameters already used, or the parameters
that impact the generation or alter the path of the reports expected by the orchestrator
(view ["Reports" section](#reports_3)).

#### Example

```yaml
- uses: cucumber/execute@v1
  with:
    test: full/path/to/cucumberProject#src/test/java/features/sample.feature#feature_name#scenario
```

### <i data-feather="plus-circle" stroke="red"></i> cucumber5/params@v1

A `params` function for use by generators, for Cucumber JVM versions 5 and later.

#### Inputs

The function has the following inputs:

- `data` (required)

    The data to use for the automated test.

- `format` (required)

    The format to use for the automated test data.  It must so far be SQUASHTM_FORMAT
    (`tm.squashtest.org/params@v1`).

#### Example

```yaml
- uses: cucumber5/params@v1
  with:
    data:
      global:
        key1: value1
        key2: value2
      test:
        key1: value3
        key3: value4
    format: tm.squashtest.org/params@v1
```

`data` can have two keys:

- `global` for defining global parameters.
- `test` for defining test parameters.

### <i data-feather="plus-circle" stroke="red"></i> cucumber/params@v1

A `params` function for use by generators, for Cucumber JVM versions up to 4 included.

#### Inputs

The function has the following inputs:

- `data` (required)

    The data to use for the automated test.

- `format` (required)

    The format to use for the automated test data.

#### Example

```yaml
- uses: cucumber/params@v1
  with:
    data:
      global:
        key1: value1
        key2: value2
      test:
        key1: value3
        key3: value4
   format: tm.squashtest.org/params@v1
```

`format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

`data` can have two keys:

- `global` for defining global parameters.
- `test` for defining test parameters.

<script src="../../js/feather.min.js"></script>
<script>feather.replace()</script>

## Using with inception

Please refer to "[Inception](https://opentestfactory.org/guides/inception.html)" for
more information on what inception is.

Preload the inception environment with at least the test execution report data.

### Example

Assuming the following workflow and an existing execution report `report.xml`:

```yaml title="my_workflow.yaml"
metadata:
  name: Cucumber Inception
resources:
  files:
  - surefire-report
jobs:
  my_job:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        report.xml: ${{ resources.files.surefire-report }}
    - uses: cucumber/execute@v1
      with:
        test: OpenWeather#src/openweather.feature##
```

You can use the following command to run it:

=== "Bash"

    ```bash
    opentf-ctl \
        run workflow my_workflow.yaml -f surefire-report=report.xml
    ```

=== "CMD"

    ```bat
    opentf-ctl ^
        run workflow my_workflow.yaml -f surefire-report=report.xml
    ```

=== "PowerShell"

    ```powershell
    opentf-ctl `
        run workflow my_workflow.yaml -f surefire-report=report.xml
    ```

## Configuration

[Hooks](../guides/hooks.md) can be defined for the provided functions. This can be done in workflow definitions or at the orchestrator level so that they apply to all your workflows.

Configuration at the orchestrator level is done by setting the
`CUCUMBER_PROVIDER_HOOKS` environment variable.

Please refer to "[Common Provider Settings](../plugins/configuring-a-provider-plugin.md)" for more information.


