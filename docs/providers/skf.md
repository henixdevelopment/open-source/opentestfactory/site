<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# SKF

This plugin provides functions that handle SKF tests.  It has been validated with SKF
1.14.0 and should work with any recent version of SKF.

It can be used directly in a workflow, or indirectly via generators (such as those
providing access to test case managers).

A working SKF installation must be available in the targeted execution environments.

The functions have an `skf` category prefix.


## Functions

<p> <a href="#skfskfv1" title="skf/skf@v1"><i data-feather="play"></i> </a> <a href="#skfexecutev1" title="skf/execute@v1"><i data-feather="play-circle" stroke="red"></i> </a> <a href="#skfparamsv1" title="skf/params@v1"><i data-feather="plus-circle" stroke="red"></i> </a>  </p>

### <i data-feather="play"></i> skf/skf@v1

Run an SKF test suite.

#### Inputs

The function has the following inputs:

- `root-project` (required)

    Path to the root SKF folder (which contains the `pom.xml` file).

- `tests` (optional)

    Specifies a list of _scripts_, each script having a `script` entry
    specifying the path of the script to run from the default ecosystem
    ('tests').
    
    Each `script` item may also have a `data` entry with a series of
    `params: values` elements.
    
    If `tests` is not specified, all test cases in the root project are executed.

- `data` (optional)

    Specifies parameters to pass to a specific test script.

- `global-parameters` (optional)

    Specifies global parameters.

- `testsuite` (optional)

    Specifies the path from the `root-project` to a whole test suite defined in an
    existing JSON file in SKF format.  If this parameter is used, no other optional
    parameter will be taken into account.

#### Reports

The function generates the following reports:

- `*.xml`

    Surefire reports (XML).
    
    All XML files in the `{root_project}/target/squashTA/surefire-reports` folder are
    attached.
    
    The surefire reports have the `application/vnd.opentestfactory.skf-surefire+xml`
    content type.

- `html-reports.tar`

    A TAR archive.  Contains all files present in the `{root_project}/target/squashTA/html-reports` folder.

- `*.txt`

    Reports formatted as text.
    
    All TXT files in the `{root_project}/target/squashTA/surefire-reports` folder are
    attached.

#### Examples

This first example runs all tests in the `foobar` test directory:

```yaml
- uses: skf/skf@v1
  with:
    root-project: foobar
```

This second example runs the `foo` and `bar` test scripts in the `foobar` test
directory and passes `tests` and `global-parameters`:

```yaml
- uses: skf/skf@v1
  with:
    root-project: foobar
    tests:
      - script: foo
        data:
          test_param1: test_value1
          test_param2: test_value2
      - script: sub_ecosystem/bar
        data:
          test_param1: test_value1
          test_param2: test_value2
    global-parameters:
      global_param1: global_value1
      global_param2: global_value2
```

This third example runs all tests defined in the JSON test suite file:

```yaml
- uses: skf/skf@v1
  with:
    root-project: foobar
    testsuite: path to the JSON file from the root project
```

### <i data-feather="play-circle" stroke="red"></i> skf/execute@v1

An `execute` function for use by generators.  Runs a test suite or a test case in a test
suite.

#### Test Reference format

The test reference format used by `skf/execute@v1` is as follows:

- `{root_project}/{ecosystem}[.{sub_ecosystems}]#{script}`

Where:

- `{root_project}` (required): path to the root SKF folder (which contains the `pom.xml`
  file) on the source repository.
- `{ecosystem}` (required): default test ecosystem of the SKF project (always `tests`).
- `{sub_ecosystems}` (optional): child tests ecosystems may be added, separated by `.`
  (e.g. `foo` or `foo.bar`).
- `{script}` (required): name of the test script to run, with its `.ta` extension.

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference.

#### Reports

The function generates the following reports:

- `TEST-{ecosystem}{sub_ecosystems}.xml`

    Surefire reports (XML).
    
    The Surefire reports come from the `{root_project}/target/squashTA/surefire-reports/`
    folder.  They have the `application/vnd.opentestfactory.skf-surefire+xml` content
    type.

- `html-reports.tar`

    A TAR archive. Contains all files present in the `{root_project}/target/squashTA/html-reports` folder.

- `{ecosystem}{sub_ecosystems}.txt`

    Reports formatted as text, as found in the `{root_project}/target/squashTA/surefire-reports` folder.

- `{ecosystem}{sub_ecosystems}-output.txt`

    Output reports formatted as text, as found in the `{root_project}/target/squashTA/surefire-reports` folder.

#### Examples

In this first example, the `pom.xml` file is expected to be in the `project` folder.
The generated reports will be `TEST-tests.xml`, `html-reports.tar`, `tests.txt`, and
`tests-output.txt`.

```yaml
- uses: skf/execute@v1
  with:
    test: project/tests#case.ta
```

In this second example, the `pom.xml` file is expected to be in the `path/to/project`
folder.  The generated reports will be `TEST-tests.ecosystem1.xml`, `html-reports.tar`,
`tests.ecosystem1.txt`, and `tests.ecosystem1-output.txt`.

```yaml
- uses: skf/execute@v1
  with:
    test: path/to/project/tests.ecosystem1#case.ta
```

### <i data-feather="plus-circle" stroke="red"></i> skf/params@v1

A `params` function for use by generators.

#### Inputs

The function has the following inputs:

- `data` (required)

    The data to use for the automated test.

- `format` (required)

    The format to use for the automated test data.
    
    `format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

#### Example

```yaml
- uses: skf/params@v1
  with:
    data:
      global:
        key1: value1
        key2: value2
      test:
        key1: value3
        key3: value4
    format: format
```
`format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

`data` can have two keys:

- `global` for defining global parameters.
- `test` for defining test parameters.

<script src="../../js/feather.min.js"></script>
<script>feather.replace()</script>

## Using with inception

Please refer to "[Inception](https://opentestfactory.org/guides/inception.html)" for
more information on what inception is.

Preload the inception environment with at least the test execution report data.

### Example

Assuming the following workflow and two existing execution reports `report_1.xml` and
`report_2.xml`:

```yaml title="my_workflow.yaml"
metadata:
  name: SKF Inception
resources:
  files:
  - surefire-report1
  - surefire-report2
jobs:
  my_non_specific_job:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        TEST-tests.xml: ${{ resources.files.surefire-report1 }}
    - uses: skf/execute@v1
      with:
        test: OpenWeather/tests#forecast.ta
  my_specific_ecosystem_job:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        TEST-tests.ecosystem1.xml: ${{ resources.files.surefire-report2 }}
    - uses: skf/execute@v1
      with:
        test: OpenWeather/tests.ecosystem1#test.ta
```

You can use the following command to run it:

=== "Bash"

    ```bash
    opentf-ctl \
        run workflow my_workflow.yaml \
        -f surefire-report1=report_1.xml \
        -f surefire-report2=report_2.xml
    ```

=== "CMD"

    ```bat
    opentf-ctl ^
        run workflow my_workflow.yaml ^
        -f surefire-report1=report_1.xml ^
        -f surefire-report2=report_2.xml
    ```

=== "PowerShell"

    ```powershell
    opentf-ctl `
        run workflow my_workflow.yaml `
        -f surefire-report1=report_1.xml `
        -f surefire-report2=report_2.xml
    ```

## Configuration

[Hooks](../guides/hooks.md) can be defined for the provided functions. This can be done in workflow definitions or at the orchestrator level so that they apply to all your workflows.

Configuration at the orchestrator level is done by setting the
`SKF_PROVIDER_HOOKS` environment variable.

Please refer to "[Common Provider Settings](../plugins/configuring-a-provider-plugin.md)" for more information.


