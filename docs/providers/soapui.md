<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# SoapUI

This plugin provides functions that handle SoapUI tests.  It has been validated with
SoapUI v5.6.0 and should work with any recent version of SoapUI.

It can be used directly in a workflow, or indirectly via generators (such as those
providing access to test case managers).

A working SoapUI installation must be available in the targeted execution environments.

The functions have a `soapui` category prefix.


## Functions

<p> <a href="#soapuisoapuiv1" title="soapui/soapui@v1"><i data-feather="play"></i> </a> <a href="#soapuiexecutev1" title="soapui/execute@v1"><i data-feather="play-circle" stroke="red"></i> </a> <a href="#soapuiparamsv1" title="soapui/params@v1"><i data-feather="plus-circle" stroke="red"></i> </a>  </p>

### <i data-feather="play"></i> soapui/soapui@v1

Wraps the SoapUI TestRunner launcher.

It has a mandatory `project` input.

It generates one report per launched test suite.

!!! warning
    If the function is used more than once in a job, it is up to the caller to ensure no
    previous test execution results remain before executing a new test.

#### Inputs

The function has the following inputs:

- `project` (required)

    The project to run.  It is a path to an XML file relative to the current directory.

- `testsuite` (optional)

    The test suite in the project to run (`-s` option).
    If you skip this argument, the runner will execute all the test suites in your project.

- `testcase` (optional)

    The test case in the project to run (`-c` option).
    If you do not specify this argument, the runner will launch all the tests present in
    the parent test suite.

- `user` (optional)

    The user to use in test requests authorization (`-u` option).
    This argument overrides user names specified in your test project.

- `host` (optional)

    The host and port to use in test requests (`-h` option).
    You can specify the host by using its IP address or name.
    This argument overrides endpoints specified in the project file.

- `endpoint` (optional)

    The endpoint to use in test requests (`-e` option).
    This argument overrides the endpoints specified for TestSteps in your test project.

- `system-properties` (optional)

    Define the value(s) of system properties (`-D` options).
    The specified value overrides the variable value during the run.

- `global-properties` (optional)

    Define the value(s) of global properties (`-G` options).
    The specified value overrides the variable value during the run.

- `project-properties` (optional)

    Define the value(s) of project properties (`-P` options).
    The specified value overrides the variable value during the run.

- `target` (optional)

    The root directory where the runner saves reports (`-f` option).

- `extra-options` (optional)

    Any other option.

#### Reports

The function generates the following reports:

- `TEST-{testsuite}.xml`

    Surefire reports (XML).
    
    `{testsuite}` is the name of a test suite with spaces replaced by `_` and
    non-alphanumeric symbols removed.  If the project contains more than one test suite
    and no `testsuite` input is provided to narrow the scope of execution, there will be
    one Surefire report per test suite.
    
    The Surefire reports have the `application/vnd.opentestfactory.soapui-surefire+xml`
    content type.

#### Examples

This first example runs all test suites declared in the `foo.xml` SoapUI test file that
is in the current directory:

```yaml
- uses: soapui/soapui@v1
  with:
    project: foo.xml
```

This second example runs the `bar` test case of the `baz` test suite declared in the
`path/to/foo.xml` SoapUI test file (path relative to the current directory).  It also
defines properties and various other options:

```yaml
- uses: soapui/soapui@v1
  with:
    project: path/to/foo.xml
    testcase: bar
    testsuite: baz
    user: user
    host: host:port
    endpoint: https://host:port/foo
    system-properties:
      key1: value1
      key2: value2
    global-properties:
      key3: value3
      key4: value4
    project-properties:
      key5: value5
      key6: value6
    target: foo/bar
    extra-options: any other option
```

### <i data-feather="play-circle" stroke="red"></i> soapui/execute@v1

An 'execute' function for use by generators.  It attempts to run the specified test(s)
and attach the produced reports.

It has a mandatory `test` input, the test reference.

It generates one report per launched test suite.

#### Test Reference format

The test reference format used by `soapui/execute@v1` is as follow:

- `{project}/{path/to/testFile.xml}[#{testSuite}[#{testCase}]]`

Where:

- `{project}` (required): name of the project on the source code repository.
- `{path/to/testFile.xml}` (required): path and name of the SoapUI test file, from the
  root of the project (with the `.xml` extension).
- `{testSuite}` (optional): name of the test suite to execute.
- `{testCase}` (optional): name of the test case to execute.

!!! note
    If `{testSuite}` is not provided, all test suites in the SoapUI test file are
    executed.

    If `{testCase}` is not provided, all test cases in the specified test suite are
    executed.  `{testCase}` cannot be provided if `{testSuite}` is not provided.

#### Inputs

The function has the following inputs:

- `test` (required)

    The test reference.

#### Reports

The function generates the following reports:

- `TEST-{testsuite}.xml`

    Surefire reports (XML).
    
    `{testsuite}` is the name of a test suite with spaces replaced by `_` and
    non-alphanumeric symbols removed.  If the test reference does not narrow the
    execution scope to one test suite, there will be one Surefire report per
    test suite.
    
    The Surefire reports have the `application/vnd.opentestfactory.soapui-surefire+xml`
    content type.

#### Examples

This first example runs all tests present in "testcaseName" included in "testsuiteName":

```yaml
- uses: soapui/execute@v1
  with:
    test: project/path/to/test.xml#testsuiteName#testcaseName
```

This second example runs all tests present in all test cases included in
"testsuiteName":

```yaml
- uses: soapui/execute@v1
  with:
    test: project/path/to/test.xml#testsuiteName#
```

### <i data-feather="plus-circle" stroke="red"></i> soapui/params@v1

A 'params' function for use by generators.

#### Inputs

The function has the following inputs:

- `data` (required)

    The data to use for the automated test.

- `format` (required)

    The format to use for the automated test data.

#### Example

```yaml
- uses: soapui/params@v1
  with:
    data:
      global:
        key1: value1
        key2: value2
      test:
        key1: value3
        key3: value4
    format: format
```

`format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

`data` can have two keys:

- `global` for defining global parameters.
- `test` for defining test parameters.

<script src="../../js/feather.min.js"></script>
<script>feather.replace()</script>

## Using with inception

Please refer to "[Inception](https://opentestfactory.org/guides/inception.html)" for
more information on what inception is.

Preload the inception environment with at least the test execution report data.  If a
test suite name has been provided to the `soapui/execute@v1` or `soapui/soapui@v1`
functions, use it (replacing spaces with `_` and removing non-alphanumeric symbols).  If
no test suite has been provided, use `*` instead.

### Example

Assuming the following workflow and two existing execution reports `report_1.xml` and
`report_2.xml`:

```yaml title="my_workflow.yaml"
metadata:
  name: SoapUI Inception
resources:
  files:
  - surefire-report1
  - surefire-report2
jobs:
  my_specific_test_suite_job:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        TEST-Forecast_Suite.xml: ${{ resources.files.surefire-report1 }}
    - uses: soapui/execute@v1
      with:
        test: OpenWeather/OpenWeatherTest-project.xml#Forecast Suite#ForecastSuccess
  my_nonspecific_job:
    runs-on: inception
    steps:
    - uses: actions/prepare-inception@v1
      with:
        TEST-*.xml: ${{ resources.files.surefire-report2 }}
    - uses: soapui/execute@v1
      with:
        test: OpenWeather/OpenWeatherTest-project.xml
```

You can use the following command to run it:

=== "Bash"

    ```bash
    opentf-ctl \
        run workflow my_workflow.yaml \
        -f surefire-report1=report_1.xml \
        -f surefire-report2=report_2.xml
    ```

=== "CMD"

    ```bat
    opentf-ctl ^
        run workflow my_workflow.yaml ^
        -f surefire-report1=report_1.xml ^
        -f surefire-report2=report_2.xml
    ```

=== "PowerShell"

    ```powershell
    opentf-ctl `
        run workflow my_workflow.yaml `
        -f surefire-report1=report_1.xml `
        -f surefire-report2=report_2.xml
    ```

## Configuration

[Hooks](../guides/hooks.md) can be defined for the provided functions. This can be done in workflow definitions or at the orchestrator level so that they apply to all your workflows.

Configuration at the orchestrator level is done by setting the
`SOAPUI_PROVIDER_HOOKS` environment variable.

Please refer to "[Common Provider Settings](../plugins/configuring-a-provider-plugin.md)" for more information.


