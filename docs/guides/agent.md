---
tags:
  - agents
  - usage
---
<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Agents

Workflow jobs steps run on execution environments.  There are currently two ways the orchestrator can communicate with execution environments: SSH and agents.

SSH is the common choice if the execution environment is based on Linux or macOS.  It can also be used on Windows, but there are some limitations if the operations to perform require a graphic session.

Agents are an alternative that does not suffer from the above limitation, and so are more commonly seen in Windows contexts.

Agents may also be used in places where the execution environments change often or are not allowed to include an SSH server.

Finally, agents initiate the conversation with the orchestrator, which may be preferred in some contexts: there is no need to have an open port so that the orchestrator can run commands on the execution environment.

## Overview

An agent is a process that runs on an execution environment.  That process will contact the orchestrator at regular intervals, looking for some orders to execute.

If there is a pending order, the agent will do as asked and send the result back to the orchestrator.

There can be any number of agents talking with a given orchestrator.  But, typically, in a given execution environment, there is at most one agent running.

Not all execution environments have agents running on them.  If the execution environment can be accessed via SSH, this may be used instead.

## Installation

The OpenTestFactory agent is a Python application that is installed in the execution environment.  It requires Python 3.7 or higher.  It works on Linux, macOS, and Windows.

As it is a simple script it may also work on other operating systems.  It only has one external dependency, the well-known `requests` library (it will be installed if not already present in the execution environment).

To install it, use the following command:

```shell
pip install --upgrade opentf-agent
```

You can test your installation by running the following command:

```shell
opentf-agent --help
```

!!! tip
    The above command will install the most recent version of the agent.  If you want to
    install a specific version you can use the following command:

        pip install opentf-agent==1.6.0

    The list of available versions is on [PyPI](https://pypi.org/project/opentf-agent/#history).

## Usage

### Summary

```console
usage: opentf-agent [-h] [--version] --tags TAGS --host HOST [--port PORT]
                    [--path_prefix PATH_PREFIX] [--token TOKEN]
                    [--encoding ENCODING] [--script_path SCRIPT_PATH]
                    [--workspace_dir WORKSPACE_DIR] [--name NAME]
                    [--namespaces NAMESPACES] [--polling_delay POLLING_DELAY]
                    [--liveness_probe LIVENESS_PROBE] [--retry RETRY]
                    [--verify VERIFY] [--debug]

OpenTestFactory Agent

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  --tags TAGS           a comma-separated list of tags (e.g., windows,robotframework)
  --host HOST           target host with protocol (e.g., https://example.local)
  --port PORT           target port (defaults to 24368)
  --path_prefix PATH_PREFIX
                        target context path (defaults to no context path)
  --token TOKEN         token
  --encoding ENCODING   encoding on the console side (defaults to utf-8)
  --script_path SCRIPT_PATH
                        where to put generated script (defaults to current
                        directory)
  --workspace_dir WORKSPACE_DIR
                        where to put workspaces (defaults to current directory)
  --name NAME           agent name (defaults to "test agent")
  --namespaces NAMESPACES, --namespace NAMESPACES
                        namespace(s) this agent is accessible from (defaults to
                        "default")
  --polling_delay POLLING_DELAY
                        polling delay in seconds (defaults to 5)
  --liveness_probe LIVENESS_PROB
                        liveness probe in seconds (defaults to 300 seconds)
  --retry RETRY         how many times to try joining host (defaults to 5,
                        0 = try forever)
  --verify VERIFY       whether to verify the SSL connection (defaults to true,
                        true = enabled, false = disabled, file.pem = server
                        certificate plus all intermediate certificates)
  --debug               whether to log debug information.
```

### Required parameters

There are two required parameters, `--tags` and `--host`.

`--tags` is a comma-separated list of tags.  One of those tags MUST be one and only one of `linux`, `macos`, or `windows`.  There may be any number of other tags.

`--host` is the orchestrator this agent will register to.  It is a fully qualified domain name (or an IP address) and must start with a protocol, typically `https://`.

### Optional parameters

If the execution environment should be accessible from more than the `default` namespace, you can use the `--namespaces` option to list
the namespaces it will be accessible from.  `-namespaces` is a comma-separated list of namespaces.  It can be `"*"` if the execution
environment should be accessible from all namespaces.

If the port the orchestrator listens to for agent registration differs from the standard one (24368), you can use the `--port` option to adjust it.

If the orchestrator does not have its own domain name but used a shared fully qualified name, you can specify its path prefix using the `--path_prefix` option.

If the orchestrator is using a self-signed certificate, you can put the server certificate and all the intermediate certificates in a file and use `--verify path_to_file`
optional argument.

For test purposes, you can use `--verify false` to disable certificate verification.  **Do not disable certificate verification in a production environment.**

### Proxy and Trusted certificates

To override the default certificate bundle used by `opentf-agent`, you can set the standard `CURL_CA_BUNDLE` environment variable.

=== "Bash"

    ```bash
    export CURL_CA_BUNDLE="/usr/local/myproxy_info/cacert.pem"
    export https_proxy="http://10.10.1.10:1080"
    opentf-agent --host https://example.com [...]
    ```

=== "CMD"

    ```bat
    set CURL_CA_BUNDLE=/path/to/myproxy_info/cacert.pem
    set https_proxy=http://10.10.1.10:1080
    opentf-agent --host https://example.com [...]
    ```

=== "PowerShell"

    ```powershell
    $Env:CURL_CA_BUNDLE = "/path/to/myproxy_info/cacert.pem"
    $Env:https_proxy = "http://10.10.1.10:1080"
    opentf-agent --host https://example.com [...]
    ```

If the following environment variables are defined in the agent's environment, they will be used:

| Environment variable | Format | Description |
| --- | --- | --- |
| `http_proxy` or `HTTP_PROXY` | `[protocol://]<host>[:port]` | Sets the proxy server to use for HTTP. |
| `https_proxy` or `HTTPS_PROXY` | `[protocol://]<host>[:port]` | Sets the proxy server to use for HTTPS. |
| `no_proxy` or `NO_PROXY` | `<comma-separated list of hosts/domains>` | List of host names that shouldn't go through any proxy.  If set to an asterisk '*' only, it matches all hosts.  Each name in this list is matched as either a domain name which contains the hostname, or the hostname itself. |
| `CURL_CA_BUNDLE` | `<path to file>` | The CA bundle in use. |

Using the `--debug` optional parameter will show the default certificate bundle used as well as the proxy configuration.

### Stopping the agent

You can stop a running agent by killing it or interrupting it (`^C`).  It will try to de-register from the orchestrator.

If the de-registration is successful, there will be an `Agent successfully de-registered` message in the console.

If the de-registration fails, the orchestrator may still consider the agent to be available for a limited amount of time (as specified by `--liveness_probe`), which
may cause jobs targeting a compatible execution environment to time out.

## Examples

### Basic example

Assuming there is an OpenTestFactory orchestrator running on `orchestrator.example.com`, with a known token stored in the `TOKEN` environment variable, the following command will register the Windows-based execution environment and will possibly receive commands targeting windows and/or robotframework from the `default` namespace:

=== "CMD"

    ```bat
    chcp 65001
    opentf-agent ^
      --tags windows,robotframework ^
      --host https://orchestrator.example.com/ ^
      --token %TOKEN%
    ```

=== "PowerShell"

    ```powershell
    chcp 65001
    opentf-agent `
      --tags windows,robotframework `
      --host https://orchestrator.example.com/ `
      --token $Env:TOKEN
    ```

The agent will poll the orchestrator every 5 seconds and will execute the received commands.

The `chcp` command sets the console to Unicode.  It is Windows-specific.  It is not mandatory but may be needed depending on the test framework available in the execution environment.

Temporary files will be generated in the current directory, as well as workspace directories.

### Namespaced example

Assuming there is an OpenTestFactory orchestrator running on `orchestrator.example.com`, with a known token stored in the `TOKEN` environment variable, the following command will register the Windows-based execution environment and will possibly receive commands targeting windows and/or robotframework from the `namespace-a` and `namespace-b` namespaces:

=== "CMD"

    ```bat
    chcp 65001
    opentf-agent ^
      --tags windows,robotframework ^
      --host https://orchestrator.example.com/ ^
      --token %TOKEN% ^
      --namespaces namespace-a,namespace-b
    ```

=== "PowerShell"

    ```powershell
    chcp 65001
    opentf-agent `
      --tags windows,robotframework `
      --host https://orchestrator.example.com/ `
      --token $Env:TOKEN `
      --namespaces namespace-a,namespace-b
    ```

The agent will poll the orchestrator every 5 seconds and will execute the received commands.

The `chcp` command sets the console to Unicode.  It is Windows-specific.  It is not mandatory but may be needed depending on the test framework available in the execution environment.

Temporary files will be generated in the current directory, as well as workspace directories.

### Advanced example

Assuming again the existence of an OpenTestFactory orchestrator running on `orchestrator.example.com`, listening for agent registrations on port 12345, with a known token stored in the `TOKEN` environment variable.

The following command will register the Linux-based execution environment and will poll the orchestrator every second.  It will accept jobs targeting `linux`, and/or `junit`, and/or `cypress`.

Scripts will be temporarily stored on `/tmp`, and workspaces will be allocated on `/var/opentf/workspace`.

=== "Bash"

    ```bash
    opentf-agent \
      --name "all in one execution environment" \
      --tags linux,junit,cypress \
      --host https://orchestrator.example.com \
      --port 12345 \
      --token $TOKEN \
      --script_path /tmp \
      --workspace_dir /var/opentf/workspace \
      --polling_delay 1
    ```

## Troubleshooting

When the agent is started with the `--debug` optional parameter, it will display much information that can help troubleshoot problems.

If you have `opentf-ctl` available on your system, you can use it to check agent registrations and status:

```shell
opentf-ctl get agents
```

```console
NAME,AGENT_ID,TAGS,REGISTRATION_TIMESTAMP,LAST_SEEN_TIMESTAMP,RUNNING_JOB
test agent,c07d0ac5-5943-4769-8e8d-2ee809e75ee8,windows:robotframework,2021-12-03T18:13:13.71,2021-12-03T18:13:23.80,6a22d890-6210-479a-aa91-b4497113512e
```

```shell
opentf-ctl delete agent c07d0ac5-5943-4769-8e8d-2ee809e75ee8
```

For more information, see "[Tools - running commands](../tools/opentf-ctl/agent.md)."

If you do not have `opentf-ctl` available, you can use `curl` to perform the same tasks, if in a less refined format:

You can list the registered agents on a given orchestrator by running the following command:

=== "Bash"

    ```bash
    curl \
      -H "Authorization: bearer $TOKEN" \
      https://orchestrator.example.com:24368/agents
    ```

=== "CMD"

    ```bat
    curl ^
      -H "Authorization: bearer %TOKEN%" ^
      https://orchestrator.example.com:24368/agents
    ```

=== "PowerShell"

    ```powershell
    curl.exe `
      -H "Authorization: bearer $Env:TOKEN" `
      https://orchestrator.example.com:24368/agents
    ```

You can de-register a rogue agent on a given orchestrator by running the following command:

=== "Bash"

    ```bash
    curl \
      -X DELETE \
      -H "Authorization: bearer $TOKEN" \
      https://orchestrator.example.com:24368/agents/<agent_id>
    ```

=== "CMD"

    ```bat
    curl ^
      -X DELETE ^
      -H "Authorization: bearer %TOKEN%" ^
      https://orchestrator.example.com:24368/agents/<agent_id>
    ```

=== "PowerShell"

    ```powershell
    curl.exe `
      -X DELETE `
      -H "Authorization: bearer $Env:TOKEN" `
      https://orchestrator.example.com:24368/agents/<agent_id>
    ```

where `<agent_id>` is the UUID of the agent as shown by the previous command.
