---
tags:
  - workflows
  - ci
---
<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Repositories

In a typical workflow, you will checkout the code at the beginning of each job.  If
you have many jobs in your workflow, duplicating the repository parameters can
become tedious.

Workflows allow you to define `repository` resources, that you can share between
your jobs.

Additionally, those resources can carry their specific credentials, so that
job generators can omit them.

## Repository resources

Repository resources are defined in the `.resources.repositories` part of a workflow.

There can be any number of repository resources defined, and they may refer
to the same repository, but they all must have different names.

```yaml
resources:
  repositories:
  - name: foo
    type: github
    repository: a/b.git
    endpoint: https://github.com
  - name: bar
    type: gitlab
    repository: c/d.git
    endpoint: https://gitlab.com
```

Once defined, those repository resources can be referenced either explicitly or
implicitly in your jobs:

```yaml
jobs:
  job1:
    steps:
    - uses: actions/checkout@v2
      with:
        repository: ${{ resources.repositories.foo }}
    - uses: actions/checkout@v2
      with:
        repository: https://gitlab.com/c/d.git
```

The explicit reference uses the _expression_ syntax.  The implicit
reference is used if the endpoint and repository match.

## Credentials

If you define a repository in the `resources` section, all checkouts of this
repository will use the credentials you specified in the `resources` section, not
the ones you may have specified in the checkout function.

### Examples

In the following example, the checkout will use the credentials specified in
`variables.user` and `variables.pwd`:

```yaml
metadata:
  name: my workflow
resources:
  repositories:
  - name: awesome
    repository: awesome/stuff.git
    type: github
    endpoint: https://${{ variables.user }}:${{ variables.pwd }}@github.com/
jobs:
  job1:
    runs-on: linux
    steps:
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/awesome/stuff.git
```

In that almost identical example, the checkout will also use the credentials
specified in `variables.user` and `variables.pwd`, not `me` and `secret`:

```yaml
metadata:
  name: my workflow
resources:
  repositories:
  - name: awesome
    repository: awesome/stuff.git
    type: gitlab
    endpoint: https://${{ variables.user }}:${{ variables.pwd }}@gitlab.com/
jobs:
  job1:
    runs-on: linux
    steps:
    - uses: actions/checkout@v2
      with:
        repository: https://me:secret@gitlab.com/awesome/stuff.git
```

## Next Steps

Here are some helpful resources for taking your next steps with the OpenTestFactory
orchestrator:

- "[actions/checkout@v2](../providers/actionprovider.md#actionscheckoutv2)" for
  more details about configuring the checkout function
- "Guides" for specific use cases and examples
- "[Expressions](../specification/expressions.md)" for more information on expressions
