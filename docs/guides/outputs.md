---
tags:
  - workflows
---
<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Outputs

In a workflow, each job run in its own separate execution environment.  But, sometime,
you would like to get some information from a job you depend on.

Basic ordering is offered by the `needs` section.  Once a job depends on another,
it can access the outputs produced by the job(s) it depends on, through _contexts_.

## Hello World

Steps can produce _outputs_ through workflow commands.  Outputs are name/value
pairs and hold strings.

Workflow commands are printed in the standard output stream of an execution environment.
For more information on defining workflow commands, see "[Workflow commands for OpenTestFactory
Orchestrator](../specification/workflow-commands.md)."

The `set-output` workflow command is as follows:

```text
::set-output name=xxx::yyyy
```

The following step will create an output named `test` of value `hello`:

```bash
steps:
- id: step1
  run: echo "::set-output name=test::hello"
```

A step can produce any number of outputs:

```bash
steps:
- id: step1
  run: |
    echo "::set-output name=foo::foo foo"
    echo "::set-output name=bar::bar baz"
```

A job can collect and expose the outputs produced by the steps it contains to its
dependent jobs.  Here, the job `job1` will expose an `output1` output that will have
a value of `hello`, and an `output2` output that will have a value of `something else`:

```yaml
job1:
  outputs:
    output1: ${{ steps.step1.outputs.test }}
    output1: something else
  steps:
  - id: step1
    run: echo "::set-output name=test::hello"
```

Other jobs that depend on this `job1` job can then access those outputs:

```yaml
metadata:
  name: Using outputs from a previous job
jobs:
  job1:
    runs-on: linux
    # Map a step output to a job output
    outputs:
      output1: ${{ steps.step1.outputs.test }}
      output2: ${{ steps.step2.outputs.test }}
    steps:
    - id: step1
      run: echo "::set-output name=test::hello"
    - id: step2
      run: echo "::set-output name=test::world"
  job2:
    runs-on: linux
    needs: job1
    steps:
    - run: echo ${{needs.job1.outputs.output1}} ${{needs.job1.outputs.output2}}
```

This should echo 'hello world' in the execution environment in which `job2` runs.
