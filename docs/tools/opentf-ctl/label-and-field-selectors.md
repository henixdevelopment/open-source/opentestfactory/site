<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Label and field selectors

Most `opentf-ctl` commands output structured data.  By default, the output shows all available
elements.

You can use the `--selector` and `--field-selector` command line options to filter the output
to only show the elements matching the specified selectors.

Logical operators supported in selector conditions are `=`, `!=`, `in` and `notin`. 
Multiple conditions may be provided, separated by commas (`,`).  The conditions are linked by
a logical _AND_ operator. For instance,
this request will return only the channels that have the `linux` tag and are not idle:

```shell
opentf-ctl get channels --field-selector="(linux) in spec.tags,status.phase!=IDLE"
```

You can use `--selector` and `--field-selector` options together.  The conditions will be linked
by a logical _AND_ operator.

Here are the possible selector formats:

```text
key                                # the key or label exists
!key                               # the key or label does not exist
key==value                         # the key exists and has the specified value
key!=value                         # the key exists but has a different value
                                   # or the key does not exist
key in (value1, value2, ...)       # the key exists and its value is in the list
key notin (value1, value2, ...)    # the key exists and has a value not in the
                                   # list or the key does not exist
(value1, value2) in key            # the key contains all values (and possibly
                                   # others)
(value1, value2) notin key         # the key does not contains all the values
                                   # (but it may contain some) or the key does
                                   # not exist
```

For label selectors, the key is the label's name.  It may contain dots.  For field selectors,
the key is a series of field names separated by dots.  The last field name may be surrounded
by `[` and `]` to allow for dots in the field name.

Here are examples of `fieldSelector` keys:

```text
apiVersion
metadata.name
spec.selector.matchLabels[example.org/label]

[apiVersion]                                    # Invalid
spec[selector][matchLabels][example.org/label]  # Invalid
```

!!! warning
    You cannot use the `[` ... `]` notation for `labelSelector`.
