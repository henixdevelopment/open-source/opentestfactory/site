<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Basic commands

The `opentf-ctl` tool provides a set of commands to manage _workflows_.  You can list active
and recent workflows, start a workflow, get the status of a workflow, and kill a running
workflow.

## `get workflows`

This command lists active and recent workflows.

```shell
opentf-ctl get workflows
```

```text
WORKFLOWID                            STATUS  NAME
6c223f7b-3f79-4c51-b200-68eaa33c1325  DONE    RobotFramework Example
31b5e665-819c-4e92-862a-f05d1993c096  DONE    RobotFramework Example
```

It returns _workflow IDs_, which are unique identifiers that can be used by the
`get workflow {workflow_id}` command.

### Optional parameters

The `get workflows` command allows for additional optional parameters:

```text
--output=wide or -o wide                        # show additional information
--output=custom-columns= or -o custom-columns=  # show specified information
--output=json or -o json                        # show information in JSON format
--output=yaml or -o yaml                        # show information in YAML format
--selector={s} or -l {s}                        # filter the output on label selector(s)
--field-selector={s}                            # filter the output on field selector(s)
--having={s}                                    # filter the output on associated events field selector(s)
```

You can only specify one output format at a time.  Please refer to "[Output formats](output-formats.md)" for more information.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

#### Wide view

If this optional parameter is specified, the execution status, the first time
the workflow was seen, and the workflow name are displayed.

```shell
opentf-ctl get workflows -o wide
opentf-ctl get workflows --output=wide
```

```text
WORKFLOW_ID                           STATUS  FIRST_SEEN_TIMESTAMP        WORKFLOW_NAME
68e81c2b-1520-42bd-a5f3-b30458f6948a  DONE    2021-12-14T14:51:18.280384  RobotFramework Example
7e0dbbec-fa69-45ec-8707-754d994a0cc3  DONE    2021-12-14T14:51:19.635299  RobotFramework Example
3766701b-c250-4eeb-92be-e4718a9c24ad  FAILED  2021-12-14T14:51:21.419208  RobotFramework Example
1ee5f091-f234-40cd-b853-9d46fe46ceb6  DONE    2021-12-14T14:51:23.614088  RobotFramework Example
```

#### Custom view

If this optional parameter is specified, you can choose what is displayed for each active
and recent workflow.

The columns definition format is as follows: `name:value[,name:value]*`.  `name` is the column
name in the output (`WORKFLOW_ID` or `WORKFLOW_NAME` in the example above).  `value` is
the path to the information in the workflow schema.

The following values are supported:

- `.metadata.workflow_id`
- `.metadata.name`
- `.metadata.creationTimestamp`
- `.status.phase`

```shell
opentf-ctl get workflows -o custom-columns=ID:.metadata.workflow_id,NAME:.metadata.name
opentf-ctl get workflows --output=custom-columns=ID:.metadata.workflow_id,NAME:.metadata.name
```

```text
ID                                    NAME
68e81c2b-1520-42bd-a5f3-b30458f6948a  RobotFramework Example
7e0dbbec-fa69-45ec-8707-754d994a0cc3  RobotFramework Example
3766701b-c250-4eeb-92be-e4718a9c24ad  RobotFramework Example
1ee5f091-f234-40cd-b853-9d46fe46ceb6  RobotFramework Example
```

#### Using selectors

By default the `get workflows` command lists all active and recent workflows.

The command output may be restricted by using the `--selector` and `--field-selector`
options.  For example, you may restrict command output to show only `FAILED` workflows:

```shell
opentf-ctl get workflows --field-selector=status.phase==FAILED
```

```text
WORKFLOW_ID                           STATUS  WORKFLOW_NAME
3766701b-c250-4eeb-92be-e4718a9c24ad  FAILED  RobotFramework Example
```

You can combine selectors with output format.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

#### Searching for workflows

The `get workflows` command allows you to search for workflows that have specific events
associated with them.  You can use the `--having` option to filter the output based on
associated events field selectors.

For example, you may restrict command output to show only workflows that have associated
events with the `kind` field set to `ExecutionResult`:

```shell
opentf-ctl get workflows --having=kind=ExecutionResult
```

```text
WORKFLOW_ID                           STATUS  WORKFLOW_NAME
68e81c2b-1520-42bd-a5f3-b30458f6948a  DONE    RobotFramework Example
7e0dbbec-fa69-45ec-8707-754d994a0cc3  DONE    RobotFramework Example
1ee5f091-f234-40cd-b853-9d46fe46ceb6  DONE    RobotFramework Example
```

You can combine this option with the `--selector` and `--field-selector` options to further refine
the output.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

??? success "Example: finding the workflow having a given job"
    If you have a job ID and want to find the corresponding workflow, you can check for
    the `metadata.job_id` field that is present in `ExecutionCommand` events (among other):

    ```shell
    opentf-ctl get workflows --having=metadata.job_id=9ea3be45-ee90-4135-b47f-e66e4f793383
    ```

    ```text
    WORKFLOW_ID                           STATUS  WORKFLOW_NAME
    68e81c2b-1520-42bd-a5f3-b30458f6948a  DONE    RobotFramework Example
    ```


??? success "Example: finding the workflows with a job runs on (windows, cucumber)"
    If you want to find the workflows that have run on an environment with a specific tag
    or a set of specific tag, you can use the following:

    ```shell
    opentf-ctl get workflows --having='(windows, cucumber) in runs-on'
    ```

    ```text
    WORKFLOW_ID                           STATUS  WORKFLOW_NAME
    68e81c2b-1520-42bd-a5f3-b30458f6948a  DONE    RobotFramework Example
    1ee5f091-f234-40cd-b853-9d46fe46ceb6  DONE    RobotFramework Example
    ```


## `run workflow {file_name}`

This command starts a workflow.  It requires one parameter, an existing file name.

```shell
opentf-ctl run workflow demo_windows_robotframework.yaml
```

```text
Workflow 7aa1e1a4-4ede-4ab9-aa80-f9213d057397 is running.
```

It returns a _workflow ID_, which is a unique identifier for the started workflow.

### Optional parameters

The `run workflow` command allows for additional optional parameters, in no specific order,
but they must follow the command if used (but they may precede the workflow file name):

```text
-e var=value                            # define variable
-e path                                 # define variables
-f name=path                            # send file with workflow
--namespace={ns} or -n {ns}             # overrides default namespace target
--tags={tag{,tag}...}                   # add extra tags to the workflow's runs-on entries
--dry-run                               # simulate workflow run without starting it
--wait or --watch or -w                 # wait until completion or cancellation
--mode={qg.name} or -m {qg.name}        # apply a quality gate to the successfully completed workflow
--report={report.ext}[:path/custom.ext] # download an execution report after workflow completion
```

The following additional options can only be used in conjunction with `--wait`:

```text
--step-depth={n} or -s {n}          # show nested steps to a given depth (1 by default)
--job-depth={n} or -j {n}           # show nested jobs to a given depth (1 by default)
--max-command-length={n} or -c {n}  # show the first n characters of running commands (15 by default)
--output-prefix={prefix}            # prefix to add to the output for regular logs
--show-notifications or -a          # show notifications
--verbose or -v                     # show all notifications (can be noisy)
--show-attachments                  # show produced attachments in the workflow output
--output={format} or -o {format}    # show information in specified format (json or yaml)
--selector={s} or -l {s}            # filter the output on label selector(s)
--field-selector={s}                # filter the output on field selector(s)
```

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

!!! tip
    You can change the default values of `job-depth`, `step-depth`, and `max-command-length`
    for a given user using `opentf-ctl config set-credentials {user} --job-depth={n}`.

    What is passed on the command line overrides what is defined for a given user.

#### Sending variables

You can provide variables for your workflow execution by using the `-e variable=value`
command-line option, repeatedly if needed.

If you have a set of variables you want to provide for your workflow execution, you can
use the `-e path` command-line option, repeatedly if needed.  `path` should be a file
containing a list of `variable=value` lines.


=== "Bash"

    ```bash
    opentf-ctl \
      run workflow demo_windows_robotframework.yaml \
      -e .env \
      -e TIMESTAMP=$TIMESTAMP
    ```

=== "CMD"

    ```bat
    opentf-ctl ^
      run workflow demo_windows_robotframework.yaml ^
      -e .env ^
      -e TIMESTAMP=%TIMESTAMP%
    ```

=== "PowerShell"

    ```powershell
    opentf-ctl `
      run workflow demo_windows_robotframework.yaml `
      -e .env `
      -e TIMESTAMP=$Env:TIMESTAMP
    ```

The options are processed in order, and the last value of a given variable is what will be
available during the workflow execution.

Environment variables with an `OPENTF_RUN_` prefix will be defined without the prefix in the
workflow and while running commands in an environment.  Those variables can be overridden by
using `-e` parameters.

=== "Bash"

    ```bash
    export OPENTF_RUN_FOO=foo
    export OPENTF_RUN_BAR=bar
    opentf-ctl run workflow demo.yaml -e FOO=foobar
    ```

=== "CMD"

    ```bat
    set OPENTF_RUN_FOO=foo
    set OPENTF_RUN_BAR=bar
    opentf-ctl run workflow demo.yaml -e FOO=foobar
    ```

=== "PowerShell"

    ```powershell
    $Env:OPENTF_RUN_FOO = "foo"
    $Env:OPENTF_RUN_BAR = "bar"
    opentf-ctl run workflow demo.yaml -e FOO=foobar
    ```

In the workflow, `FOO` will be `foobar` and `BAR` will be `bar`.

#### Sending files

If your workflow requires it, you can join files by using the `-f name=file` command-line
option, repeatedly if needed.

=== "Bash"

    ```bash
    opentf-ctl \
      run workflow demo_windows_robotframework.yaml \
      -f report=data/report.html \
      -f key=secret/key.pem
    ```

=== "CMD"

    ```bat
    opentf-ctl ^
      run workflow demo_windows_robotframework.yaml ^
      -f report=data\report.html ^
      -f key=secret\key.pem
    ```

=== "PowerShell"

    ```powershell
    opentf-ctl `
      run workflow demo_windows_robotframework.yaml `
      -f report=data\report.html `
      -f key=secret\key.pem
    ```

```text
Workflow 03680dca-6b6a-4eac-a082-ffd164e8f8e0 is running.
```

#### Overriding default namespace target

By default, the `run workflow` command starts the workflow in its specified namespace (as
defined by its `.metadata.namespace` entry), or in the `default` namespace if no namespace
is specified in the workflow.

If you use the `--namespace=target` command-line option, the workflow will start in the `target`
namespace instead.

#### Adding tags

You can add tags to the workflow by using the `--tags` command-line option.  The tags will be
added to all jobs with a `runs-on` entry in the workflow.

You can include at most one OS tag (`linux`, `macos`, or `windows`) in the mix, and it will
replace the ones found on `runs-on` entries, if applicable.

??? success "Example: replacing and adding tags"
    Given the following workflow:

    ```yaml title="demo_windows.yaml"
    metadata:
      name: demo workflow
    jobs:
      job:
        runs-on: windows
        steps:
        - run: echo '${{ runner.os }}'
    ```

    The following command will replace the `windows` tag with `linux` and will add a `foo` tag:

    ```shell
    opentf-ctl run workflow demo_windows.yaml --tags foo,linux
    ```

    That is, it will be as if the following workflow was started:

    ```yaml
    metadata:
      name: demo workflow
    jobs:
      job:
        runs-on: [linux, foo]
        steps:
        - run: echo '${{ runner.os }}'
    ```

    Assuming there exist an execution environment with the `linux` and `foo` tags, it would
    gives:

    ```text
    Workflow c8c13a81-a3d7-4329-9363-e7e666b453f2 is running.
    Workflow demo workflow
    (running in namespace 'default')
    [2024-07-17T17:15:58] [job 6b2c4ded-1230-4f58-b7f9-d26a61a8d872] Requesting execution environment providing ['linux', 'foo'] in namespace 'default' for job 'job'
    [2024-07-17T17:15:58] [job 6b2c4ded-1230-4f58-b7f9-d26a61a8d872] Running command: echo 'linux'
    [2024-07-17T17:16:00] [job 6b2c4ded-1230-4f58-b7f9-d26a61a8d872] linux
    [2024-07-17T17:16:00] [job 6b2c4ded-1230-4f58-b7f9-d26a61a8d872] Releasing execution environment for job 'job'
    Workflow completed successfully.
    ```

#### Simulating a workflow run without starting it

If you want to simulate a workflow run without actually starting it, you can use the
`--dry-run` option.

```shell
opentf-ctl run workflow demo_windows_robotframework.yaml --dry-run
```

```text
Workflow 7aa1e1a4-4ede-4ab9-aa80-f9213d057397 is running.
```

If the workflow needs files or variables, you must provide them.  It allows you to test your
workflow syntax, or to ensure your `run workflow` command is complete.  If anything goes
wrong, you will get the same messages you would have got if the workflow had been started.

No further processing occurs, though, and the workflow will not show on further `get workflows`
commands, and using the `get workflow` or `--wait` or `--watch` options will fail with a message
indicating the workflow does not exist.

#### Waiting for completion

By default, the `run workflow` command starts a workflow and immediately returns.

If you use the `--wait` command-line option, it will wait until the workflow's completion
or cancellation.

Stopping or interrupting the command does not cancel the running workflow.

```shell
opentf-ctl run workflow demo_windows_robotframework.yaml -w
opentf-ctl run workflow demo_windows_robotframework.yaml --wait
```

```text
Workflow 7aa1e1a4-4ede-4ab9-aa80-f9213d057397 is running.
...
Workflow completed successfully.
```

When used with `--wait` option, `run workflow` command output may be adjusted using depth
and length specifiers [in the same way](#nested-jobs-and-steps) as for the `get workflow`
command.

#### Applying a quality gate to a completed workflow

It is possible to apply a quality gate to a successfully completed workflow directly 
from the `run workflow` command. Just use the `--mode` or `-m` option followed by 
the name of the quality gate to apply.

`run workflow` command also supports all the remaining `get qualitygate` command 
options: see the [Quality Gate Commands](qualitygate.md#optional-parameters) page.

??? success "Example: running a workflow and applying a quality gate when it is completed"
    ```shell
    opentf-ctl run workflow workflow_file.yaml --mode=my.quality.gate
    ```

    ```text
    Workflow 5ccd93f8-66ca-4ae1-a784-c0067500c675 is running.
    Waiting for workflow completion to apply qualitygate...
    RULE                         RESULT   TESTS_IN_SCOPE  TESTS_FAILED  TESTS_PASSED  SUCCESS_RATIO
    Cucumber.all                 FAILURE  5               2             3             60.0%

    ```
#### Downloading execution reports

It is possible to download workflow execution reports directly after workflow completion 
using the `--report` option. This option has the following syntax:

```
--report=report.ext[:report/path/custom.ext]
```

`report.ext` is the report name as specified in the Insight Collector configuration file with 
file extension. Currently, there are three possible file extensions: `.txt` for `ExecutionLog` kind 
insights, `.xml` for `SurefireXmlReport`, and `.html` for `SummaryReport` kind insights.
If no Insight Collector configuration file is provided, default reports are generated: you can 
download them using `executionlog.txt`, `executionreport.xml`, and `executionreport.html` report 
names.

You can also specify a custom report path with file name. If the path does not exist, it will be created.

The `--report` option can be specified multiple times.

??? success "Example: downloading all default reports after workflow completion"
    ```shell
    opentf-ctl run workflow my_workflow.yaml \
               --report=executionlog.txt \
               --report=executionreport.xml \
               --report=executionreport.html
    ```

    ```text
    Workflow f523c572-a1f9-4e5d-98ad-f614b3e8925a is running.
    Waiting for workflow completion to download reports...
    Waiting for workflow workers teardown...
    Attachment executionlog.txt (0308cd3b-05ed-4bc8-ab63-bdfb69b91658) is downloaded at ./executionlog.txt.
    Attachment executionreport.xml (58c2584b-ff54-4c0c-80cf-c80e02723a45) is downloaded at ./executionreport.xml.
    Attachment executionreport.html (3690ddf2-92cd-47fd-ac71-d316a51b26b9) is downloaded at ./executionreport.html.
    ```

??? success "Example: downloading SummaryReport at the custom path"

    Assuming the Insight Collector is used with a custom definition file, the following 
    command will download the `SummaryReport` insight `detailed-report` as `./test/report001.html` 
    when the workflow `my_workflow.yaml` is completed:

    ```shell
    opentf-ctl run worfklow my_workflow.yaml --report detailed-report.html:./test/report001.html
    ```

    ```text
    Workflow 98d848a8-58ab-4e97-8591-2cec3bc75376 is running.
    Waiting for workflow completion to download reports...
    Waiting for workflow workers teardown...
    Attachment detailed-report.html (9bc2a7c0-fc62-4243-9eea-74663a4fde4c) is downloaded at ./test/report001.html.
    ```

??? failure "Example: trying to download a report with wrong extension"
    ```shell
    opentf-ctl run workflow my_workflow.yaml --report custom-report.json
    ```

    ```text
    Workflow 553970af-806f-4f33-8b71-f8486b1e9ca2 is running.
    Waiting for workflow completion to download reports...
    WARNING:root:Unexpected report name custom-report.json, was expecting one of extensions .html, .txt, .xml.
    ERROR:root:No report to download, aborting.
    ```


#### Using selectors

When used with `--wait` option, `run workflow` command output may also be restricted using
field and label selectors [in the same way](#using-selectors_2) as for `get workflow` command.

For example, you may restrict command output to show only _ExecutionResult_ events:

??? success "Example: displaying only ExecutionResult events of a running workflow"
    ```shell
    opentf-ctl run workflow workflow_file.yaml -w --field-selector=kind=ExecutionResult
    ```

    ```text
    Workflow 672efdb1-cd34-4190-94ed-2c987d522309 is running.
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO] Scanning for projects...
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO]
    [...]
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] -------------------------------------------------------
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b]  T E S T S
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] -------------------------------------------------------
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] Running projet_selenium.SampleTest_01_linux
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.17 sec
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b]
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] Results :
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b]
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b]
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO] ------------------------------------------------------------------------
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO] BUILD SUCCESS
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO] ------------------------------------------------------------------------
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO] Total time:  2.419 s
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO] Finished at: 2023-03-30T12:26:47+02:00
    [2023-03-30T12:26:47] [job 71f2a3cf-aa00-4847-8541-f564a8f0b67b] [INFO] ------------------------------------------------------------------------
    Workflow completed successfully.
    ```

## `get workflow {workflow_id}`

This command shows the running status of the specified workflow.

```shell
opentf-ctl get workflow 396e99ec-0fe4-4f35-8b9e-69a550a0e474
opentf-ctl get workflow 396
```

```text
[2021-10-13T17:18:33] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Requesting execution environment providing ['windows', 'robotframework'] for job keyword-driven
[2021-10-13T17:18:33] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Running actionscheckoutv2
[2021-10-13T17:18:37] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Running ['dir']
[2021-10-13T17:18:42] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Running robotframeworkrobotv1
Workflow completed successfully.
```

You can specify a partial workflow ID, as long as it is unique (if it is not, a message is
displayed listing the available workflows).

The last line can take four values:

- Workflow completed successfully.
- Workflow is running.
- Workflow canceled.
- Workflow failed.

### Optional parameters

The `get workflow` command allows for additional optional parameters, in no specific order,
but they must follow the command if used (but they may precede the workflow ID):

```text
--watch or -w                       # wait until completion or cancellation, displaying status updates as they occur
```

```text
--step-depth={n} or -s {n}          # show nested steps to a given depth (1 by default)
--job-depth={n} or -j {n}           # show nested jobs to a given depth (1 by default)
--max-command-length={n} or -c {n}  # show the first n characters of running commands (15 by default)
--output-prefix={prefix}            # prefix to add to the output of regular logs
--show-notifications or -a          # show notifications
--verbose or -v                     # show all notifications (can be noisy)
--show-attachments                  # show information on produced attachments in the workflow
--output={format} or -o {format}    # show information in specified format (json or yaml)
--selector={s} or -l {s}            # filter the output on label selector(s)
--field-selector={s}                # filter the output on field selector(s)
```

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

!!! tip
    You can change the default values of `job-depth`, `step-depth`, and `max-command-length`
    for a given user using `opentf-ctl config set-credentials {user} --job-depth={n}`.

    What is passed on the command line overrides what is defined for a given user.

#### Waiting for completion

By default, the `get workflow` command shows the running status of a workflow and
immediately returns.

If you use the `--watch` (or `-w`) command-line option, it will wait until the workflow's
completion or cancellation.

Stopping or interrupting the command does not cancel the running workflow.

```shell
opentf-ctl get workflow 396 -w
opentf-ctl get workflow 396 --watch
```

```text
[2021-10-13T17:18:33] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Requesting execution environment providing ['windows', 'robotframework'] for job keyword-driven
...
Workflow completed successfully.
```

#### Nested jobs and steps

By default, the nested steps and jobs are hidden.  If you want to see the jobs produced
by generators or the steps produced by providers, you can add the following parameters:

```text
--job-depth={n} or -j {n}           # 1 by default
--step-depth={n} or -s {n}          # 1 by default
```

```shell
opentf-ctl get workflow 396 -s 2
opentf-ctl get workflow 396 --step-depth=2
```

```text
[2021-10-13T17:18:33] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Requesting execution environment providing ['windows', 'robotframework'] for job keyword-driven
[2021-10-13T17:18:33] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Running actionscheckoutv2
[2021-10-13T17:18:33] [job 9ea3be45-ee90-4135-b47f-e66e4f793383]  Running command: git clone https...
[2021-10-13T17:18:37] [job 9ea3be45-ee90-4135-b47f-e66e4f793383] Running command: dir
...
Workflow completed successfully.
```

If you want to view all jobs and all steps, use `--job-depth=0` (or `-j 0`) and `--step-depth=0` (or `-s 0`).

If you want to see the full running command, use `--max-command-length=0` (or `-c 0`).

#### Showing notifications

The `--show-notifications` (or `-a`) command-line option shows additional notifications that are not
displayed by default.  If used in conjunction with the `--verbose` (or `-v`) option, the debug and
trace notifications are also shown, if available.  This can be noisy.

```shell
opentf-ctl get workflow 421 --show-notifications
opentf-ctl get workflow 421 --a
```

#### Enabling verbose output

By default, possibly repeated messages are only shown once (for example, execution environment
requests), to prevent logs clutter.

If you want to see all messages, you can use the `--verbose` (or `-v`) command-line option:

```shell
opentf-ctl get workflow 421 --verbose
opentf-ctl get workflow 421 -v
```

!!! info
    When used in conjunction with the `--show-notifications` (`-a`) command-line option, all available
    notifications, including debug and trace, are shown.  This can be noisy.

#### Showing attachments

The `--show-attachments` command-line option is available for the `get workflow` command if you 
want to display the attachments produced by the workflow. 

When the `get workflow` command is used with the `--show-attachments` command-line option, the
command output is completed by the attachment creation steps. These steps contain the produced
attachment UUID and its name. 

You can use the attachment UUID to retrieve it with the [`opentf-ctl cp` command](attachments.md#cp-workflow_id-destination).

??? success "Example: displaying the workflow with the attachment creation steps"
    ```shell
    opentf-ctl get workflow 7eb --show-attachments
    ```

    ```text
    [1970-01-01T14:14:08] [job 831821da-ac32-47c3-8855-1ca4f219c710] Requesting execution environment providing ['linux', 'robotframework']
    [1970-01-01T14:14:08] [job 831821da-ac32-47c3-8855-1ca4f219c710] Running function robotframeworkexecutev1 
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] ==============================================================================
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] Test Backslashes
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] ==============================================================================
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] Test  single                                                          | PASS |
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] ------------------------------------------------------------------------------
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] Test \ double                                                         | PASS |
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] ------------------------------------------------------------------------------
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] Test Backslashes                                             | PASS |
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] 2 tests, 2 passed, 0 failed
    [1970-01-01T14:14:11] [job 831821da-ac32-47c3-8855-1ca4f219c710] ==============================================================================
    [...]
    [1970-01-01T14:14:13] [job 831821da-ac32-47c3-8855-1ca4f219c710]  Produced attachment e3801f19-eb7a-4447-949f-a27499868ac8 (RobotFramework_reports.tar).
    [1970-01-01T14:14:13] [job 831821da-ac32-47c3-8855-1ca4f219c710]  Produced attachment 841b9301-7d85-4ed8-8b0f-a33b21b4c446 (output.xml).
    [1970-01-01T14:14:14] [job 831821da-ac32-47c3-8855-1ca4f219c710] Releasing execution environment for job
    [1970-01-01T14:14:14] Produced attachment 64a5cf38-f6ba-4f5e-8849-439ee0a23753_e (executionlog.txt).
    Workflow completed successfully.
    ```

#### Using selectors

You may use field or label [selectors](label-and-field-selectors.md) to restrict 
the `get workflow` command output (the events list).

Label selectors could be specified with `--selector` or `-l` option and field selectors
with `--field-selector` option. 

The most relevant selector for `get workflow` command is `kind` field selector, which makes
it possible to select the format of events shown in the output. Possible event formats are
listed on [Events](../../specification/events.md#event-formats) page.

If the selector condition contains spaces, you must surround it with double quotes 
to prevent bash parsing errors.

??? success "Example: displaying only ProviderCommand and Notification events of a workflow"
    ```shell
    opentf-ctl get workflow 7eb -a --field-selector="kind in (ProviderCommand,Notification)"
    ```

    ```text
    [1970-01-01T11:21:27] [job 75395f45-8bd9-4ce3-9528-0a4302370310] Running function junitparamsv1
    [1970-01-01T11:21:27] [job 75395f45-8bd9-4ce3-9528-0a4302370310]  Running function actionscreate-filev1
    [1970-01-01T11:21:27] [job 75395f45-8bd9-4ce3-9528-0a4302370310]  Running function actionscreate-filev12
    [1970-01-01T11:21:27] [job 75395f45-8bd9-4ce3-9528-0a4302370310] Running function junitexecutev1
    [1970-01-01T11:21:27] [job 75395f45-8bd9-4ce3-9528-0a4302370310]  Running function actionsdelete-filev1
    [1970-01-01T11:21:27] [job 75395f45-8bd9-4ce3-9528-0a4302370310]  Running function actionsdelete-filev12
    [job 75395f45-8bd9-4ce3-9528-0a4302370310] [INFO] Parsing JUnit test results...
    [job 75395f45-8bd9-4ce3-9528-0a4302370310] [INFO] Parsing JUnit test report.
    [job 75395f45-8bd9-4ce3-9528-0a4302370310] [INFO] Test report parsing result received for step id=40e1a8da-ebba-4827-a232-0a0e94ddfe7e
    ```

## `kill workflow {workflow_id}`

This command kills a running workflow.

```shell
opentf-ctl kill workflow aa6e99ec-0fe4-4f35-8b9e-69a550a0e4ed
opentf-ctl kill workflow aa6e99ec c07d0ac5
opentf-ctl kill workflow --all
```

```text
Killing workflow aa6e99ec-0fe4-4f35-8b9e-69a550a0e4ed.
```

You can specify partial workflow IDs, as long as they are unique (if they are not, a message is
displayed and the ambiguous workflows are not killed).

The optional `reason` and `source` options can be used to document the reason for killing the
workflow and the source of the kill request.

### Optional parameters

The `kill workflow` command allows for optional parameters:

```text
--reason={reason}                   # reason for killing the workflow
--source={source}                   # source of the kill request
--all                               # kill all running workflows
--selector={s} or -l {s}            # kill workflows based on label selector(s)
--field-selector={s}                # kill workflows based on field selector(s)
--dry-run                           # simulate workflow kill without killing it
```

You cannot mix `--all` with `--selector` or `--field-selector`, and you cannot use those
options when specifying workflow IDs.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

#### Simulating a workflow kill without actually killing it

If you want to simulate a workflow kill without actually killing it, you can use the
`--dry-run` option.

```shell
opentf-ctl kill workflow aa6e99ec-0fe4-4f35-8b9e-69a550a0e4ed --dry-run
```

```text
Killing workflow aa6e99ec-0fe4-4f35-8b9e-69a550a0e4ed.
```

If anything goes wrong, you will get the same messages you would have got if you had not used
the `--dry-run` option.

No further processing occurs, though, and the workflow will still be running.
