<!--
Copyright (c) 2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Data Sources Commands

The `opentf-ctl` tool provides a command to manage data sources. You can query for 
data sources of a workflow: test cases, execution environment tags, or workflow jobs. 
Data sources can be queried during workflow execution, as they are cached dynamically, 
but in this case, provided info can be incomplete.

## `get datasource {workflow_id} --kind {kind}`

This command queries for the specified workflow data sources.

```shell
opentf-ctl get datasource a13f0572-b23b-40bc-a6eb-a12429f0143c --kind=testcases
opentf-ctl get datasource a13 -k testcases
```
```text
NAME                                         TECHNOLOGY      OUTCOME   EXECUTION TIME
Successful Test 1#Sample Successful Test 1   cypress         SUCCESS  44
Successful Test 2#Sample Successful Test 2   cypress         SUCCESS  18
Failing Test 1#Sample Failing Test 1         cypress         FAILURE  1
Failing Test 2#Sample Failing Test 2         cypress         FAILURE  1
```

You can specify a partial workflow ID, as long as it is unique (if it is not, a 
message is displayed listing the available workflows).

The `--kind` (or `-k`) parameter is mandatory. It defines the data source to query. 
Currently, three kinds are available: `testcases`, `jobs`, and `tags`.

### The `testcases` data source

The `get datasource {workflow_id} --kind testcases` command default output for the `testcases` data source 
contains the test cases list, displaying, for each test case, its name, technology, 
outcome, and execution time.

It is also possible to display, in addition, execution environment (or `runs-on`) 
tags and job name using `--output=wide` option. If you want to display the complete 
data source, use `--output=json|yaml` option.

### The `jobs` data source

The `get datasource {workflow_id} --kind jobs` command default output for the `jobs` data source 
contains the job list, displaying, for each job, its name, successful, failed and 
total test cases count.

```shell
opentf-ctl get datasource a13 -k jobs
```
```text
NAME                               SUCCESS  FAILURE  TOTAL
cucumber-execute-test              3        2        5
cypress-junit-execute-test         3        6        9
postman-robot-soapui-execute-test  10       3        13
```

It is also possible to display, in addition, counts for tests in error and skipped 
tests, and execution environment (or `runs-on`) tags using `--output=wide` option. 
If you want to display the complete data source, use `--output=json|yaml` option.

### The `tags` data source

The `get datasource {workflow_id} --kind tags` command default output for the `tags` data source 
contains the execution environment tags list, displaying, for each tag, its name, 
successful, failed and total test cases count.

```shell
opentf-ctl get datasource a13 -k tags
```
```text
NAME            SUCCESS  FAILURE  TOTAL
linux           16       11       27
cucumber        3        2        5
cypress         3        6        9
junit           3        6        9
postman         10       3        13
```

It is also possible to display, in addition, counts for tests in error and skipped 
tests using `--output=wide` option. If you want to display the complete data source, 
use `--output=json|yaml` option.

### Optional parameters

The `get datasource {workflow_id}` command allows for additional optional parameters, 
in no specific order, but they must follow the command if used:

```
--output=wide or -o wide                          # show additional information
--output=custom-columns= or -o custom-columns=    # show specified information
--output=json or -o json                          # show information in JSON format
--output=yaml or -o yaml                          # show information in YAML format
--field-selector={selector}                       # filter query on field selector
--timeout={x}                                     # set query timeout in seconds
```

#### Custom view

If this optional parameter is specified, you can choose what is displayed for each 
known execution environment.

The columns definition format is as follows: `name:value[,name:value]*`.  `name` 
is the column name in the output (`OUTCOME` or `DETAILS` in the example above). 
`value` is the path to the information in the data source schema.

The following values are supported:

1) Shared paths:

- `.metadata.name`
- `.metadata.namespace`
- `.metadata.workflow_id`

2) `testcases`:

- `.metadata.id`
- `.metadata.job_id`
- `.metadata.execution_id`
- `.metadata.creationTimestamp`
- `.metadata.executions`
- `.test.runs-on`
- `.test.uses`
- `.test.technology`
- `.test.managed`
- `.test.job`
- `.test.test`
- `.test.suiteName`
- `.test.testCaseName`
- `.test.outcome`
- `.execution.startTime`
- `.execution.endTime`
- `.execution.duration`
- `.execution.outcome`
- `.execution.errorsList`
- `.execution.(failure|warning|error)Details`
- `.execution.(failure|warning|error)Details.(message|type|text)`
- `.executionHistory`

`failureDetails`, `errorDetails`, and `warningDetails` paths are specific to the 
failed test cases. `errorsList` path is available for Robot Framework executions 
only, in case there are general execution errors. 

3) `jobs`:

- `.metadata.id`
- `.metadata.creationTimestamp`
- `.spec.runs-on`
- `.spec.variables`
- `.status.phase`
- `.status.requestTime`
- `.status.startTime`
- `.status.endTime`
- `.status.duration`
- `.status.testCaseCount`
- `.status.testCaseCountStatusSummary.(success|failure|error|skipped|cancelled)`

4) `tags`:

- `.status.jobCount`
- `.status.testCaseCount`
- `.status.testCaseCountStatusSummary.(success|failure|error|skipped|cancelled)`

The following command will output three columns with test case name, outcome and details 
on failure type if relevant:

```shell
opentf-ctl get datasource a13 -k testcases -o \
   custom-columns=NAME:.metadata.name,OUTCOME:.test.outcome,DETAILS:.execution.failureDetails.type
```
```text
NAME                                               OUTCOME  DETAILS
Successful Test 1#Sample Successful Test 1         SUCCESS  <none>
Successful Test 2#Sample Successful Test 2         SUCCESS  <none>
Successful Test 3#Sample Successful Test 3         SUCCESS  <none>
Failing Test 1#Sample Failing Test 1               FAILURE  AssertionError
Failing Test 2#Sample Failing Test 2               FAILURE  AssertionError
Failing Test 3#Sample Failing Test 3               FAILURE  AssertionError
```

#### Using field selectors

By default the `get datasource {workflow_id}` command lists all currently cached 
data source items. 

The command output may be restricted by using the `--field-selector` option. For 
example, you may restrict command output to show only failed test cases.

```shell
opentf-ctl get datasource a13 --kind testcases --field-selector=test.outcome==failure
```
```text
NAME                                      TECHNOLOGY  OUTCOME  EXECUTION TIME
Failing Test 1#Sample Failing Test 1      cypress     FAILURE  1
Failing Test 2#Sample Failing Test 2      cypress     FAILURE  1
Failing Test 3#Sample Failing Test 3      cypress     FAILURE  1
```

You can combine selectors with output format.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

#### Timeout

!!! info
    `--timeout` option is relevant for orchestrator versions from `0.65.0` to `0.68.0` 
    that cached data sources after workflow completion. Newer versions use live data 
    source caching.

All data sources are cached. For large workflows containing thousands of test cases, 
cache creation may take some time, so your request may time out. Default request 
timeout is 8 seconds: if you need to raise it, you can use the `--timeout=` option 
to set your own timeout in seconds.

```shell
opentf-ctl get datasource a13 --kind testcases --timeout 300
```

When the `--field-selector` option is used, a new data source cache is created 
for each field selector. It may influence request times.
