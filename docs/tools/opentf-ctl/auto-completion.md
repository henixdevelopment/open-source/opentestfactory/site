<!--
Copyright (c) 2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Auto-completion for Bash

The `opentf-ctl` tool supports commands auto-completion for Bash. 
The auto-completion script needs to be installed separately.

This script depends on [`bash-autocompletion`](https://github.com/scop/bash-completion#installation){ target="_blank" } 
package.

To enable `opentf-ctl` commands auto-completion, run the following commands:

```bash
opentf-ctl completion bash | sudo tee /etc/bash_completion.d/opentf-ctl > /dev/null
sudo chmod a+r /etc/bash_completion.d/opentf-ctl
```

Then reload your shell or source the `/etc/bash_completion.d/opentf-ctl` file in 
the current session:

```bash
source /etc/bash_completion.d/opentf-ctl
```

The auto-completion supports full and partial auto-completion for commands, commands options and 
dynamical parameters like workflow ids:

```bash
opentf-ctl get workflow [tab]
```

```console
9de3d8aa-3feb-4580-bf4b-1ba9daf41f99  cf877db8-7124-49fe-8721-bf057bc28b1c  
e72d271f-168e-4dcd-8865-3dc7f56d3faa  d0954316-9b1d-4b5f-9eff-b4d58e173951
```
