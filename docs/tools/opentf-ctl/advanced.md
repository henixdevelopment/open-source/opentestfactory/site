<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Advanced commands

The `opentf-ctl` tool also provides a set of commands to manage _namespaces_ and _subscriptions_.
You can list accessible namespaces, list subscriptions, and cancel an active subscription.

## `get namespaces`

This command lists namespaces the token can access.  It is most useful when [namespaces](../../guides/namespaces.md)
are enabled.

```shell
opentf-ctl get namespaces
```

```console
NAMESPACE
*
```

A `*` means the token has access to all namespaces.  Otherwise, the token can access all listed
namespaces.  A token can have access to no namespaces.

A common use case is to check the access rights of a token.  You do not have to adjust your
opentfconfig configuration file, you can use the `--token` optional global parameter:

```shell
opentf-ctl get namespaces -l verb==create,resource==workflows --token=$TOKEN
opentf-ctl get namespaces --selector=verb==create,resource==workflows --token=$TOKEN
```

```console
NAMESPACE
project-a
```

### Optional parameters

The `get namespaces` command allows for one additional optional parameter:

```text
--selector={s} or -l {s}  # selector (query) to filter on, supports 'resource=='
                          # and 'verb==', both required when specifying a selector.
```

The possible values for `verb` are `list`, `get`, `create`, and `delete`.  The possible values
for `resource` are `agents`, `channelhandlers`, `channels`, `qualitygates`, `status`, `subscriptions`,
and `workflows`.

If the `--selector` optional parameter is specified, it must specify a `resource` value as well
as a `verb` value.

It only shows the namespaces the specified operation is allowed on.

#### Using selectors

This first command lists the namespaces you can run workflows on (that is, where you can use `run workflow`):

```shell
opentf-ctl get namespaces -l verb==create,resource==workflows
opentf-ctl get namespaces --selector=verb==create,resource==workflows
```

```console
NAMESPACE
sandbox
project-a
```

This second command lists the namespaces you can get workflows on (that is, where you can use
`get workflows` and `get workflow`):

```shell
opentf-ctl get namespaces -l verb==get,resource==workflows
opentf-ctl get namespaces --selector=verb==get,resource==workflows
```

```console
NAMESPACE
sandbox
project-a
project-b
project-c
```

## `get subscriptions`

This command lists active subscriptions on the event bus.

```shell
opentf-ctl get subscriptions
```

```console
NAME            ENDPOINT                     CREATION                COUNT  SUBSCRIPTION
actionprovider  http://127.0.0.1:9065/inbox  2021-12-14T14:50:57.12  20     "kind==ProviderCommand:opentestfactory.org/category==delete-file,opentestfactory.org/categoryPrefix==actions"
junit           http://127.0.0.1:9865/inbox  2021-12-14T14:50:57.79  0      "kind==ProviderCommand:opentestfactory.org/category==mvntest,opentestfactory.org/categoryPrefix==junit,opentestfactory.org/categoryVersion==v1"
actionprovider  http://127.0.0.1:9065/inbox  2021-12-14T14:50:57.44  0      "kind==ProviderCommand:opentestfactory.org/category==put-file,opentestfactory.org/categoryPrefix==actions"
localcleaner    http://127.0.0.1:9566/inbox  2021-12-14T14:50:55.86  0      kind==WorkflowCanceled:
observer        http://127.0.0.1:9565/inbox  2021-12-14T14:50:55.21  4      "kind==Workflow,apiVersion==opentestfactory.org/v1alpha1:"
observer        http://127.0.0.1:9565/inbox  2021-12-14T14:50:56.46  0      "kind==GeneratorCommand,apiVersion==opentestfactory.org/v1alpha1:"
robotframework  http://127.0.0.1:9265/inbox  2021-12-14T14:50:57.46  0      "kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1:opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==robotframework,opentestfactory.org/categoryVersion==v1"
actionprovider  http://127.0.0.1:9065/inbox  2021-12-14T14:50:57.60  0      "kind==ProviderCommand:opentestfactory.org/category==touch-file,opentestfactory.org/categoryPrefix==actions"
robotframework  http://127.0.0.1:9265/inbox  2021-12-14T14:50:57.05  4      "kind==ProviderCommand,apiVersion==opentestfactory.org/v1alpha1:opentestfactory.org/category==robot,opentestfactory.org/categoryPrefix==robotframework,opentestfactory.org/categoryVersion==v1"
junit           http://127.0.0.1:9865/inbox  2021-12-14T14:50:57.89  0      "kind==ProviderCommand:opentestfactory.org/category==params,opentestfactory.org/categoryPrefix==junit,opentestfactory.org/categoryVersion==v1"
```

The first item is the service name, the second item is the service endpoint, the third item
is the subscription date, the fourth item is the number of events sent for this subscription,
and the last item is the subscription detail (the kind of events that match the subscription).

### Optional parameters

The `get subscriptions` command allows for one additional optional parameter:

```text
--output=wide or -o wide                        # show additional information
--output=custom-columns= or -o custom-columns=  # show specified information
--output=json or -o json                        # show information in JSON format
--output=yaml or -o yaml                        # show information in YAML format
--selector={s} or -l {s}                        # filter the output on label selector(s)
--field-selector={s}                            # filter the output on field selector(s)
```

You can only specify one output format at a time.  Please refer to "[Output formats](output-formats.md)" for more information.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

#### Wide view

This option shows the default columns, as above, plus the subscription ID.

```shell
opentf-ctl get subscriptions -o wide
```

```console
ID                                    NAME            ENDPOINT                     CREATION                COUNT  SUBSCRIPTION
08831072-a070-4438-a6d2-cd51b05a8f1d  actionprovider  http://127.0.0.1:9065/inbox  2021-12-14T14:50:57.12  20     "kind==ProviderCommand:opentestfactory.org/category==delete-file,opentestfactory.org/categoryPrefix==actions"
```

#### Custom view

If this optional parameter is specified, you can choose what is displayed for each registered
agent.

The columns definition format is as follows: `name:value[,name:value]*`.  `name` is the column
name in the output (such as `NAME` in the example above).  `value` is
the path to the information in the service subscription schema.

The following values are supported:

- `.metadata.name`
- `.metadata.creationTimestamp`
- `.metadata.annotations`
- `.metadata.subscription_id`
- `.spec.subscriber.endpoint`
- `.status.publicationCount`

```shell
opentf-ctl get subscriptions --output=custom-columns=NAME:.metadata.name
```

```console
NAME
junit
```

#### Using selectors

By default the `get subscriptions` command lists all subscriptions.

The command output may be restricted by using the `--selector` and `--field-selector`
options.  For example, you may restrict command output to show only quarantined
subscriptions:

```shell
opentf-ctl get subscriptions --field-selector=status.quarantine!=0
```

```text
NAME      ENDPOINT                      CREATION                    COUNT  SUBSCRIPTIONS
observer  http://127.0.0.1:7775/inbox   2023-09-19T12:42:31.183226  0      ["kind==ProviderCommand,apiVersion==opentestfactory.org/v1beta1"]
```

You can combine selectors with output format.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

## `delete subscription {subscription_id}`

This command cancels an active subscription on the event bus.

```shell
opentf-ctl delete subscription 08831072-a070-4438-a6d2-cd51b05a8f1d --context allinone
opentf-ctl delete subscription 08831072 c07d0ac5
opentf-ctl delete subscription --all
```

You can specify partial subscription IDs, as long as they are unique (if they are not, a message is
displayed and the ambiguous subscriptions are not canceled).

If publications matching this subscription were already queued, the event bus may still attempt
to dispatch them to the canceled subscription.

### Optional parameters

The `delete subscription` command allows for additional optional parameters:

```text
--all                               # delete all subscriptions
--selector={s} or -l {s}            # delete subscriptions based on label selector(s)
--field-selector={s}                # delete subscriptions based on field selector(s)
```

You cannot mix `--all` with `--selector` or `--field-selector`, and you cannot use those
options when specifying subscription IDs.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."
