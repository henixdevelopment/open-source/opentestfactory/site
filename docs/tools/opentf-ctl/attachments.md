<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Attachment Commands

The `opentf-ctl` tool provides three commands to manage workflow attachments. 

## `get attachments {workflow_id}`

This command lists attachments produced by a workflow.

```shell
opentf-ctl get attachments 396e99ec-0fe4-4f35-8b9e-69a550a0e474
opentf-ctl get attachments 396
```
```text
JOB_NAME            STEP    UUID                                    FILENAME                    TESTCASE
robot-execute-test  4       8bb0a81d-f20b-491d-bf0c-dd84a8454a4e    RobotFramework_reports.tar  test_statuses.robot
robot-execute-test  4       49dda176-25b5-4ac9-bbe6-1b64ba1c5da7    output.xml                  test_statuses.robot
<none>              <none>  68f8f5cc-36ba-439d-b15b-2c6c5e94a595_e  executionlog.txt            <none>
```

You can specify a partial workflow ID, as long as it is unique (if it is not, a 
message is displayed listing the available workflows).

The `get attachments` default output contains the attachments list, displaying, for 
each attachment, its UUID, filename, job and job step that attachment was produced 
by and the name of the related testcase (if any).

It is also possible to display, in addition, execution channel OS, attachment 
type and creation timestamp using `--output=wide` option. If you want to display 
the complete attachment data, use `--output=json|yaml` option:

??? success "Example: displaying workflow attachments in `.yaml` format"
    ```shell
    opentf-ctl get attachments 396 --output=yaml
    ``` 
    ```yaml
    [...]
    - attachment:
        filename: RobotFramework_reports.tar
        job:
          channel_id: 903c68ea-1ccc-41a6-a311-46701b01faeb
          channel_os: linux
          job_id: 4652ffbf-b20e-4e15-9dc9-b535739fa784
          job_origin: []
          name: robot-execute-test
          namespace: default
        metadata:
          creationTimestamp: '1970-01-01T14:41:25.197874'
          name: run33
          step_id: 449d1f11-8ce9-4519-a938-ed1b2cde89b5
          step_origin:
          - 9953ecf6-d60b-4529-a1ad-7c88007b129e
          - 74aa826f-cb20-4771-a167-4642ef7259be
          step_sequence_id: 31
        parent_step:
          sequence_id: 4
          step_id: 9953ecf6-d60b-4529-a1ad-7c88007b129e
          technology: robotframework
          testcase: test_statuses.robot
        type: null
    uuid: 8bb0a81d-f20b-491d-bf0c-dd84a8454a4e
    - attachment:
    filename: output.xml
    [...]
    ```
### Optional Parameters

The `get attachments` command allows for additional optional parameters, in no specific order,
but they must follow the command if used:

```text
--output=wide or -o wide                       # show additional information
--output=custom-columns= or -o custom-columns= #show specified information
--output=json or -o json                       # show information in JSON format
--output=yaml or -o yaml                       # show information in YAML format
--verbose or -v                                # show attachments source files and attachments related errors
```

You can only specify one output format at a time.  Please refer to "[Output formats](output-formats.md)" for more information.

#### Verbose Output

By default, only the successfully generated attachments are displayed in the command 
output. If you want to display also the source files of the workflow Allure report (i.e. 
the Robot Framework execution related `.json` and `.html` files) and/or the attachment 
generation errors, use the `--verbose` (or `-v`) flag.

??? success "Example: displaying workflow attachments in verbose mode"
    ```shell
    opentf-ctl get attachments 396 --verbose
    ``` 
    ```text
    ERROR,Failed to fetch file /home/user/not.exist: [Errno 2] No such file or directory: '/home/user/not.exist'.

    ERROR,Failed to fetch file /home/user/also.not.exist: [Errno 2] No such file or directory: '/home/user/also.not.exist'.

    JOB_NAME            STEP    UUID                                    FILENAME                                              TESTCASE
    robot-execute-test  4       8bb0a81d-f20b-491d-bf0c-dd84a8454a4e    RobotFramework_reports.tar                            test_statuses.robot
    robot-execute-test  4       49dda176-25b5-4ac9-bbe6-1b64ba1c5da7    output.xml                                            test_statuses.robot
    robot-execute-test  4       b30db2bc-dc1e-419c-8450-0cad83ac99b6    2b799bd6-8cbb-404b-9b68-4b014891ae7d-result.json      test_statuses.robot
    robot-execute-test  4       e112a97f-59aa-466a-8720-265ff688e414    a9bd3be9-34a6-4c90-acfe-ae3c57c80d9f-result.json      test_statuses.robot
    robot-execute-test  4       f6c4631a-b2e0-48ee-b720-5024290ab579    c9227398-3fd6-45a7-9102-3a8941ae12c7-result.json      test_statuses.robot
    robot-execute-test  4       22bd59b5-e022-4a72-8371-123dc4a0457d    4fa71d7c-7b7d-47a9-8761-71d764332827-attachment.html  test_statuses.robot
    robot-execute-test  4       a35828f9-89f7-4fa2-b61a-bdaef4adec88    7cfc7aff-4af3-43df-bf68-33d52d37e1be-attachment.html  test_statuses.robot
    robot-execute-test  4       2b863b66-f74b-4a68-a829-4680a7e6f457    7df58887-960f-4e74-b4d5-044c71658fda-attachment.html  test_statuses.robot
    robot-execute-test  4       3ee669ca-a6a8-44f1-9814-52e46f802b82    b70ab9b1-bd68-4517-9b75-ba3beed1af3b-attachment.html  test_statuses.robot
    <none>              <none>  68f8f5cc-36ba-439d-b15b-2c6c5e94a595_e  executionlog.txt                                      <none>
    ```

#### Custom View

If this optional parameter is specified, you can choose what is displayed for each known
workflow attachment.

The columns definition format is as follows: `name:value[,name:value]*`.  `name` is the column
name in the output (`NAME`, `TECHNOLOGY`, or `CREATED` in the example below).  `value` is
the path to the information in the attachments schema.

The following values are supported:

- `.attachment.filename`
- `.attachment.type`
- `.attachment.uuid`
- `.attachment.job.channel_id`
- `.attachment.job.channel_os`
- `.attachment.job.job_id`
- `.attachment.job.job_origin`
- `.attachment.job.name`
- `.attachment.job.namespace`
- `.attachment.metadata.creationTimestamp`
- `.attachment.metadata.name`
- `.attachment.metadata.step_id`
- `.attachment.metadata.step_origin`
- `.attachment.metadata.step_sequence_id`
- `.attachment.parent_step.sequence_id`
- `.attachment.parent_step.step_id`
- `.attachment.parent_step.technology`
- `.attachment.parent_step.testcase`

```shell
opentf-ctl get attachments 396 --output=custom-columns=NAME:.attachment.filename,TECHNOLOGY:.attachment.parent_step.technology,CREATED:.attachment.metadata.creationTimestamp
```
```text
NAME                        TECHNOLOGY      CREATED
RobotFramework_reports.tar  robotframework  1970-01-01T14:41:25.197874
output.xml                  robotframework  1970-01-01T14:41:25.279055
executionlog.txt            <none>          1970-01-01T14:41:25.485685
```

## `cp {workflow_id}:{...} {destination}`

This command copies a workflow attachment or workflow attachments to the specified 
destination. The command has the following syntax:

```shell
opentf-ctl cp {workflow_id}:({attachment_id} | {file_pattern}) /destination/path
```

### Optional Parameters

The `cp` command allows for additional optional parameters, in no specific order,
but they must follow the command if used:

```text
--type= or -t=      # get only attachments of specified type
```

#### Copying single attachment

If you want to copy a single attachment, use its ID. You can specify a partial workflow 
ID and/or a partial attachment ID, as long as it is unique (if it is not, a message is 
displayed listing the available workflows or attachments).

The destination path may be a directory. In this case, the attachment is saved under 
attachment original name.

??? success "Example: copying the report.html to the local filesystem"
    ```shell
    opentf-ctl cp be:ab1b22b3-2cae-4f7f-8029-0afcc53da1bf /path/to/19700101_report.html
    ```
    
    ```text
    Attachment report.html (ab1b22b3-2cae-4f7f-8029-0afcc53da1bf) is downloaded as /path/to/19700101_report.html.
    ```

#### Copying multiple attachments

If you need to copy multiple attachments, use a filename pattern instead of attachment ID. 
The destination path needs to be a directory (if it does not exist, it will be created). 
When multiple attachments are copied, a folder tree is created in the destination 
directory, matching the following schema: `job/testcase/`. Each attachment is copied 
to the `job/testcase` folder to which it is related to, execution log and Allure report 
are copied to the destination root. User-specified attachments from job and provider 
hooks are respectively copied in `job/setup|teardown` and `job/testcase` folder.

??? success "Example: copying all `.xml` attachments to the local filesystem"
    ```shell
    opentf-ctl cp be:*.xml /home/user/attachments
    ```
    
    ```text
    Attachment output.xml (5475bbf6-b588-4248-ac08-7e31b1d07049) is downloaded as /home/user/attachments/robotframework-job/1_test_backslashes_reporter.robot/output.xml.
    Attachment output.xml (705a53d1-1fd5-4618-a5bc-c3101cd0d52f) is downloaded as /home/user/attachments/robotframework-job/2_test_special_chars.robot/output.xml.
    Attachment calculator-report.xml (c33e4613-8063-45b8-a937-7359d66bc65b) is downloaded as /home/user/attachments/cypress-job/1_calculator.mult.ko.spec.js/calculator-report.xml.
    Attachment calculator-report.xml (728a1268-c525-45d2-a7c8-e1bf3f5c9080) is downloaded as /home/user/attachments/cypress-job/2_calculator.add.ok.spec.js/calculator-report.xml.
    ```

    ```shell
    tree /home/user/attachments
    ```
    ```text
    .
    ├── cypress-job
    │   ├── 1_calculator.mult.ko.spec.js
    │   │   └── calculator-report.xml
    │   └── 2_calculator.add.ok.spec.js
    │       └── calculator-report.xml
    └── robotframework-job
        ├── 1_test_backslashes_reporter.robot
        │   └── output.xml
        └── 2_test_special_chars.robot
            └── output.xml
    ```

If you need to download only the attachments of the specified type, use `--type=` 
option. 

!!! warning "Attention"
    `--type=` option also allows for Unix name patterns, but they should be properly quoted
    to avoid being interpreted by shell. 

The following attachment types are currently supported:

- `application/vnd.opentestfactory.executionlog+text`
- `application/vnd.opentestfactory.{provider_name}-surefire+xml` (for Cucumber, Cypress, JUnit, Postman, SKF and SoapUI)
- `application/vnd.opentestfactory.robotframework-output+xml`

??? success "Example: copying all Cucumber `surefire-xml` reports to the local filesystem"
    ```shell
    opentf-ctl cp be:* /home/user/attachments --type='*cypress-surefire*'
    ```
    ```text
    Attachment calculator-report.xml (c33e4613-8063-45b8-a937-7359d66bc65b) is downloaded as /home/user/attachments/cypress-job/1_calculator.mult.ko.spec.js/calculator-report.xml.
    Attachment calculator-report.xml (728a1268-c525-45d2-a7c8-e1bf3f5c9080) is downloaded as /home/user/attachments/cypress-job/2_calculator.add.ok.spec.js/calculator-report.xml.
    ```

    ```shell
    tree /home/user/attachments
    ```
    ```text
    .
    └── cypress-job
        ├── 1_calculator.mult.ko.spec.js
        │       └── calculator-report.xml
        └── 2_calculator.add.ok.spec.js
                └── calculator-report.xml
    ```

## `generate report {workflow_id} using {file}`

This command generates a report based on an insight from the user-provided definition
file, then saves it locally. The command has the following syntax:

```shell
opentf-ctl generate report {workflow_id} using {definition_file}
```

You can specify a partial workflow ID, as long as it is unique (if it is not, a message 
is displayed listing the available workflows).

The _definition file_ is mandatory.  It contains one or more report definition.

### Optional parameters

The `generate report` command allows for the following additional parameters, which can be 
specified in any order, but must follow command if used (they can be specified before the workflow ID):

```
--name={insight_name}          # report name to be generated
--save-to={destination}        # save report to the specified destination
--as={file_name}               # specify custom file name for the report    
--timeout={x}                  # set query timeout in seconds
```

`--name`, when omitted, defaults to `'*'`  (all insights from the definition file
are generated), `--save-to`, when omitted, defaults to the current directory (`.`).

!!! Tip
    `--name` option accepts Unix filename patterns. Nevertheless, you should 
    quote them properly (i.e. `... --name='on-error*'`) to avoid them being 
    interpreted by shell.

`--save-to` destination path must be a directory. If provided path does not exist, 
it will be created.

To override the report name, use the `--as` option, which allows you to specify a 
custom name for the report (without the extension). Note that when using `--as`, 
the value provided for `--name` cannot be a pattern.

For large workflows containing thousands of test cases, report generation may take 
some time, so the request may time out. Default request timeout is 60 seconds: if 
you need to raise it, you can use the `--timeout=` command-line option to define your 
own timeout in seconds.

Generated reports are saved to the destination as `{insight_name}.{extension}`.
Extension depends on the insight kind: `SummaryReport` insights provides `.html`
files and `ExecutionLog` insight `.txt` files. 

### Example

The following command generates the report `detailed-execution-report` from the `my_insights.yaml` 
definition file and saves it to the `/home/user/reports/` directory as `custom-execution-report.html`.  
Request timeout is set to five minutes.

```shell
opentf-ctl generate report 8321ab61-325c-446d-b4b9-dd76b6d93bb4 using my_insights.yaml \
    --name detailed-execution-report \
    --save-to /home/user/reports \
    --as custom-execution-report \
    --timeout 300
```
```console
Generating reports...
Attachment detailed-execution-report.html (3c206328-bcbb-4150-964b-7640aa705a50) 
is downloaded as /home/user/reports/custom-execution-report.html.
```
