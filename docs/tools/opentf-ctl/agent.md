<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Agent commands

The `opentf-ctl` tool provides a set of commands to manage _agents_.  You can list and
deregister agents.

For more information on agents, see "[Agents](../../guides/agent.md)."

## `get agents`

This command lists registered agents.

```shell
opentf-ctl get agents
```

```text
AGENT_ID                              NAME        NAMESPACES  TAGS                           REGISTRATION_TIMESTAMP      LAST_SEEN_TIMESTAMP         RUNNING_JOB
53fb7037-d91a-4df1-9181-dacfee6d273f  test agent  default     ["windows", "robotframework"]  2023-09-08T14:42:45.557246  2023-09-08T15:18:57.617358  <none>
a285a1dc-7813-4237-bc93-1b90c17c6274  test agent  default     ["linux", "robotframework"]    2023-09-08T15:18:40.421573  2023-09-08T15:19:00.456028  <none>
```

It returns _agent IDs_, which are unique identifiers that can be used by the
`delete agent` command.

The last column, `RUNNING_JOB`, is `<none>` if the agent is currently idle.  It contains
the _job ID_ it is currently processing otherwise.

### Optional parameters

The `get agents` command allows for additional optional parameters:

```text
--output=wide or -o wide                        # show additional information
--output=custom-columns= or -o custom-columns=  # show specified information
--output=json or -o json                        # show information in JSON format
--output=yaml or -o yaml                        # show information in YAML format
--selector={s} or -l {s}                        # filter the output on label selector(s)
--field-selector={s}                            # filter the output on field selector(s)
```

You can only specify one output format at a time.  Please refer to "[Output formats](output-formats.md)" for more information.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

#### Wide view

This option shows the default columns, as above.

```shell
opentf-ctl get agents -o wide
opentf-ctl get agents --output=wide
```

```text
AGENT_ID                              NAME        NAMESPACES  TAGS                           REGISTRATION_TIMESTAMP      LAST_SEEN_TIMESTAMP         RUNNING_JOB
53fb7037-d91a-4df1-9181-dacfee6d273f  test agent  default     ["windows", "robotframework"]  2023-09-08T14:42:45.557246  2023-09-08T15:18:57.617358  <none>
a285a1dc-7813-4237-bc93-1b90c17c6274  test agent  default     ["linux", "robotframework"]    2023-09-08T15:18:40.421573  2023-09-08T15:19:00.456028  <none>
```

#### Custom view

If this optional parameter is specified, you can choose what is displayed for each registered
agent.

The columns definition format is as follows: `name:value[,name:value]*`.  `name` is the column
name in the output (`AGENT_ID` or `NAME` in the example above).  `value` is
the path to the information in the agent registration schema.

The following values are supported:

- `.metadata.agent_id`
- `.metadata.name`
- `.metadata.namespaces`
- `.metadata.creationTimestamp`
- `.spec.tags`
- `.status.lastCommunicationTimestamp`
- `.status.currentJobID`

```shell
opentf-ctl get agents -o custom-columns=ID:.metadata.agent_id,NAME:.metadata.name
opentf-ctl get agents --output=custom-columns=ID:.metadata.agent_id,NAME:.metadata.name
```

```text
ID                                    NAME
53fb7037-d91a-4df1-9181-dacfee6d273f  test agent
a285a1dc-7813-4237-bc93-1b90c17c6274  test agent
```

#### Using selectors

By default the `get agents` command lists all registered agents.

The command output may be restricted by using the `--selector` and `--field-selector`
options.  For example, you may restrict command output to show only agents providing the
`windows` tag:

```shell
opentf-ctl get agents --field-selector="(windows) in spec.tags"
```

```text
AGENT_ID                              NAME        NAMESPACES  TAGS                           REGISTRATION_TIMESTAMP      LAST_SEEN_TIMESTAMP         RUNNING_JOB
53fb7037-d91a-4df1-9181-dacfee6d273f  test agent  default     ["windows", "robotframework"]  2023-09-08T14:42:45.557246  2023-09-08T15:21:53.044245  <none>
```

You can combine selectors with output format.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."

## `delete agent {agent_id}`

This command de-registers agents.

```shell
opentf-ctl delete agent 6c223f7b-3f79-4c51-b200-68eaa33c1325
opentf-ctl delete agent 6c223f7b c07d0ac5
opentf-ctl delete agent --all
```

If the agent is currently processing a job, the corresponding workflow may fail
and the workspace in the execution environment may remain.

You can specify partial agent IDs, as long as they are unique (if they are not, a message is
displayed and the ambiguous agents are not de-registered).

### Optional parameters

The `delete agent` command allows for additional optional parameters:

```text
--all                               # de-register all running agents
--selector={s} or -l {s}            # de-register agents based on label selector(s)
--field-selector={s}                # de-register agents based on field selector(s)
```

You cannot mix `--all` with `--selector` or `--field-selector`, and you cannot use those
options when specifying agent IDs.

For more information on selectors, see  "[selectors](label-and-field-selectors.md)."
