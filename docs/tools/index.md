<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Tools

This section of the OpenTestFactory orchestrator documentation contains pages that show how
to do individual tasks. A task page shows how to do a single thing, typically by giving a
short sequence of steps.

## [Install Tools](install.md)

Set up the OpenTestFactory orchestrator tools on your computer.

## [Running commands (`opentf-ctl`)](opentf-ctl/index.md)

Run workflows, inspect and manage orchestrator resources, and view workflow logs.

## [Starting the orchestrator](start-and-wait.md)

Wait until the orchestrator is ready to accept workflows.

## [Stopping the orchestrator](wait-and-stop.md)

Wait until the orchestrator is ready to be killed.
