<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Stopping the orchestrator

When the OpenTestFactory orchestrator is instantiated on demand, some tasks may
still be running after the workflow has completed.

If the orchestrator service is stopped before the end of those pending tasks, some
reports may end in an inconsistent state.

This tool waits until the orchestrator service idles.  It can safely be stopped when
idle.

## `opentf-done`

This tool can be used to query the status of the orchestrator.

```text
usage: opentf-done [-h] [--timeout TIMEOUT] [--polling_delay POLLING_DELAY] --host HOST [--port PORT] --token TOKEN

OpenTestFactory Orchestrator availability checker

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           target host with protocol (e.g., https://example.local)
  --token TOKEN         token
  --port PORT           target port (default to 7775)
  --polling_delay POLLING_DELAY
                        polling delay in seconds (default to 5)
  --timeout TIMEOUT     verification timeout in seconds (default to 3600)
```

It returns an error code 0 if the orchestrator is idle.

It returns an error code 1 if the timeout verification delay expires.  Pending tasks
are still running.

It returns an error code 2 if a technical error occurred while checking the orchestrator
status.

## Example

In the following example, the OpenTestFactory orchestrator is deployed using
`docker-compose`.

```bash
docker-compose up -d
...
opentf-done --host http://localhost --token $TOKEN
docker-compose down
```
