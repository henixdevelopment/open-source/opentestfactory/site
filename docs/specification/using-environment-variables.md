<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using environment variables

_The OpenTestFactory orchestrator sets default environment variables for each orchestrator workflow run. Custom environment variables can also be set in a workflow file._

## About environment variables

The OpenTestFactory orchestrator sets default environment variables that are available to every step in a workflow run. Environment variables are case-sensitive. Commands run in functions or steps can create, read, and modify environment variables.

To set custom environment variables, you need to specify the variables in the workflow file. You can define environment variables for a step, job, or entire workflow using the `jobs.<job_id>.steps.variables`, `jobs.<job_id>.variables`, and `variables` keywords. For more information, see "[Workflow syntax for OpenTestFactory orchestrator](/using-workflows/workflows.html)."

```yaml
jobs:
  weekday_job:
    runs-on: linux
    variables:
      DAY_OF_WEEK: Mon
    steps:
      - name: "Hello world when it's Monday"
        if: ${{ variables.DAY_OF_WEEK == 'Mon' }}
        run: echo "Hello $FIRST_NAME $middle_name $Last_Name, today is Monday!"
        variables:
          FIRST_NAME: Mona
          middle_name: The
          Last_Name: Octocat
```

Variables specified in the workflow file are subject to the execution environment shell's expansions and substitution rules.  Use the target shell's escape conventions if you need the literal content.

```yaml
jobs:
  substitution-demo-linux:
    runs-on: linux
    variables:
      MY_WORK_DIR: $HOME/work_dir
      LITERAL_VAL: '''$HOME/work_dir'''
    steps:
    - run: echo "$MY_WORK_DIR"             # Prints '/home/<user>/work_dir'
    - run: echo "$LITERAL_VAL"             # Prints '$HOME/work_dir'
  substitution-demo-windows:
    runs-on: windows
    variables:
      MY_WORK_DIR: '%USERPROFILE%\work_dir'
      LITERAL_VAL: '^%USERPROFILE^%\work_dir'
    steps:
    - run: echo %MY_WORK_DIR%              # Prints 'C:\Users\<user>\work_dir'
    - run: echo %LITERAL_VAL%              # Prints '%USERPROFILE%\work_dir'
```

To use the value of an environment variable in a workflow file, you should use the
[`variables` context](contexts.md#variables-context). If you want
to use the value of an environment variable inside an execution environment, you can use
the execution environment operating system's normal method for reading environment variables.

If you use the workflow file's `run` key to read environment variables from within the
execution environment operating system (as shown in the example above), the variable is
substituted in the execution environment operating system after the job is sent to the
execution environment. For other parts of a workflow file, you must use the `variables`
context to read environment variables; this is because workflow keys (such as `if`) require
the variable to be substituted during workflow processing before it is sent to the execution
environment.

You can also use the `OPENTF_VARIABLES` environment file to set an environment variable that
the following steps in a workflow can use. The environment file can be used directly by a
function or by a shell command in a workflow file using the `run` keyword. For more information,
see "[Workflow commands for OpenTestFactory Orchestrator Plugins](/using-workflows/workflow-commands.html#setting-an-environment-variable)."

## Default environment variables

We strongly recommend that functions use environment variables to access the file system rather
than using hard-coded file paths. The OpenTestFactory orchestrator sets environment variables
for functions to use in all execution environments.

| Environment variable | Description
| --- | ---
| CI | Always set to `true` if started from a CI pipeline.
| HOME | The path to the OpenTestFactory home directory used to store user data. For example, `/opentf/home`.
| OPENTF_ACTOR | The name of the person or app that initiated the workflow. For example, octocat.
| OPENTF_WORKSPACE  | The OpenTestFactory orchestrator workspace directory path. The workspace directory contains a subdirectory with a copy of your repository if your workflow uses the actions/checkout function. If you don't use the actions/checkout function, the directory will be empty. For example, `/home/runner/work/my-repo-name/my-repo-name`.
| OPENTF_VARIABLES  | The path of the environment variables file.  Steps can add their own variables definitions in this file, and they will be available for the remaining steps in the current job.

### Determining when to use default environment variables or contexts

The OpenTestFactory orchestrator includes a collection of variables called _contexts_ and a
similar collection of variables called _default environment variables_.  These variables are
intended for use at different points in a workflow:

- **Default environment variables**: These variables exist only in the execution environment
  that is executing your job.  For more information, see "[Default environment variables](#default-environment-variables)."
- **Contexts**: You can use most contexts at any point in your workflow, including when
  _default environment variables_ would be unavailable.  For example, you can use contexts
  with expressions to perform initial processing before the job is routed to an execution
  environment for execution; this allows you to use a context with the conditional `if`
  keyword to determine whether a step should run.  Once a job is running, you can also
  retrieve context variables from the execution environment that is executing the job,
  such as `runner.os`.  For details of where you can use various contexts within a
  workflow, see "[Context availability.](contexts.md#contexts)"

## Naming conventions for environment variables

!!! Note
    The OpenTestFactory orchestrator reserves the `OPENTF_` environment variable prefix for
    internal use by the orchestrator. Setting an environment variable or secret with the
    `OPENTF_` prefix will result in an error.

Any new environment variables you set that point to a location on the file system should
have a `_PATH` suffix. The `HOME` and `OPENTF_WORKSPACE` default variables are exceptions
to this convention because the words "home" and "workspace" already imply a location.
