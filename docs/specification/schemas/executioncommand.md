<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# ExecutionCommand schema

```json
{
    "$schema": "https://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1alpha1 ExecutionCommand manifests",
    "type": "object",
    "properties": {
        "apiVersion": {
            "const": "opentestfactory.org/v1"
        },
        "kind": {
            "const": "ExecutionCommand"
        },
        "metadata": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "namespace": {
                    "type": "string",
                    "pattern": "^[a-z0-9][a-z0-9-]*$"
                },
                "workflow_id": {
                    "type": "string"
                },
                "job_id": {
                    "type": "string"
                },
                "job_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "step_id": {
                    "type": "string"
                },
                "step_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "step_origin_status": {
                    "type": "object",
                    "patternProperties": {
                        "^[a-zA-Z0-9-]+$": {
                            "type": "number"
                        }
                    }
                },
                "step_sequence_id": {
                    "type": "number"
                },
                "labels": {
                    "type": "object",
                    "patternProperties": {
                        "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {
                            "type": "string"
                        }
                    },
                    "minProperties": 1,
                    "additionalProperties": false
                }
            },
            "additionalProperties": true,
            "required": [
                "name",
                "workflow_id",
                "job_id",
                "job_origin",
                "step_sequence_id"
            ]
        },
        "runs-on": {
            "$ref": "#/definitions/runs-on-maybe-empty-const-array"
        },
        "scripts": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "shell": {
            "type": "string"
        },
        "variables": {
            "$ref": "#/definitions/variables"
        },
        "working-directory": {
            "type": "string"
        }
    },
    "required": [
        "apiVersion",
        "kind",
        "metadata",
        "runs-on",
        "scripts"
    ],
    "additionalProperties": false,
    "definitions": {
        "runs-on-maybe-empty-const-array": {
            "type": "array",
            "items": {
                "type": "string",
                "pattern": "^[a-zA-Z][a-zA-Z0-9-]*$"
            },
            "minItems": 0
        },
        "variables": {
            "type": "object",
            "patternProperties": {
                "^[a-zA-Z0-9_]+$": {
                    "oneOf": [
                        {
                            "type": "string"
                        },
                        {
                            "type": "object",
                            "properties": {
                                "value": {
                                    "type": "string"
                                },
                                "verbatim": {
                                    "type": "boolean"
                                }
                            },
                            "required": [
                                "value"
                            ],
                            "additionalProperties": false
                        }
                    ]
                }
            },
            "minProperties": 1
        }
    }
}
```
