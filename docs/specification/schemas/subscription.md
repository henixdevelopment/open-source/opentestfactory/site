<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Subscription schema

```json
{
    "$schema": "https://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1 Subscription manifests",
    "type": "object",
    "properties": {
        "apiVersion": {
            "enum": [
                "opentestfactory.org/v1alpha1",
                "opentestfactory.org/v1"
            ]
        },
        "kind": {
            "const": "Subscription"
        },
        "metadata": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                }
            },
            "additionalProperties": true,
            "required": [
                "name"
            ]
        },
        "spec": {
            "type": "object",
            "properties": {
                "selector": {
                    "type": "object",
                    "properties": {
                        "matchKind": {
                            "type": "string"
                        },
                        "matchLabels": {
                            "type": "object",
                            "patternProperties": {
                                "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {
                                    "type": "string"
                                }
                            },
                            "minProperties": 1,
                            "additionalProperties": false
                        },
                        "matchExpressions": {
                            "$ref": "#/definitions/labelexpressions"
                        },
                        "matchFields": {
                            "type": "object",
                            "patternProperties": {
                                "^[a-z]([a-zA-Z0-9.-]*[a-zA-Z0-9])?$": {
                                    "type": "string"
                                }
                            },
                            "minProperties": 1,
                            "additionalProperties": false
                        },
                        "matchFieldExpressions": {
                            "$ref": "#/definitions/fieldexpressions"
                        }
                    },
                    "minProperties": 1,
                    "additionalProperties": false
                },
                "subscriber": {
                    "type": "object",
                    "properties": {
                        "endpoint": {
                            "type": "string"
                        },
                        "insecure-skip-tls-verify": {
                            "type": "boolean"
                        }
                    },
                    "additionalProperties": false,
                    "required": [
                        "endpoint"
                    ]
                },
                "queues": {
                    "type": "number"
                },
                "rateLimit": {
                    "type": "number"
                },
                "backoff": {
                    "type": "object",
                    "properties": {
                        "duration": {
                            "type": "string"
                        },
                        "factor": {
                            "type": "number"
                        },
                        "jitter": {
                            "type": "number"
                        },
                        "steps": {
                            "type": "number"
                        },
                        "retries": {
                            "type": "number"
                        }
                    },
                    "additionalProperties": false,
                    "required": [
                        "duration",
                        "factor",
                        "jitter",
                        "steps",
                        "retries"
                    ]
                }
            },
            "required": [
                "subscriber"
            ],
            "additionalProperties": false
        }
    },
    "required": [
        "apiVersion",
        "kind",
        "metadata",
        "spec"
    ],
    "additionalProperties": false,

    "definitions": {
        "existexpression": {
            "type": "object",
            "properties": {
                "key": {
                    "type": "string"
                },
                "operator": {
                    "enum": [
                        "Exists",
                        "DoesNotExist"
                    ]
                }
            },
            "additionalProperties": false,
            "required": [
                "key",
                "operator"
            ]
        },
        "inexpression": {
            "type": "object",
            "properties": {
                "key": {
                    "type": "string"
                },
                "operator": {
                    "enum": [
                        "In",
                        "NotIn"
                    ]
                },
                "values": {
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "type": "string"
                    }
                }
            },
            "additionalProperties": false,
            "required": [
                "key",
                "operator",
                "values"
            ]
        },
        "setexpression": {
            "type": "object",
            "properties": {
                "key": {
                    "type": "string"
                },
                "operator": {
                    "enum": [
                        "In",
                        "NotIn",
                        "ContainsAll",
                        "DoesNotContainAll"
                    ]
                },
                "values": {
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "type": "string"
                    }
                }
            },
            "additionalProperties": false,
            "required": [
                "key",
                "operator",
                "values"
            ]
        },
        "fieldexpressions": {
            "type": "array",
            "minItems": 1,
            "items": {
                "anyOf": [
                    {
                        "$ref": "#/definitions/setexpression"
                    },
                    {
                        "$ref": "#/definitions/existexpression"
                    }
                ]
            }
        },
        "labelexpressions": {
            "type": "array",
            "minItems": 1,
            "items": {
                "anyOf": [
                    {
                        "$ref": "#/definitions/inexpression"
                    },
                    {
                        "$ref": "#/definitions/existexpression"
                    }
                ]
            }
        }
    }
}
```
