<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# ProviderResult schema

```json
{
    "$schema": "https://json-schema.org/draft/2019-09/schema#",
    "title": "JSON SCHEMA for opentestfactory.org/v1 ProviderResult manifests",
    "type": "object",
    "properties": {
        "apiVersion": {
            "enum": [
                "opentestfactory.org/v1alpha1",
                "opentestfactory.org/v1beta1",
                "opentestfactory.org/v1"
            ]
        },
        "kind": {
            "const": "ProviderResult"
        },
        "metadata": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "namespace": {
                    "type": "string",
                    "pattern": "^[a-z0-9][a-z0-9-]*$"
                },
                "workflow_id": {
                    "type": "string"
                },
                "job_id": {
                    "type": "string"
                },
                "job_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "step_id": {
                    "type": "string"
                },
                "step_origin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "labels": {
                    "type": "object",
                    "patternProperties": {
                        "^([a-zA-Z0-9-.]+/)?[a-zA-Z0-9]([a-zA-Z0-9._-]*[a-zA-Z0-9])?$": {
                            "type": "string"
                        }
                    },
                    "minProperties": 1,
                    "additionalProperties": false
                }
            },
            "additionalProperties": true,
            "required": [
                "name",
                "workflow_id",
                "job_id",
                "job_origin",
                "step_id",
                "step_origin"
            ]
        },
        "hooks": {
            "$ref": "#/definitions/hooks"
        },
        "steps": {
            "type": "array",
            "minItems": 0,
            "items": {
                "anyOf": [
                    {
                        "type": "object",
                        "properties": {
                            "name": {
                                "type": "string"
                            },
                            "id": {
                                "type": "string"
                            },
                            "if": {
                                "type": "string"
                            },
                            "uses": {
                                "type": "string"
                            },
                            "with": {
                                "$ref": "#/definitions/with"
                            },
                            "variables": {
                                "$ref": "#/definitions/variables"
                            },
                            "timeout-minutes": {
                                "$ref": "#/definitions/number-expression"
                            },
                            "continue-on-error": {
                                "$ref": "#/definitions/boolean-expression"
                            },
                            "working-directory": {
                                "type": "string"
                            }
                        },
                        "required": [
                            "uses"
                        ],
                        "additionalProperties": false
                    },
                    {
                        "type": "object",
                        "properties": {
                            "name": {
                                "type": "string"
                            },
                            "id": {
                                "type": "string"
                            },
                            "if": {
                                "type": "string"
                            },
                            "run": {
                                "type": "string"
                            },
                            "shell": {
                                "type": "string"
                            },
                            "variables": {
                                "$ref": "#/definitions/variables"
                            },
                            "timeout-minutes": {
                                "$ref": "#/definitions/number-expression"
                            },
                            "continue-on-error": {
                                "$ref": "#/definitions/boolean-expression"
                            },
                            "working-directory": {
                                "type": "string"
                            }
                        },
                        "required": [
                            "run"
                        ],
                        "additionalProperties": false
                    }
                ]
            }
        },
        "outputs": {
            "$ref": "#/definitions/outputs"
        }
    },
    "required": [
        "apiVersion",
        "kind",
        "metadata",
        "steps"
    ],
    "additionalProperties": false,
    "definitions": {
        "number-expression": {
            "anyOf": [
                {
                    "type": "number"
                },
                {
                    "type": "string",
                    "pattern": "^\\$\\{\\{.*\\}\\}$"
                }
            ]
        },
        "boolean-expression": {
            "anyOf": [
                {
                    "type": "boolean"
                },
                {
                    "type": "string",
                    "pattern": "^\\$\\{\\{.*\\}\\}$"
                }
            ]
        },
        "variables": {
            "type": "object",
            "patternProperties": {
                "^[a-zA-Z0-9_]+$": {
                    "oneOf": [
                        {
                            "type": "string"
                        },
                        {
                            "type": "object",
                            "properties": {
                                "value": {
                                    "type": "string"
                                },
                                "verbatim": {
                                    "type": "boolean"
                                }
                            },
                            "required": [
                                "value"
                            ],
                            "additionalProperties": false
                        }
                    ]
                }
            },
            "minProperties": 1
        },
        "hooks": {
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "if": {
                        "type": "string"
                    },
                    "events": {
                        "type": "array",
                        "minItems": 1
                    },
                    "before": {
                        "type": "array",
                        "minItems": 1
                    },
                    "after": {
                        "type": "array",
                        "minItems": 1
                    }
                },
                "anyOf": [
                    {
                        "required": [
                            "name",
                            "events",
                            "before"
                        ]
                    },
                    {
                        "required": [
                            "name",
                            "events",
                            "after",
                            "before"
                        ]
                    },
                    {
                        "required": [
                            "name",
                            "events",
                            "after"
                        ]
                    }
                ],
                "additionalProperties": false
            }
        },
        "with": {
            "type": "object"
        },
        "outputs": {
            "type": "object",
            "patternProperties": {
                "^[a-zA-Z_][a-zA-Z0-9_-]*$": {
                    "oneOf": [
                        {
                            "type": "object",
                            "properties": {
                                "description": {
                                    "type": "string"
                                },
                                "value": {
                                    "type": "string"
                                }
                            },
                            "required": [
                                "value"
                            ],
                            "additionalProperties": false
                        },
                        {
                            "type": "string"
                        }
                    ]
                }
            },
            "minProperties": 1
        }
    }
}
```
