<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Status messages

Responses follow the following format:

```json
{
  "apiVersion": "v1",
  "kind": "Status",
  "metadata": {},
  "status": "Success", // if code // 100 == 2 else "Failure",
  "message": "...",
  "reason": "...",
  "code": 200
  // ...
}
```

Where `code` is a standard HTTP return code, `reason` an element of the following table
which results from the return code, `message` a descriptive text, and `details` an
optional part whose nature depends on the exchange.

| Reason          | Code |
|:----------------|------|
| OK              | 200  |
| Created         | 201  |
| NoContent       | 204  |
| BadRequest      | 400  |
| Unauthorized    | 401  |
| PaymentRequired | 402  |
| Forbidden       | 403  |
| NotFound        | 404  |
| AlreadyExists   | 409  |
| Conflict        | 409  |
| Invalid         | 422  |
| Internalerror   | 500  |
