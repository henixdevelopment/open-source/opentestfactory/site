<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Finding and customizing plugins

_Functions and generators provide the building blocks that power your workflows.
A workflow can use functions and generators provided by plugins created by the
community, or you can create your own functions and generators._

This guide will show you how to discover, use, and customize functions and generators.

## Overview

The functions and generators you use in your workflows come from plugins registered on
your orchestrator.  They can run in:

- Your OpenTestFactory orchestrator deployment
- Their own container in a cluster
- Your workstation
- A job in your CI pipeline

Your orchestrator administrator may restrict where plugins can run.

## Browsing plugins

You can search and browse plugins directly in this documentation, in the
[Providers](../providers/index.md) section.

## Adding a function or generator to your workflow

A function or generator listing page includes the function or generator version
and the workflow syntax required to use it.  To keep your workflow stable even when
updates are made to a function or generator, you can reference the version of the
function or generator by specifying the tag number in your workflow file.

1. Navigate to the function or generator you want to use in your workflow

2. Under "Example", click ![copy to clipboard](../resources/copy.jpg) to copy the workflow
   syntax.

    ![actions/checkout@v2](../resources/actioncheckoutv2.jpg)

3. Paste the syntax as a new step or job in your workflow.  For more information,
   see "[Workflow syntax for OpenTestFactory Orchestrator.](../specification/workflows.md)"

4. If the function or generator requires you to provide inputs, set them in your
   workflow.  For information on inputs a function or generator might require, see
   "[Using inputs and outputs.](#using-inputs-and-outputs)"

## Using inputs and outputs

A function or generator often accepts or requires inputs and generates outputs that
you can use. For example, a function might require you to specify a path to a file,
the name of a label, or other data it will use as part of the function's processing.

To see the inputs and outputs of a plugin, check the `plugin.yaml` in the root of the
plugin's repository.

In this example `plugin.yaml`, the `inputs` keyword defines a required input called
`file-path`, and includes a default value that will be used if none is specified.
The `outputs` keyword defines an output called `results-file`, which tells you where
to locate the results.

```yaml
name: 'Example'
description: 'Receives file and generates output'
inputs:
  file-path:  # id of input
    description: "Path to test script"
    required: true
    default: 'test-file.js'
outputs:
  results-file: # id of output
    description: "Path to results file"
```

## Next steps

To continue learning about the OpenTestFactory Orchestrator, see
"[Essential features of OpenTestFactory Orchestrator.](essential-features-of-opentf-orchestrator.md)"
