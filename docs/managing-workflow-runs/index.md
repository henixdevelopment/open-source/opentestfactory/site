<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Managing workflow runs

## [Triggering a workflow](trigger-a-workflow.md)

How to automatically trigger OpenTestFactory orchestrator workflows.

## [Manually running a workflow](manually-run-a-workflow.md)

You can run a workflow using the opentf-ctl CLI or the REST API.

## [Monitoring a workflow run](monitor-a-workflow-run.md)

You can monitor the progress of a workflow run in the OpenTestFactory orchestrator.

## [Canceling a workflow](cancel-a-workflow.md)

You can cancel a workflow run that is in progress.  When you cancel a workflow run,
the orchestrator cancels all jobs and steps that are part of that workflow.

## [Downloading workflow attachments](download-attachments.md)

You can download generated attachments before they automatically expire.
