<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Monitoring a workflow

You can monitor the progress of a workflow run in the OpenTestFactory orchestrator.

By default, the OpenTestFactory orchestrator stores runs logs for 1 hour,
and you can customize this retention period.  For more information, see "[Configuring the retention policy](../services/observer.md#retention-policy)."

## Monitoring a workflow run

1.  Using the OpenTestFactory CLI, list the running workflow runs:

    ```bash
    opentf-ctl get workflows
    ```

    ```text
    WORKFLOW_ID      STATUS   NAME
    <workflow_id_1>  RUNNING  Test Orchestrator with No Initial Environments
    <workflow_id_2>  DONE     Workflow end-to-end
    ```

2. Get the status of a workflow run:

    ```bash
    opentf-ctl get workflow <workflow_id_2>
    ```

    ```text
    Workflow Workflow end-to-end TestSuite 32e2db32-899a-4dca-af6e-1ae780d3d1d6
    (running in namespace 'default')
    [2024-07-26T12:14:33] [job 87a90239-c82a-4c83-97c9-664bb7bad224] Running function launchGenerator
    [2024-07-26T12:14:33] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] Requesting execution environment providing ['linux', 'cypress'] in namespace 'default' for job 'squashTMJob-0'
    [2024-07-26T12:14:37] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] Cloning into 'cypress-param-calc-single-spec'...
    [2024-07-26T12:14:37] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] warning: redirecting to https://gitlab.com/henixdevelopment/open-source/opentestfactory/test-samples/cypress/cypress-param-calc-single-spec.git/
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] ====================================================================================================
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   (Run Starting)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Cypress:        12.12.0
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Browser:        Electron 106 (headless)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Node Version:   v18.16.0 (/usr/local/bin/node)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Specs:          1 found (params.ultimate.special.cy.js)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Searched:       cypress/e2e/params.ultimate.special.cy.js
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   └────────────────────────────────────────────────────────────────────────────────────────────────┘
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] ────────────────────────────────────────────────────────────────────────────────────────────────────
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   Running:  params.ultimate.special.cy.js
     (1 of 1)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   (Results)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Tests:        1
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Passing:      1
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Failing:      0
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Pending:      0
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Skipped:      0
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Screenshots:  0
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Video:        false
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Duration:     2 seconds
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ Spec Ran:     params.ultimate.special.cy.js
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   └────────────────────────────────────────────────────────────────────────────────────────────────┘
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] ====================================================================================================
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   (Run Finished)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]        Spec                                              Tests  Passing  Failing  Pending  Skipped
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   │ ✔  params.ultimate.special.cy.js            00:02        1        1        -        -        - │
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]   └────────────────────────────────────────────────────────────────────────────────────────────────┘
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]     ✔  All specs passed!                        00:02        1        1        -        -        -
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] [47691:0726/101507.367051:ERROR:zygote_host_impl_linux.cc(263)] Failed to adjust OOM score of renderer with pid 47891: Permission denied (13)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb]
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] DevTools listening on ws://127.0.0.1:42129/devtools/browser/95384b54-9214-4f28-a40f-ea8ee69cd902
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] libva error: vaGetDriverNameByIndex() failed with unknown libva error, driver_name = (null)
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] [47891:0726/101507.868442:ERROR:gpu_memory_buffer_support_x11.cc(44)] dri3 extension not supported.
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] Couldn't determine Mocha version
    [2024-07-26T12:17:03] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] [47691:0726/101540.469817:ERROR:zygote_host_impl_linux.cc(263)] Failed to adjust OOM score of renderer with pid 48133: Permission denied (13)
    [2024-07-26T12:17:12] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] ERROR,The specified patterns do not match any files.
    [2024-07-26T12:17:13] [job edc2dc3b-d643-4d13-a44e-de5b37bbb9eb] Releasing execution environment for job 'squashTMJob-0'
    Workflow completed successfully.
    ```

IF you want to see the progress as it occurs, you can use the `--watch` flag.

You can specify the level of details you want to see in the logs by using the `--step-depth={n}`
or `--job-depth={n}` command-line options.

You can also configure the output format if you want to use the logs in another tool or script by using the `--output={format}` command-line option.

For more information, see "[Monitoring the running status of a workflow](../tools/opentf-ctl/basic.md#get-workflow-workflow_id)."
