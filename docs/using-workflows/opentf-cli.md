<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Using the OpenTestFactory orchestrator CLI

_You can use the OpenTestFactory orchestrator CLI to interact with the orchestrator and perform
various tasks._

## Overview

The OpenTestFactory orchestrator CLI is a collection of command-line tools that you can use to
interact with the orchestrator.  The CLI is designed to be easy to use and to provide a rich set of
features for managing your workflow runs and your orchestrators.

## `opentf-ctl`

The `opentf-ctl` command-line tool allows you to run commands against orchestrators.  You can
use `opentf-ctl` to run workflows, inspect and manage orchestrator resources, and view
workflow logs.

The following are some basic commands that you can use with `opentf-ctl`:

- [`get workflows`](../tools/opentf-ctl/basic.md#get-workflows) List active and recent workflows
- [`run workflow {filename}`](../tools/opentf-ctl/basic.md#run-workflow-file_name) Start a workflow
- [`get workflow {workflow_id}`](../tools/opentf-ctl/basic.md#get-workflow-workflow_id) Get a workflow status
- [`kill workflow {workflow_id`](../tools/opentf-ctl/basic.md#kill-workflow-workflow_id) Cancel a running workflow

Some other commands can be of interest while interacting with workflows:

- [`get attachments {workflow_id}`](../tools/opentf-ctl/attachments.md#get-attachments-workflow_id) List workflow attachments
- [`cp {workflow_id}:{...} {destination}`](../tools/opentf-ctl/attachments.md#cp-workflow_id-destination) Copy a workflow attachment or workflow attachments to a local destination
- [`get agents`](../tools/opentf-ctl/agent.md#get-agents) List registered agents
- [`get channels`](../tools/opentf-ctl/channel.md#get-channels) List known channels

Finally, you can use the following commands to interact with quality gates:

- [`get qualitygate {workflow_id}`](../tools/opentf-ctl/qualitygate.md#get-qualitygate-workflow_id) Get quality gate status for a workflow
- [`describe qualitygate {workflow_id}`](../tools/opentf-ctl/qualitygate.md#describe-qualitygate-workflow_id) Get description of a quality gate status for a workflow

For more information, see "[opentf-ctl](../tools/opentf-ctl/)".

## `opentf-ready`

The recommended way to deploy the OpenTestFactory orchestrator is as a service in your environment.

In some contexts, you may want to start it, use it, and stop it when done.

Starting the orchestrator depends on your environment. It may be deployed as a standalone deployment
using `docker` or `docker-compose`, or it can be deployed as a side container in your CI worker, or some
other way.

In those contexts, there is a need to ensure the dynamically deployed orchestrator is ready to accept workflows.

For more information, see "[Starting the orchestrator](../tools/start-and-wait.md)."

## `opentf-done`

When the OpenTestFactory orchestrator is instantiated on demand, some tasks may still be running after the workflow has completed.

If the orchestrator service is stopped before the end of those pending tasks, some reports may end in an inconsistent state.

This tool waits until the orchestrator service idles. It can safely be stopped when idle.

For more information, see "[Stopping the orchestrator](../tools/wait-and-stop.md)."
