<!--
Copyright (c) Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Manually running a workflow

You can run a workflow using the `opentf-ctl` CLI or the REST API.

## Running a workflow using the CLI

To run a workflow manually, use the `opentf-ctl run workflow` command.  You can specify the
workflow file to run and the variables and files to use.

```shell
opentf-ctl run workflow my-workflow.yaml
```

For more information about using the OpenTestFactory CLI, see "[Starting a workflow](../tools/opentf-ctl/basic.md#run-workflow-file_name)."

<h3>Example: Specifying variables</h3>

You can specify variables to use when running the workflow using the `-e` option.  For example,
to specify the `build` variable, use the following command:

```shell
opentf-ctl run workflow my-workflow.yaml -e build=123
```

<h3>Example: Specifying files</h3>

You can specify files to use when running the workflow using the `-f` option.  For example,
to specify the `my-file.txt` file, use the following command:

```shell
opentf-ctl run workflow my-workflow.yaml -f file1=my-file.txt
```

<h3>Example: Specifying the execution environment</h3>

You can specify additional tags to use when running the workflow using the `--tags` option.
For example, to specify the `my-env` execution environment, use the following command:

```shell
opentf-ctl run workflow my-workflow.yaml --tags my-env
```

## Running a workflow using the REST API

When using the REST API, you can configure the `variables` and `files` as request body
parameters.  If the variables are omitted, the default values defined in the workflow
file are used.

For more information about using the REST API, see "[User-facing endpoints](../services/endpoints.md)."
