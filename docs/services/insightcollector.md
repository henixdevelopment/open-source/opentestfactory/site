<!--
Copyright (c) 2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Insight Collector Service

This collector service collects and publishes various reports at the end of 
each workflow. These reports are available as workflow attachments. Currently, 
the following reports are published:

- Execution log: workflow execution log (`executionlog.txt` by default)
- Summary report: workflow summary report (`executionreport.html` by default)
- Surefire XML report: workflow summary report in Surefire XML format (`executionreport.xml` by default)

This service exposes one [user-facing endpoint](endpoints.md#workflowsworkflow_idinsights-post).

## Environment Variables

### Logging

You can set the `INSIGHTCOLLECTOR_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `INSIGHTCOLLECTOR_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

### Configuration Options

You can also set three service-specific configuration options using environment variables:

- `INSIGHTCOLLECTOR_WORKERS_MAX_WAIT_SECONDS` sets the maximum duration to wait for workflow workers to complete their jobs (defaults to 30).
- `INSIGHTCOLLECTOR_CONFIGURATIONS_PATH` sets the path to the directory containing reports configuration files (defaults to `/app/insights`).
- `INSIGHTCOLLECTOR_DEFAULT_SCOPE` sets the default scope to use when no scope is provided in the insight (defaults to `'true'`).

If those environment variables are defined they override the values set in the configuration file.

### Reports Specification

The `INSIGHTCOLLECTOR_CONFIGURATION` (all upper-cased) environment variable, if defined, 
must refer to an existing file that contains reports configuration.
  
See "[Reports Configuration](#reports-configuration)" below for more information.

!!! tip
    If the content of this referred file changes, the reports configuration used by the
    Insight Collector service will change accordingly.  You do not need to restart the service.

## Configuration File

This service has a configuration file (`insightcollector.yaml` by default) that describes 
the common service configuration options.

If no configuration file is found it will default to the following values:

```yaml
apiVersion: opentestfactory.org/v1beta2
kind: ServiceConfig
current-context: default
contexts:
- name: default
  context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: token
    workers_max_wait_seconds: 30
    configurations_path: /app/insights
    default_scope: 'true'
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is 7796 and the bind address is `0.0.0.0` as the service may expose a user-facing endpoint.

It has three service-specific configuration options besides the common ones.

### Worker Max Wait Seconds

_This limit must be an integer.  If the entry is missing, the default value will be assumed.
It must be defined in the currently-used context._

`workers_max_wait_seconds` sets the maximum duration to wait for workflow workers to complete their jobs.  If not specified, defaults to 30 seconds.

When a workflow completes, the insight collector service waits for all workers to finish their jobs.  If the workers do not complete within the specified time, the service will stop waiting and generate reports based on the available data.

### Configurations Path

_This configurations path must be a string.  If the entry is missing, the default value will be
assumed.  It must be defined in the currently-used context._

`configurations_path` sets the path to the directory containing reports configuration files.
If not specified, defaults to `/app/insights`.

See "[Reports Configuration](#reports-configuration)" below for more information.

### Default Scope

_This default scope value must be a string.  If the entry is missing, the default value will be
assumed.  It must be defined in the currently-user context._

`default_scope` sets the default scope to use when no scope is provided in the insight.
If not specified, defaults to `'true'`.

## Reports Configuration

### Customizing reports

If you want to customize reports, you must create a YAML file.

Reports configuration files use YAML syntax and may have a `.yml` or `.yaml` 
file extension.

A reports configuration file has one mandatory section: `insights`.  It 
must contain at least one entity definition. An entity is defined by his `name` 
and `kind` properties, which are mandatory.

An example of a reports configuration file is:

```yaml
insights:
- name: detailed-executionlog
  kind: ExecutionLog
  spec:
    step-depth: 0
    job-depth: 0
    max-command-length: 0
```

Please refer to the [Insights guide](../guides/insights.md) for more information.

### Default Configurations and Configuration Files Handling

The Insight Collector service provides default configurations for all insight kinds. These 
configurations can be overridden using report configuration files and default insights names ('executionlog' 
for `ExecutionLog` insight, 'executionreport' for `SummaryReport` and `SurefireXmlReport` insights).

An example of a reports configuration file that overrides default configuration values is:

```yaml
insights:
- name: executionlog
  kind: ExecutionLog
  spec:
    step-depth: 0
    job-depth: 0
    max-command-length: 0
- name: executionreport
  kind: SummaryReport
  spec:
    scope: test.technology == 'robotframework'
- name: executionreport
  kind: SurefireXmlReport
  spec:
    scope: test.technology == 'robotframework'
```

When this configuration file is used, default HTML and XML reports will contain only Robot Framework 
test results, while the default execution log will provide all execution details.

The service looks for report configurations in two locations: 

- The file defined by the `INSIGHTCOLLECTOR_CONFIGURATION` environment variable.
- The directory defined by the `configurations_path` parameter (`/app/insights/` by default).

Since there may be more than one configuration file, the default insight definitions are prioritized. 
Definitions from the file specified by the `INSIGHTCOLLECTOR_CONFIGURATION` variable have the highest priority.

The `configurations_path` directory can contain multiple configuration files. You can assign numerical prefixes 
to set their priority. For example, if the `configurations_path` directory contains the files `01_conf.yaml` 
and `02_conf.yaml`, the second file will take precedence over the first.

This prioritization logic also applies to non-default insights specified in configuration files. In other words, 
all same-named and same-typed insights across different files are prioritized, and only those with the highest 
priority are generated.

<h3>Example</h3>

Assume that the `INSIGHTCOLLECTOR_CONFIGURATION` environment variable is set to 
the path of the `env_conf.yaml` configuration file, and the `configurations_path` 
directory contains the files `01_conf.yaml` and `02_conf.yaml`:

```yaml title="env_conf.yaml"
insights: 
- name: executionlog
  kind: ExecutionLog
  spec:
    ...
- name: my-report
  kind: SummaryReport
  spec:
    ...
```

```yaml title="01_conf.yaml"
insights:
- name: executionreport
  kind: SummaryReport
  spec:
    ...
- name: my-report
  kind: SummaryReport
  spec:
    ...
- name: my-executionlog
  kind: ExecutionLog
  spec:
    ...
```

```yaml title="02_conf.yaml"
insights:
- name: executionreport
  kind: SummaryReport
  spec:
    ...
- name: executionreport
  kind: SurefireXmlReport
  spec:
    ...
```

First, the insights from the `01_conf.yaml` configuration are read. These are then 
overridden by the insights from the `02_conf.yaml`, and finally, the `env_conf.yaml` 
insights take precedence, overriding existing insights. As a result, the following 
insights are generated for a completed workflow:

- default `ExecutionLog` as defined in `env_conf.yaml`,
- default `SummaryReport` as defined in `02_conf.yaml`,
- default `SurefireXmlReport` as defined in `02_conf.yaml`,
- non-default `my-report` HTML report as defined in `env_conf.yaml`,
- and non-default `my-executionlog` execution log as defined in `01_conf.yaml`.

## Subscriptions

The insight collector plugin subscribes to the following events:

| `kind`              | `apiVersion`           |
| ------------------- | ---------------------- |
| `WorkflowCompleted` | opentestfactory.org/v1 |
| `WorkflowCancelled` | opentestfactory.org/v1 |

The insight collector plugin exposes an `/inbox` endpoint that is used by the event bus to post relevant events.


## Launch Command

If you want to manually start the Insight Collector service, use the following command:

```shell
python -m opentf.plugins.insightcollector.main [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."
