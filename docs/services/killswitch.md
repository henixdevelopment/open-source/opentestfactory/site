<!--
Copyright (c) 2021-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Killswitch Service

This core service can be used to cancel a currently executing _workflow_.

Due to the asynchronous nature of workflow executions, some actions may still
occur on execution environments and SUT when a workflow is canceled.

It exposes a [user-facing endpoint](endpoints.md#killswitch-service) that is used by clients.

There are no service-specific configuration file options.

## Environment variables

You can set the `KILLSWITCH_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `KILLSWITCH_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

## Configuration file

This module has a configuration file (`killswitch.yaml` by default)
that describes the _host_, _port_, _ssl_context_, and _trusted_authorities_ to use.  It can
also enable _insecure_ logins.

If no configuration file is found it will default to the following
values:

```yaml
apiVersion: opentestfactory.org/v1beta2
kind: SSHServiceConfig
current-context: default
contexts:
- context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: invalid
  name: default
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is `7776` and the bind address is `0.0.0.0` as the service exposes user-facing endpoints.

There are no service-specific configuration options besides the common ones.

## Subscriptions

The killswitch service subscribes to no event.

## Launch command

If you want to manually start the killswitch service, use the following command:

```shell
python -m opentf.core.killswitch [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."

