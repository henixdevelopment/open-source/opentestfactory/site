<!--
Copyright (c) 2022-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Agent channel service

This channel plugin manages communications with _execution environments_ that are accessed
via _agents_.

It exposes [user-facing endpoints](endpoints.md#agent-channel-plugin) that are used by agents.

There are two service-specific configuration file options.

## Environment variables

### Logging

You can set the `AGENTCHANNEL_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `AGENTCHANNEL_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

### Hooks specification

The `AGENTCHANNEL_CHANNEL_HOOKS` environment variable, if defined, must refer to an
existing file that contains hook definitions.

For more information about hooks, see "[Hooks for jobs and providers](../impl/reference/hooks.md)."

!!! tip
    If the content of this referred file changes, the hooks definitions used by the channel
    handler will change accordingly.  You do not need to restart the provider plugin.

### Configuration options

You can also set the two configuration options using environment variables:

- `ARRANGER_AVAILABILITY_CHECK_DELAY` sets the channel availability polling interval (defaults to 10 seconds).
- `ARRANGER_WAITRESS_THREADS_COUNT` sets the number of threads used by the waitress server (defaults to 10).

## Configuration file

This plugin has a configuration file (`agentchannel.yaml` by default) that describes the
_host_, _port_, _ssl_context_, and _trusted_authorities_ to use.  In this file, it is 
possible to enable _insecure_ logins or set _availability_check_delay_ to define the channel 
availability polling interval (defaults to 10 seconds).

If no configuration file is found it will default to the following
values:

```yaml hl_lines="12 13"
apiVersion: opentestfactory.org/v1beta2
kind: ServiceConfig
current-context: default
contexts:
- context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: invalid
    availability_check_delay: 10
    waitress_threads_count: 10
  name: default
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is `24368` and the bind address is `0.0.0.0` as the service exposes user-facing endpoints.

Two service-specific configuration options can be refined.

### `availability_check_delay`

The `availability_check_delay` option sets the channel availability polling interval.  It defaults to 10 seconds.

### `waitress_threads_count`

The `waitress_threads_count` option sets the number of threads used by the waitress server.  It defaults to 10.

## Subscriptions

The agent channel plugin subscribes to the following events:

| `kind`              | `apiVersion`                 |
| ------------------- | ---------------------------- |
| `ExecutionCommand`  | opentestfactory.org/v1beta1  |

The agent channel plugin exposes an `/inbox` endpoint that is used by the event bus to post relevant events.

## Launch command

If you want to manually start the agent channel plugin, use the following command:

```shell
python -m opentf.plugins.agentchannel.main [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."
