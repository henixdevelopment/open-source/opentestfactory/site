<!--
Copyright (c) 2023-2024 Henix, Henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# Tracker Publisher Plugin

This publisher plugin applies the quality gate to a successfully 
completed workflow.  It then publishes the result as a note in the tracker 
issue that is specified by the related workflow labels and the tracker 
instances configuration file.

It exposes no user-facing endpoints.

## Environment Variables

### Logging

You can set the `TRACKERPUBLISHER_DEBUG_LEVEL` (all upper-cased) or `DEBUG_LEVEL` environment variables
to `DEBUG` to add additional information in the console for the launched service. It defaults to
`INFO`. (Please note that setting `DEBUG_LEVEL` to `DEBUG` will produce tons of logs.)

The possible values are `NOTSET`, `DEBUG`, `INFO`, `WARNING`, `ERROR`, and `FATAL`. Those values
are from the most verbose, `NOTSET`, which shows all logs, to the least verbose, `FATAL`, which
only shows fatal errors.

If `TRACKERPUBLISHER_DEBUG_LEVEL` is not defined then the value of `DEBUG_LEVEL` is used (or `INFO` if
`DEBUG_LEVEL` is not defined either).

Access logs are only shown at `NOTSET` and `DEBUG` levels.

### Tracker Instances

The `TRACKERPUBLISHER_INSTANCES` (all upper-cased) environment variable must be 
defined to use the plugin. This variable must refer to an existing file that 
contains tracker instances definitions.

!!! tip
    If the content of this referred file changes, the tracker instances definitions used by the
    Tracker publisher plugin will change accordingly.  You do not need to restart the plugin.

## Tracker Instances File

The tracker publisher plugin needs at least one configured tracker instance. 
You must create a YAML file to configure your instance or instances.

Tracker instances files must use YAML syntax and may have a `.yml` or `.yaml` 
file extension.

A tracker instances file has two mandatory sections: `default-instance` and `instances`. 

```yaml
default-instance: main.instance

instances:
- name: main.instance
  endpoint: http://example.com
  type: gitlab
  keep-history: false
  token: 1e49e4nfgkd9333
  default-qualitygate: nightly.quality.gate
- name: backup.instance
  ...
```

`default-instance` is the name of the default tracker instance. This section 
is mandatory and the instance name must be present in `instances`.

`instances` section, which is also mandatory, lists the available tracker 
instances. It must contain at least one instance that will be used by Tracker 
publisher plugin to publish the quality gate results. 

Each tracker instance is described by the following (possibly optional) items:

- `name`: name of a tracker instance. Must be unique. Required.
- `type`: type of a tracker instance. Currently supported 
  types: `gitlab`. Required.
- `keep-history`: true or false. When set to `false`, the most recent results for 
an applied quality gate on the target are updated, otherwise, all the 
results are kept. Required.
- `endpoint`: URL of a tracker instance. Required.
- `token`: tracker authentication token.
- `default-qualitygate`: name of the default quality gate to apply when the 
  instance is used.

## Configuration File

This plugin has a configuration file (`trackerpublisher.yaml` by default) that describes 
the common service configuration options.

If no configuration file is found it will default to the following values:

```yaml
apiVersion: opentestfactory.org/v1beta2
kind: ServiceConfig
current-context: default
contexts:
- name: default
  context:
    port: 443
    host: 127.0.0.1
    ssl_context: adhoc
    eventbus:
      endpoint: https://127.0.0.1:38368
      token: token
    services:
      qualitygate:
        endpoint: http://127.0.0.1:12312
        token: reuse
```

The configuration included in the 'allinone' image is described in "[Common settings](config.md#configuration-file)."  The
listening port is 7797 and the bind address is `127.0.0.1` as the service exposes no user-facing endpoints.

## Subscriptions

The Tracker publisher plugin subscribes to the following events:

| `kind`              | `apiVersion`           |
| ------------------- | ---------------------- |
| `WorkflowCompleted` | opentestfactory.org/v1 |

The Tracker publisher plugin exposes an `/inbox` endpoint that is used by the event bus to post relevant events.


## Launch Command

If you want to manually start the Tracker publisher plugin, use the following command:

```shell
python -m opentf.plugins.trackerpublisher.main [--context context] [--config configfile]
```

Additional command-line options are available and described in "[Command-line options](config.md#command-line-options)."